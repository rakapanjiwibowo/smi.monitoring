﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Services;
using SMI.EncryptTools;
using SMI.EncryptTools.Agent;
using System.Configuration;
using System.Data.SqlClient;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain2.Services;

namespace SMI.Monitoring.IOC
{
    public class SimpleInjectorInitializer
    {
        static Container container = null;

        public static Container Initialize()
        {
            container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            RegisterComponents(container);

            //if DEBUG
            container.Verify();
            //end

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            return container;
        }

        public static Container GetContainer()
        {
            return container;
        }

        private static void RegisterComponents(Container container)
        {

            var encrypt = ConfigurationManager.AppSettings["Connection"];
            var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //var entities = new MainEntities();
            var entities = new MainEntities1();
            entities.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);


            //container.RegisterPerWebRequest<MainEntities>(() => new MainEntities());
            //container.RegisterPerWebRequest<MainEntities>(() => entities);

            //container.RegisterPerWebRequest<MainEntities1>(() => new MainEntities1());
            container.RegisterPerWebRequest<MainEntities1>(() => entities);

            container.RegisterPerWebRequest<MaxFlowEntities>(() => new MaxFlowEntities());
            container.RegisterPerWebRequest<IMSEntities>(() => new IMSEntities());
            container.RegisterPerWebRequest<vr2_smiEntities>(() => new vr2_smiEntities());
            container.RegisterPerWebRequest<OracleEntities>(() => new OracleEntities());
            container.RegisterPerWebRequest<vr1_staging_prodEntities>(() => new vr1_staging_prodEntities());
            container.RegisterPerWebRequest<MainEntitiesSPPD>(() => new MainEntitiesSPPD());

            container.RegisterPerWebRequest<IK2BinaryService, K2BinaryService>();
            container.RegisterPerWebRequest<IMappingRoleUserService, MappingRoleUserService>();
            container.RegisterPerWebRequest<IMappingRoleModuleService, MappingRoleModuleService>();
            container.RegisterPerWebRequest<IModuleService, ModuleService>();

            container.RegisterPerWebRequest<IAuditTrailMasterService, AuditTrailMasterService>();
            container.RegisterPerWebRequest<IActiveDirectoryService, ActiveDirectoryService>();
            container.RegisterPerWebRequest<ILoggingService, LoggingService>();
            container.RegisterPerWebRequest<IUserService, UserService>();
            container.RegisterPerWebRequest<IRoleService, RoleService>();
            container.RegisterPerWebRequest<ICustomQueryService, CustomQueryService>();
            container.RegisterPerWebRequest<ICustomQueryTreasuryService, CustomQueryTreasuryService>();
            container.RegisterPerWebRequest<ICustomQueryVRTreasuryService, CustomQueryVRTreasuryService>();
            container.RegisterPerWebRequest<IConfigureService, ConfigureService>();

            container.RegisterPerWebRequest<IIntBatchProcessService, IntBatchProcessService>();
            container.RegisterPerWebRequest<IintSourceService, IntSourceService>();
            container.RegisterPerWebRequest<IIntValidationLogService, IntValidationLogService>();
            container.RegisterPerWebRequest<IGLJournalLogService, GLJournalLogService>();
            container.RegisterPerWebRequest<IAPInvoiceLogService, APInvoiceLogService>();
            container.RegisterPerWebRequest<IAPSuplierLogService, APSuplierLogService>();
            container.RegisterPerWebRequest<IPOOrderLogService, POOrderLogService>();
            container.RegisterPerWebRequest<IPORequisitionLogService, PORequisitonLogService>();
            container.RegisterPerWebRequest<IARInvoiceLogService, ARInvoiceLogService>();
            container.RegisterPerWebRequest<IARReceiptLogService, ARReceiptLogService>();
            container.RegisterPerWebRequest<IFAAssetTransferLogService, FAAssetTransferLogService>();
            container.RegisterPerWebRequest<IGLProjectDebiturLogService, GLProjectDebiturService>();

            container.RegisterPerWebRequest<Isgt_gl_journal_stagingService, sgt_gl_journalService>();
            container.RegisterPerWebRequest<Isgt_ap_invoice_stagingService, sgt_ap_invoiceService>();

            container.RegisterPerWebRequest<IGASPPDService, GASPPDMonitoringService>();
            container.RegisterPerWebRequest<IMappingK2GASPPDService, MappingK2GASPPDService>();
            
        }


    }
}
