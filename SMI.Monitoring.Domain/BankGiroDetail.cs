//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class BankGiroDetail
    {
        public long Id { get; set; }
        public long BankGiroId { get; set; }
        public string NamaRekening { get; set; }
        public string NomorRekening { get; set; }
        public string Currency { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    }
}
