﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SMI.Monitoring.Domain.Services
{
    public interface IMappingRoleUserService
    {
        MappingRoleUserViewModel GetById(long Id);
        IEnumerable<MappingRoleUserViewModel> List(Expression<Func<MappingRoleUser, bool>> predicate = null);
        IEnumerable<VwMappingRoleUserViewModel> ListView(Expression<Func<VwMappingRoleUser, bool>> predicate = null);
        MappingRoleUserViewModel Insert(MappingRoleUserViewModel item);
        void Insert(IEnumerable<MappingRoleUserViewModel> list);
        void Edit(MappingRoleUserViewModel item);
        void Edit(IEnumerable<MappingRoleUserViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<MappingRoleUserViewModel> list);
    }
}
