﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SMI.Monitoring.Domain.Services
{
    public interface IModuleService
    {
        ModuleViewModel GetById(long Id);
        IEnumerable<ModuleViewModel> List(Expression<Func<Module, bool>> predicate = null);
        IEnumerable<VwModuleViewModel> ListView(Expression<Func<VwModule, bool>> predicate = null);
        ModuleViewModel Insert(ModuleViewModel item);
        void Insert(IEnumerable<ModuleViewModel> list);
        void Edit(ModuleViewModel item);
        void Edit(IEnumerable<ModuleViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<ModuleViewModel> list);
    }
}
