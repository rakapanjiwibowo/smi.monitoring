﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
    public interface IGASPPDService
    {
        GASPPDMonitoringViewModel GetById(long Id);
       // GASPPDMonitoringViewModel GetByUserName(string userAccount);
        //IEnumerable<GASPPDMonitoringViewModel> List(Expression<Func<SPPD, bool>> predicate = null);
        IEnumerable<GASPPDMonitoringViewModel> List();
        IEnumerable<VwMappingK2GASPPDViewModel> ListK2();    

        
    }
}
