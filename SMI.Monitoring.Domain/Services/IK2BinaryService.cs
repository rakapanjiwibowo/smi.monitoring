﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using SourceCode.Workflow.Client;

namespace SMI.Monitoring.Domain.Services
{
    public interface IK2BinaryService
    {
        string GetByProcessId(string folioNumber);
        Dictionary<string, object> PopulateK2DataField(
           string folioNumber,
           long offeringLetterId,
            string offeringLetterDate,
            long bankId,
            string bankCode,
            string bankName,
            long branchId,
            string branchCode,
            string branchName,
            string offeringLetterCode,
            string minimumAmount,
            string noRekeningBank,
            string atasNamaRekening,
            decimal rate1Month,
            decimal rate3Month,
            decimal? oldRate1Month,
            decimal? oldRate3Month,
            string currency,
            string isBreakable,
            string notes,
            int statusId,
            string tlAccount,
            string tlName,
            string tlEmail,
            string creatorUserAccount,
            string creatorFullName,
            string creatorEmail,
            string urlApproval,
            string urlDetail,
            string urlRevise,
            string rejectReason,
            string isRevise
           );
        string DefaultUserAccount { get; set; }
        bool RevertUser();
        long StartProcessInstance(string folioNumber, ref string viewFlowUrl);
        ProcessInstance OpenProcessInstance(int processInstanceId);
        ProcessInstance GetProcessInstances(int procInstId, ref string message);
        WorklistItem K2WorklistItem(string serialNumber, ref string message);
        bool ExecuteActionBySerialNumber(string serialNumber, Dictionary<string, object> dataFields, string action, ref string message);
        IEnumerable<WorklistItem> ListK2WorklistItem(ref string message);
        IEnumerable<WorklistItem> ListK2WorklistItem(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message);
        bool GoToActivity(int procInstId, string actionName, ref string message);
        bool GoToActivity(string serialNumber, string activity, ref string message);
        bool UpdateProcessInstance(int procInstId, Dictionary<string, object> datafields, ref string message);
        List<SourceCode.Workflow.Management.ProcessInstance> FindProcessInstances(string packageName, string folio, string originator, string fromDate, string toDate, ref string message);
        //====

        IEnumerable<WorklistItem> ListK2WorklistItemPlacement(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message);
        long StartProcessInstancePlacement(string folioNumber, ref string viewFlowUrl);
        Dictionary<string, object> PopulateK2DataFieldPlacement(
             long reqPlacementId,
             string reqPlacementNumber,
             string requestDate,
             string dirUserAccount,
             string dirFullName,
             string dirEmail,
             string creatorUserAccount,
             string creatorFullName,
             string creatorEmail,
             string depositoDetail,
             string urlDetail,
             string approvalUrl,
             string urlRevise,
            string isRevise
           );        
        //====
        IEnumerable<WorklistItem> ListK2WorklistItemEditPlacement(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message);
        long StartProcessInstanceEditPlacement(string folioNumber, ref string viewFlowUrl);
        Dictionary<string, object> PopulateK2DataFieldEditPlacement(
             long reqPlacementId,
             string reqPlacementNumber,
             string requestDate,
             string dirUserAccount,
             string dirFullName,
             string dirEmail,
             string creatorUserAccount,
             string creatorFullName,
             string creatorEmail,
             string depositoDetail,
             string urlDetail,
             string approvalUrl,
             string urlRevise,
            string isRevise
           );

        IEnumerable<SourceCode.Workflow.Management.WorklistItem> ListAllK2WorklistItem(string type);
        //====
        void Dispose();

        #region Edit OfferingLetter
        //==== EDIT OL ====
        Dictionary<string, object> PopulateK2DataFieldEditOL(
           string folioNumber,
           long offeringLetterId,
            string offeringLetterDate,
            long bankId,
            string bankCode,
            string bankName,
            long branchId,
            string branchCode,
            string branchName,
            string offeringLetterCode,
            string minimumAmount,
            string noRekeningBank,
            string atasNamaRekening,
            decimal rate1Month,
            decimal rate3Month,
            decimal? oldRate1Month,
            decimal? oldRate3Month,
            string currency,
            string isBreakable,
            string notes,
            int statusId,
            string tlAccount,
            string tlName,
            string tlEmail,
            string creatorUserAccount,
            string creatorFullName,
            string creatorEmail,
            string urlApproval,
            string urlDetail,
            string urlRevise,
            string rejectReason,
            string isRevise
           );
        long StartProcessInstanceEditOL(string folioNumber, ref string viewFlowUrl);
        IEnumerable<WorklistItem> ListK2WorklistItemEditOL(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message);
        #endregion

    }
}
