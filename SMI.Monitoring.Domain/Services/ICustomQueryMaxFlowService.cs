﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SMI.Monitoring.Domain.Services
{
    public interface ICustomQueryMaxFlowService
    {
        IEnumerable<T> ExecuteSqlQuery<T>(string query, params object[] args);
        IEnumerable<T> ExecuteQuery<T>(string query, params object[] args);
        int ExecuteStoreCommand(string query, params object[] args);
        IEnumerable<UserViewModel> ListLookUpUserAccount();
        UserViewModel LookUpUserAccount(string userAccount);
    }
}
