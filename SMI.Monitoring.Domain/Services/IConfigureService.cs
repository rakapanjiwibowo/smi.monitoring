﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;


namespace SMI.Monitoring.Domain.Services
{
    public interface IConfigureService
    {
        TblConfigureViewModel GetById(long Id);
        IEnumerable<TblConfigureViewModel> List(Expression<Func<TblConfigure, bool>> predicate = null);
        TblConfigureViewModel Insert(TblConfigureViewModel item);
        void Insert(IEnumerable<TblConfigureViewModel> list);
        void Edit(TblConfigureViewModel item);
        void Edit(IEnumerable<TblConfigureViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<TblConfigureViewModel> list);
    }
}
