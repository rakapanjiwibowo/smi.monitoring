﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SMI.Monitoring.Domain.Services
{
    public interface ICustomQueryVRTreasuryService
    {
        IEnumerable<T> VRExecuteSqlQuery<T>(string query, params object[] args);
        IEnumerable<T> VRExecuteQuery<T>(string query, params object[] args);
        int VRExecuteStoreCommand(string query, params object[] args);
    }
}
