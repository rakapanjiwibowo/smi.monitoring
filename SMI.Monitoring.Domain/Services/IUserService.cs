﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
    public interface IUserService
    {
        UserViewModel GetById(long Id);
        long FindIdByUserAccount(string userAccount);
        string GetUserRole(string userAccount);
        UserViewModel FindByUserAccount(string userAccount);
        IEnumerable<UserViewModel> List(Expression<Func<User, bool>> predicate = null);
        UserViewModel Insert(UserViewModel item);
        void Insert(IEnumerable<UserViewModel> list);
        void Edit(UserViewModel item);
        void Edit(IEnumerable<UserViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<UserViewModel> list);
        IEnumerable<UserViewModel> ListLookUpLoginUserAccount();
        UserViewModel LookUpLoginUserAccount(string userAccount);
        UserViewModel LookUpDirectReport(string userAccount);
        IEnumerable<UserViewModel> LookUpUserDepartment(string Department);
        IEnumerable<UserViewModel> ListDivision();
        //IEnumerable<UserViewModel> ListUserRole(Expression<Func<VwMappingRoleUser, bool>> predicate = null);
    }
}
