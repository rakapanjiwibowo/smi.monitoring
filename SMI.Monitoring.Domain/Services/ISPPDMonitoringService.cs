﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
   public interface ISPPDMonitoringService
    {
        MaxFlowViewModel Insert(MaxFlowEntities item);
        void Insert(IEnumerable<MaxFlowViewModel> list);

        IEnumerable<SPPDMonitoringViewModel> ListSPPD();
        IEnumerable<FPPDViewModel> ListFPPD();
        IEnumerable<MasterEmployeeViewModel> ListEmployee();
        IEnumerable<SPPDMonitoringViewModel> PendingFPPD();
        IEnumerable<MasterEmployeeViewModel> ListDivisi();

        bool EditSPPD(long param_id, string emp_account, string divisi, string pos_id);
        bool EditFPPD(long param_id, string emp_id, string emp_account, string divisi, string pos_id);
    }
}
