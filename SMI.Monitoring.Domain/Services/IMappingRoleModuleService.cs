﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SMI.Monitoring.Domain.Services
{
    public interface IMappingRoleModuleService
    {
        MappingRoleModuleViewModel GetById(long Id);
        IEnumerable<MappingRoleModuleViewModel> List(Expression<Func<MappingRoleModule, bool>> predicate = null);
        IEnumerable<VwMappingRoleModuleViewModel> ListView(Expression<Func<VwMappingRoleModule, bool>> predicate = null);
        MappingRoleModuleViewModel Insert(MappingRoleModuleViewModel item);
        void Insert(IEnumerable<MappingRoleModuleViewModel> list);
        void Edit(MappingRoleModuleViewModel item);
        void Edit(IEnumerable<MappingRoleModuleViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<MappingRoleModuleViewModel> list);
    }
}
