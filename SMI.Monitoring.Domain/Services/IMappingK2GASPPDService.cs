﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
    public interface IMappingK2GASPPDService
    {
       // VwMappingK2GASPPDViewModel GetById(long Id);
        IEnumerable<VwMappingK2GASPPDViewModel> List();
        //IEnumerable<VwMappingK2GASPPDViewModel> ListView();
    }
}
