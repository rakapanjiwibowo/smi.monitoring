﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
    public interface IActiveDirectoryService
    {
        ActiveDirectoryViewModel GetPersonalInformation(string username, string domain);
        ActiveDirectoryViewModel GetPersonalInformation(string username);
        IEnumerable<ActiveDirectoryViewModel> GetListPersonalInformations(string username, string domain);
    }
}
