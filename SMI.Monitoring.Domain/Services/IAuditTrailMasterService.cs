﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.Services
{
    public interface IAuditTrailMasterService
    {
        AuditTrailMasterViewModel GetById(long Id);
        IEnumerable<AuditTrailMasterViewModel> List(Expression<Func<AuditTrailMaster, bool>> predicate = null);
        IEnumerable<AuditTrailMasterDetailViewModel> ListDetail(Expression<Func<AuditTrailMasterDetail, bool>> predicate = null);
        IEnumerable<AuditTrailActionViewModel> ListAction(Expression<Func<AuditTrailAction, bool>> predicate = null);
        IEnumerable<VwAuditTrailMasterViewModel> ListView(Expression<Func<VwAuditTrailMaster, bool>> predicate = null);
        IEnumerable<VwAuditTrailMasterDetailViewModel> ListViewDetail(Expression<Func<VwAuditTrailMasterDetail, bool>> predicate = null);
        AuditTrailMasterViewModel Insert(AuditTrailMasterViewModel item);
        void Insert(IEnumerable<AuditTrailMasterViewModel> list);
        void Edit(AuditTrailMasterViewModel item);
        void Edit(IEnumerable<AuditTrailMasterViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<AuditTrailMasterViewModel> list);
        
        IEnumerable<AuditTrailMasterDetailViewModel> ListAuditDetail(Expression<Func<AuditTrailMasterDetail, bool>> predicate = null);
        AuditTrailMasterDetailViewModel InsertAuditDetail(AuditTrailMasterDetailViewModel item);
        void InsertAuditDetail(IEnumerable<AuditTrailMasterDetailViewModel> list);
        void EditAuditDetail(AuditTrailMasterDetailViewModel item);
        void EditAuditDetail(IEnumerable<AuditTrailMasterDetailViewModel> list);
        void DeleteAuditDetail(long Id);
        void DeleteAuditDetail(IEnumerable<AuditTrailMasterDetailViewModel> list);

        void AuditTrail<T>(List<T> oldObject, List<T> newObject, EnumAuditAction action, string moduleName, string notes, string userIdentity);
        void AuditTrail<T>(T oldObject, T newObject, EnumAuditAction action, string moduleName, string notes, string userIdentity);
    }
}
