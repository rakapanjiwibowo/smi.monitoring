﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
    public interface ILoggingService
    {
        void LogError(Exception e, string addMessage = null, string user = null);
        void LogError(LogErrorViewModel item);
        void SendErrorNotification(Exception e, string addMessage = null);
    }
}
