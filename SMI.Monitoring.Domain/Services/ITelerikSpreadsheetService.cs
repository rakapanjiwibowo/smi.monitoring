﻿using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace SMI.Monitoring.Domain.Services
{
    public interface ITelerikSpreadsheetService
    {
        void SetTemplatePath(string path);
        void SetParamArgs(params object[] _args);
        byte[] ExportToFile(string fileFormat, out string mimeType);
    }
}
