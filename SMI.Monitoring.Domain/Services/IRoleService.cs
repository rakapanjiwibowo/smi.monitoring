﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain.Services
{
    public interface IRoleService
    {
        RoleViewModel GetById(long Id);
        RoleViewModel GetByUserName(string userAccount);
        IEnumerable<RoleViewModel> List(Expression<Func<Role, bool>> predicate = null);
        RoleViewModel Insert(RoleViewModel item);
        void Insert(IEnumerable<RoleViewModel> list);
        void Edit(RoleViewModel item);
        void Edit(IEnumerable<RoleViewModel> list);
        void Delete(long Id);
        void Delete(IEnumerable<RoleViewModel> list);
        IEnumerable<VwMappingRoleModuleViewModel> ListMenu(string role);
    }
}
