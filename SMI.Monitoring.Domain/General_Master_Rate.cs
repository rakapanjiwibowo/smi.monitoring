//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class General_Master_Rate
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> Rate_Date { get; set; }
        public Nullable<decimal> Rate_USD { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Created_By { get; set; }
    }
}
