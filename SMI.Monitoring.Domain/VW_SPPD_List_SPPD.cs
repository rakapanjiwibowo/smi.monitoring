//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_SPPD_List_SPPD
    {
        public long ID { get; set; }
        public string Doc_ID { get; set; }
        public string Requestor_ID { get; set; }
        public string Requestor_Name { get; set; }
        public string Requestor_Position { get; set; }
        public string Creator_ID { get; set; }
        public string Creator_Name { get; set; }
        public string Creator_Position { get; set; }
        public string SPPD_Type_Desc { get; set; }
        public string Type_SPPD { get; set; }
        public Nullable<System.DateTime> Start_Date { get; set; }
        public Nullable<System.DateTime> End_Date { get; set; }
        public Nullable<long> Cost_Center_ID { get; set; }
        public string Purpose_Desc { get; set; }
        public string Activity_Desc { get; set; }
        public string Travel_Purpose_Desc { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Payment_Type { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public string Revision_Type { get; set; }
        public string Reff_SPPD { get; set; }
        public string SPPD_Status_WF { get; set; }
        public string SerialNumber { get; set; }
        public string FPPD_Doc_ID { get; set; }
        public string FPPD_Status_WF { get; set; }
        public string Doc00 { get; set; }
    }
}
