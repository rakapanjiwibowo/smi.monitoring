//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class Allowance
    {
        public long Allowance_ID { get; set; }
        public Nullable<long> SPPD_ID { get; set; }
        public Nullable<long> FPPD_ID { get; set; }
        public Nullable<decimal> Sum_Ticket_Allowance { get; set; }
        public Nullable<decimal> Sum_Diem_Allowance_IDR { get; set; }
        public Nullable<decimal> SUM_Diem_Allowance_USD { get; set; }
        public Nullable<decimal> Sum_Hotel_Allowance { get; set; }
        public Nullable<decimal> Sum_Rental_Allowance { get; set; }
        public Nullable<decimal> Sum_Visa_Allowance { get; set; }
        public Nullable<decimal> Sum_Insurance_Allowance { get; set; }
        public Nullable<decimal> Winter_Allowance_IDR { get; set; }
        public Nullable<decimal> Winter_Allowance_USD { get; set; }
        public Nullable<decimal> Passport_Allowance { get; set; }
        public Nullable<decimal> ReIssued_Expense { get; set; }
        public Nullable<decimal> Transport_Allowance { get; set; }
        public Nullable<decimal> Sum_Other_Expense { get; set; }
        public Nullable<decimal> Sum_GA_Adjustment { get; set; }
        public Nullable<decimal> Sum_Diem_Actual_Allowance { get; set; }
        public Nullable<decimal> Sum_Diem_Actual_Plan { get; set; }
        public Nullable<decimal> GRAND_TOTAL_IDR { get; set; }
        public Nullable<decimal> GRAND_TOTAL_USD { get; set; }
        public Nullable<decimal> GRAND_TOTAL_CONV { get; set; }
        public Nullable<decimal> BUDGET_USAGE_IDR { get; set; }
        public Nullable<decimal> BUDGET { get; set; }
        public Nullable<decimal> BUDGET_REALIZATION { get; set; }
        public Nullable<decimal> BUDGET_REMAINING { get; set; }
        public Nullable<decimal> USD_Rate { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
    }
}
