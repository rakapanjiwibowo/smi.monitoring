﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class MainEntitiesSPPD : DbContext
    {
        public MainEntitiesSPPD()
            : base("name=MainEntitiesSPPD")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Allowance> Allowances { get; set; }
        public virtual DbSet<Allowance_Temp> Allowance_Temp { get; set; }
        public virtual DbSet<Audit_Log> Audit_Log { get; set; }
        public virtual DbSet<Audit_Setup> Audit_Setup { get; set; }
        public virtual DbSet<Diem_Oracle> Diem_Oracle { get; set; }
        public virtual DbSet<FPPD> FPPDs { get; set; }
        public virtual DbSet<GA_Monitoring_Status> GA_Monitoring_Status { get; set; }
        public virtual DbSet<GAMonitoring> GAMonitorings { get; set; }
        public virtual DbSet<GAMonitoring_Log> GAMonitoring_Log { get; set; }
        public virtual DbSet<General_Bank_Account> General_Bank_Account { get; set; }
        public virtual DbSet<General_Master_Activity> General_Master_Activity { get; set; }
        public virtual DbSet<General_Master_Bank> General_Master_Bank { get; set; }
        public virtual DbSet<General_Master_Budget> General_Master_Budget { get; set; }
        public virtual DbSet<General_Master_Debitur> General_Master_Debitur { get; set; }
        public virtual DbSet<General_Master_Grade> General_Master_Grade { get; set; }
        public virtual DbSet<General_Master_Itinerary> General_Master_Itinerary { get; set; }
        public virtual DbSet<General_Master_Natural_Account> General_Master_Natural_Account { get; set; }
        public virtual DbSet<General_Master_Payment> General_Master_Payment { get; set; }
        public virtual DbSet<General_Master_Purpose> General_Master_Purpose { get; set; }
        public virtual DbSet<General_Master_Rate> General_Master_Rate { get; set; }
        public virtual DbSet<General_Master_TypeNA> General_Master_TypeNA { get; set; }
        public virtual DbSet<General_Master_Vendor> General_Master_Vendor { get; set; }
        public virtual DbSet<General_Master_Zona> General_Master_Zona { get; set; }
        public virtual DbSet<General_Passport> General_Passport { get; set; }
        public virtual DbSet<General_Sequence> General_Sequence { get; set; }
        public virtual DbSet<General_Setup> General_Setup { get; set; }
        public virtual DbSet<Invoicing> Invoicings { get; set; }
        public virtual DbSet<Invoicing_Detail> Invoicing_Detail { get; set; }
        public virtual DbSet<Last_Winter> Last_Winter { get; set; }
        public virtual DbSet<Mapping_Purpose> Mapping_Purpose { get; set; }
        public virtual DbSet<Master_SPPD_Type> Master_SPPD_Type { get; set; }
        public virtual DbSet<Master_Transport_Type> Master_Transport_Type { get; set; }
        public virtual DbSet<Master_Travel_Type> Master_Travel_Type { get; set; }
        public virtual DbSet<Master_Vendor_Type> Master_Vendor_Type { get; set; }
        public virtual DbSet<Other_Expense> Other_Expense { get; set; }
        public virtual DbSet<Pagu> Pagus { get; set; }
        public virtual DbSet<SPPD> SPPDs { get; set; }
        public virtual DbSet<SPPD_Cancellation_Attachment> SPPD_Cancellation_Attachment { get; set; }
        public virtual DbSet<SPPD_Non_Employee> SPPD_Non_Employee { get; set; }
        public virtual DbSet<SPPD_Requester_Temp> SPPD_Requester_Temp { get; set; }
        public virtual DbSet<SPPDEmailApprovalLog> SPPDEmailApprovalLogs { get; set; }
        public virtual DbSet<Supporting_Document> Supporting_Document { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<TEMP_XSMI_K2FND_INTERFACE> TEMP_XSMI_K2FND_INTERFACE { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<Train> Trains { get; set; }
        public virtual DbSet<Travel_Debitur> Travel_Debitur { get; set; }
        public virtual DbSet<Travel_Debitur_Actual> Travel_Debitur_Actual { get; set; }
        public virtual DbSet<Travel_Debitur_Actual_Temp> Travel_Debitur_Actual_Temp { get; set; }
        public virtual DbSet<Travel_Debitur_Temp> Travel_Debitur_Temp { get; set; }
        public virtual DbSet<Travel_Detail> Travel_Detail { get; set; }
        public virtual DbSet<Travel_Detail_Actual> Travel_Detail_Actual { get; set; }
        public virtual DbSet<Travel_Detail_Actual_Temp> Travel_Detail_Actual_Temp { get; set; }
        public virtual DbSet<Travel_Detail_Temp> Travel_Detail_Temp { get; set; }
        public virtual DbSet<Travel_Purpose_PDLN_Temp> Travel_Purpose_PDLN_Temp { get; set; }
        public virtual DbSet<WorkFlowHistoryTemp> WorkFlowHistoryTemps { get; set; }
        public virtual DbSet<Mapping_Purpose_Back> Mapping_Purpose_Back { get; set; }
        public virtual DbSet<SunFish_Master_TrainingType> SunFish_Master_TrainingType { get; set; }
        public virtual DbSet<VW_SPPD_Employee_Vendor_Oracle> VW_SPPD_Employee_Vendor_Oracle { get; set; }
        public virtual DbSet<VW_SPPD_List_GA_Invoicing> VW_SPPD_List_GA_Invoicing { get; set; }
        public virtual DbSet<VW_SPPD_List_GA_Invoicing_Detail> VW_SPPD_List_GA_Invoicing_Detail { get; set; }
        public virtual DbSet<VW_SPPD_List_GA_Monitoring> VW_SPPD_List_GA_Monitoring { get; set; }
        public virtual DbSet<VW_SPPD_List_GA_Monitoring_Detail> VW_SPPD_List_GA_Monitoring_Detail { get; set; }
        public virtual DbSet<VW_SPPD_List_SPPD> VW_SPPD_List_SPPD { get; set; }
        public virtual DbSet<VW_SPPD_Overview_FPPD> VW_SPPD_Overview_FPPD { get; set; }
    }
}
