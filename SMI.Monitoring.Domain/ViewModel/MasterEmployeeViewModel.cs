﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
   public class MasterEmployeeViewModel
    {
        public MasterEmployeeViewModel() { }

        public long Id { get; set; }
        public string Employee_ID { get; set; }
        public string AD_Account { get; set; }
        public string Employee_Name { get; set; }
        public string Email { get; set; }
        public string Divisi { get; set; } //structure
        public string Position_ID { get; set; }
        public string PositionName { get; set; }
        public string Department { get; set; }
        /// <summary>
        /// Is Active or Not
        /// </summary>
        public string Param6 { get; set; }

        #region custom

        #endregion
    }
}
