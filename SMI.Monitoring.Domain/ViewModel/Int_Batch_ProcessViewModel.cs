﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SMI.LOSDeposito.Domain.ViewModel
{
    public class Int_Batch_ProcessViewModel : IModel
    {
        [Display(Name = "Source ID")]
        public string INT_SOURCE_ID { get; set; }
        [Display(Name = "Batch ID")]
        public decimal INT_BATCH_ID { get; set; }
        [Display(Name = "Scheduler Flag")]
        public string SCHEDULER_FLAG { get; set; }
        [Display(Name = "Request Phase")]
        public string REQUEST_PHASE { get; set; }
        [Display(Name = "Request Status")]
        public string REQUEST_STATUS { get; set; }
        [Display(Name = "Request ID")]
        public decimal? REQUEST_ID { get; set; }
        [Display(Name = "Batch Flag")]
        public string BATCH_FLAG { get; set; }
        [Display(Name = "Creation Date")]
        public System.DateTime? CREATION_DATE { get; set; }
        [Display(Name = "Created By")]
        public string CREATED_BY { get; set; }
        [Display(Name = "Last Update Date")]
        public System.DateTime? LAST_UPDATE_DATE { get; set; }



        #region Tambahan Buat IModel
        public long Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        #endregion

    }
}
