﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class AuditTrailActionViewModel : IModel
    {
        public AuditTrailActionViewModel() { }

        public long Id { get; set; }
        public string Description { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public enum EnumAuditAction
    {
        Insert = 1,
        Edit = 2,
        Delete = 3
    }
}
