﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class VwMappingRoleModuleViewModel : IModel
    {
        public VwMappingRoleModuleViewModel() { }

        public long Id { get; set; }

        public long RoleId { get; set; }

        //[UIHint("ListModule")]
        public long ModuleId { get; set; }

        public bool IsGrantedToView { get; set; }

        public bool IsGrantedToCreate { get; set; }

        public bool IsGrantedToUpdate { get; set; }

        public bool IsGrantedToDelete { get; set; }

        public bool IsGrantedToDetail { get; set; }

        public bool IsGrantedToUpload { get; set; }

        public bool IsGrantedToDownload { get; set; }

        public bool IsGrantedToPrint { get; set; }

        public bool IsActive { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public System.DateTime? UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

        public string RoleCode { get; set; }

        public string RoleName { get; set; }

        public string RoleDescription { get; set; }

        public string ModuleAction { get; set; }

        public string ModuleCode { get; set; }

        public string ModuleController { get; set; }

        public string ModuleFileName { get; set; }

        public string ModuleDescription { get; set; }

        public string ModuleCssClass { get; set; }

        public long? ModuleOrderMenu { get; set; }

        public long? ModuleParentId { get; set; }

    }
}
