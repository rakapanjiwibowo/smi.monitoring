﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class MaxFlowViewModel
    {
        public MaxFlowViewModel() { }
        public long Id { get; set; }
        public long Transaction_ID { get; set; }
        public long Process_ID { get; set; }
        public string Process_Name { get; set; }
        public long Activity_ID { get; set; }
        public string Activity_Name { get; set; }
        public long Outcome { get; set; }
        public string Action_Name { get; set; }
        public string Remark { get; set; }
        public string Actioner_ID { get; set; }
        public string Actioner_Name { get; set; }
        public DateTime? Task_Received_Date { get; set; }
        public DateTime? ActionDate { get; set; }
        public decimal Duration { get; set; }
        public string Orig_Dest { get; set; }

        #region custom
        public string NextApprover { get; set; }
        public string No_SPPD { get; set; }
        public string Bulan { get; set; }
        public int Duration_Day { get; set; }
        #endregion
    }
}
