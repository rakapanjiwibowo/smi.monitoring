﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class VwUserViewModel : IModel
    {
        public VwUserViewModel() { }

        public long Id { get; set; }
        [Display(Name ="User Account"), Required(ErrorMessage ="User Account harus diisi")]
        public string UserAccount { get; set; }
        [Display(Name ="Nama Lengkap"), Required(ErrorMessage ="Nama lengkap harus diisi")]
        public string FullName { get; set; }
        [Display(Name ="Email"), Required(ErrorMessage ="Email harus diisi")]
        public string Email { get; set; }
        public bool IsActive { get; set; }

        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        #region Custom
        [Display(Name ="Role"), UIHint("ListRole"), Required(ErrorMessage ="Role harus dipilih")]
        public long RoleId { get; set; }
        public string RoleName { get; set; }
    
        
        #endregion

    }
}
