﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class ApprovalActionViewModel : IModel
    {
        public ApprovalActionViewModel() { }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

    }

    public enum EnumApprovalAction
    {
        Approve = 2,
        Revise = 3,
        Cancel = 4,
        Reject = 5,
        Submit = 6,
        Draft = 8
           
    }

}
