﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class MappingRoleModuleViewModel : IModel
    {
        public MappingRoleModuleViewModel() { }

        public long Id { get; set; }
        public long RoleId { get; set; }
        public long ModuleId { get; set; }
        public bool IsGrantedToView { get; set; }
        public bool IsGrantedToCreate { get; set; }
        public bool IsGrantedToUpdate { get; set; }
        public bool IsGrantedToDelete { get; set; }
        public bool IsGrantedToDetail { get; set; }
        public bool IsGrantedToUpload { get; set; }        
        public bool IsGrantedToDownload { get; set; }
        public bool IsGrantedToPrint { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
