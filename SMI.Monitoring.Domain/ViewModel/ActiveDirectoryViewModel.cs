﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class ActiveDirectoryViewModel
    {
        public ActiveDirectoryViewModel() { }

        public string Nik { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string AccountName { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string Title { get; set; }
        public string MobileNumber { get; set; }
        public bool IsLocked { get; set; }

        public string Domain { get; set; }
    }
}
