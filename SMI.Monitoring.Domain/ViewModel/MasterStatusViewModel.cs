﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class MasterStatusViewModel : IModel
    {
        public MasterStatusViewModel() { }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public enum EnumMasterStatus
    {
        New = 1,
        Approved = 2,
        Revise = 3,
        Rejected = 4,
        Canceled = 5,
        Edited = 6

    }
}
