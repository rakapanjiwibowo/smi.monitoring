﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.LOSDeposito.Domain.ViewModel
{
    public class VwMappingRoleUserViewModel : IModel
    {
        public VwMappingRoleUserViewModel() { }

        public long Id { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public string UserAccount { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
    }
}
