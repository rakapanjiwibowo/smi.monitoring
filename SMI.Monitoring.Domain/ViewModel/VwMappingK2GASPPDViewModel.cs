﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class VwMappingK2GASPPDViewModel
    {
        public long Seq { get; set; }
        public long Group_ID { get; set; }
        public string IFACE_CODE { get; set; }
        public string CREATION_DATE { get; set; }
        public string START_DATE { get; set; }
        public string FINISH_DATE { get; set; }
        public string STATUS { get; set; }
        public string RESULT { get; set; }
        public string NOTE { get; set; }
        //,[H01]
        public string H02 { get; set;}
        public string H04 { get; set;}
        //,[H03]
        //,[H04]
        //,[H05]
        //,[H06]
        //,[H07]
        public string H08 {get; set;}
        //,[H09]
        //,[H10]
        //,[H11]
        //,[H12]
        //,[H13]
        //,[H14]
        //,[H15]
        public string H16 { get; set; }
        public string H17 { get; set; }
        //,[H18]
        //,[H19]
        //,[H20]
        //,[H21]
        //,[H22]
        //,[H23]
        //,[H24]
        //,[H25]
        //,[H26]
        //,[H27]
        //,[H28]
        //,[H29]
        //,[H30]
        public string L01 { get; set; }
        public string L03 { get; set; }
        public string L04 { get; set; }
        public string L06 { get; set; }
        public string L07 { get; set; }

        public string L08 { get; set; }
        public string L09 { get; set; }
        public string L10 { get; set; }
        public string L11 { get; set; }
        public string L12 { get; set; }
        public string L15 { get; set; }
        public string L17 { get; set; }
        //,[L02]
        //,[L03]
        //,[L04]
        //,[L05]
        //,[L06]
        //,[L07]
        //,[L08]
        //,[L09]
        //,[L10]
        //,[L11]
        //,[L12]
        //,[L13]
        //,[L14]
        //,[L15]
        //,[L16]
        //,[L17]
        //,[L18]
        //,[L19]
        //,[L20]
        //,[L21]
        //,[L22]
        //,[L23]
        //,[L24]
        //,[L25]
        public long? Oracle_Seq { get; set; }
        public DateTime? Copy_Date { get; set; }



        #region Test

        //s public long ID { get; set; }
        //public long SPPD_ID { get; set; }
        #endregion
    }
}
