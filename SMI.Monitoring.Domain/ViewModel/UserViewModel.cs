﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class UserViewModel : IModel
    {
        public UserViewModel() { }

        public long Id { get; set; }
        [Display(Name = "User Account"), Required]
        public string UserAccount { get; set; }
        [Display(Name = "Nama Lengkap")]
        public string FullName { get; set; }
        [Display(Name = "Email"), Required]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        #region Custom
        public string EmployeeId { get; set; }
        public string NIK { get; set; }
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string PositionCode { get; set; }
        public string PositionName { get; set; }
        public string Divisi { get; set; }
        public string Department { get; set; }
        public string Password { get; set; }
        #endregion
    }
}
