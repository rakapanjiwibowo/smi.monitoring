﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.LOSDeposito.Domain.ViewModel
{
    public class VwMappingRoleUserViewModel : IModel
    {
        public VwMappingRoleUserViewModel() { }

        public long Id { get; set; }
        public long UserId { get; set; }

        [Display(Name = "Role"), Required(ErrorMessage = "Role harus dipilih")]
        public long RoleId { get; set; }
        public string UserAccount { get; set; }
        public string FullName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        #region custom
        [Display(Name = "User Account")]
        public string Account { get; set; }
        [Display(Name = "User Name")]
        public string Name { get; set; }
        #endregion
    }
}
