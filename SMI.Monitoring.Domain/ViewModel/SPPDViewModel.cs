﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class SPPDViewModel
    {
        public SPPDViewModel() { }
        public long ID { get; set; }
        public string Doc_ID { get; set; }
        public string Requestor_ID { get; set; }
        public string Requestor { get; set; }
        public string Requestor_Position { get; set; }
        public string Requestor_CompanyStructure { get; set; }
        public string Requestor_Company { get; set; }
        public long? Requestor_Grade_ID { get; set; }
        public string Creator_ID { get; set; }
        public string Creator { get; set; }
        public string Creator_Position { get; set; }
        public string Creator_CompanyStructure { get; set; }
        public string Creator_Company { get; set; }
        public string Assignor_ID { get; set; }
        public string Assignor { get; set; }
        public string Assignor_Position { get; set; }
        public string Assignor_CompanyStructure { get; set; }
        public string Assignor_Company { get; set; }
        public string SPPD_Type { get; set; }
        public bool? Winter { get; set; }
        public DateTime? LastWinterRequest { get; set; }
        public bool? Passport { get; set; }
        public string Passport_No { get; set; }
        public long? Purpose_ID { get; set; }
        public long? Activity_ID { get; set; }
        public string NA_Parent_Code { get; set; }
        public long? Cost_Center_ID { get; set; }
        public string Trave_Purpose_Desc { get; set; }
        public string Payment_Type { get; set; }
        public string Transport_To { get; set; }
        public string Transport_From { get; set; }
        public string Reff_SPPD { get; set; }
        public string Revision_Type { get; set; }
        public int? Request_Seq { get; set; }
        public DateTime? Start_Date { get; set; }
        public DateTime? End_Date { get; set; }
        public DateTime? Created_Date { get; set; }
        public DateTime? Modified_Date { get; set; }
        public string SerianNumber { get; set; }
        public string Status_WF { get; set; }
        public string Remark { get; set; }
        public string Type_SPPD { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }

        #region custom
        public string SPPD_ID { get; set; }
        public string No_SPPD { get; set; }
        public string Nama_Requestor { get; set; }
        public string Email { get; set; }
        public long Transaction_ID { get; set; }
        public string ActivityName { get; set; }
        public string DestinationUserAccount { get; set; }
        public string DestinationName { get; set; }
        public string DestinationEmail { get; set; }
        public int ProcInsId { get; set; }
        public int ActInstDestID { get; set; }
        public string Bulan { get; set; }
        public DateTime CreatedDate { get; set; }
        public string URL { get; set; }
        public string URLApproval { get; set; }
        public string URLDetail { get; set; }
        #endregion

        public enum EnumAction
        {
            Approve = 2,
            Reject = 3
        }
    }
}
