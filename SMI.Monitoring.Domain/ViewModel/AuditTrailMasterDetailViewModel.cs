﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class AuditTrailMasterDetailViewModel : IModel
    {
        public AuditTrailMasterDetailViewModel() { }

        public long Id { get; set; }
        public long AuditTrailMasterId { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public string FieldOldValues { get; set; }
        public string FieldNewValues { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
