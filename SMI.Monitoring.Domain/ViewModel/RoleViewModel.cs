﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class RoleViewModel : IModel
    {
        public RoleViewModel() { }

        public long Id { get; set; }
        [Display(Name = "Kode"), Required(ErrorMessage = "Kode harus diisi")]
        public string Code { get; set; }
        [Display(Name = "Role"), Required(ErrorMessage = "Role harus diisi")]
        public string RoleName { get; set; }
        public bool IsRoot { get; set; }
        public string Description { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public static explicit operator int(RoleViewModel v)
        {
            throw new NotImplementedException();
        }
    }

    public enum EnumRole
    {
        Administrator = 1,
        Operator = 3,
        User = 4
    }
}
