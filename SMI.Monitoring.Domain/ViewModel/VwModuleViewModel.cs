﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class VwModuleViewModel : IModel
    {
        public VwModuleViewModel() { }

        public long Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string CSSClass { get; set; }
        [UIHint("ParentId")]
        public long? ParentId { get; set; }
        public long? OrderMenu { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public string ParentCode { get; set; }
        public string ParentFileName { get; set; }

        #region Custom

        public string OrderMenuString
        {
            get
            {
                return string.Format("{0}", OrderMenu);
            }
        }

        #endregion

    }
}
