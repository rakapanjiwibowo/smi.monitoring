﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class AuditTrailMasterViewModel : IModel
    {
        public AuditTrailMasterViewModel() { }

        public long Id { get; set; }
        public string TypeName { get; set; }
        public string ModuleName { get; set; }
        public long AuditTrailActionId { get; set; }
        public string Notes { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
