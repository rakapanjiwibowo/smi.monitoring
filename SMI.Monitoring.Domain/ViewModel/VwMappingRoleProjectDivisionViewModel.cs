﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.LOSDeposito.Domain.ViewModel
{
    public class VwMappingRoleProjectDivisionViewModel : IModel
    {
        public VwMappingRoleProjectDivisionViewModel() { }

        public long Id { get; set; }
        [Required(ErrorMessage = "Divisi harus dipilih"), UIHint("ProjectDivision")]
        public long ProjectDivisionId { get; set; }
        public long RoleId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public string DivisionCode { get; set; }
        public string DivisionDescription { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
    }
}
