﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SMI.Monitoring.Domain.ViewModel
{
   public class SPPDMonitoringViewModel
    {
        public SPPDMonitoringViewModel() { }
        public long Id { get; set; }
        [Display(Name = "No.SPPD")]
        public string No_SPPD { get; set; }
        public string Requestor_Id { get; set; }
        public string Status_WF { get; set; }
        public string Nama_Requestor { get; set; }
        public string Email { get; set; }
        public string Field10 { get; set; }
        public string PositionName { get; set; }
        public string Divisi { get; set; }
        public string Department { get; set; }
        public string Destinantion { get; set; }
        public DateTime Date_From { get; set; }
        public DateTime Date_To { get; set; }


        #region custom
        public string SN { get; set; }
        public string Employee_ID { get; set; }
        public string NewDestinationAccount { get; set; }
        [Display(Name ="Employee Name")]
        public string Employee_Name { get; set; }
        public string Position_ID { get; set; }
        [Display(Name = "No. FPPD")]
        public string No_FPPD { get; set; }
        public long Transaction_ID { get; set; }
        public string ActivityName { get; set; }
        public string DestinationUserAccount { get; set; }
        public string DestinationName { get; set; }
        public string DestinationEmail { get; set; }
        public int ProcInsId { get; set; }
        public int ActInstDestID { get; set; }
        public string Bulan { get; set; }
        public DateTime CreatedDate { get; set; }
        public string URL { get; set; }
        public string URLApproval { get; set; }
        public string URLDetail { get; set; }
        #endregion

        public enum EnumAction
        {
            Approve = 2,
            Reject = 3
        }
    }
}
