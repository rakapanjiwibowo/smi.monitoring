﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
   public class GASPPDMonitoringViewModel
    {
        public long ID { get; set; }
        public string Doc_ID { get; set; }
        public string Requestor_ID { get; set; }
        public string Status_WF { get; set; }
        public string Travel_Purpose_Desc { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }

        #region Tambahan
        public string IFACE_CODE { get; set; }

        #endregion

    }
}
