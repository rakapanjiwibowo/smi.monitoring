﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class VwUserRoleViewModel : IModel
    {
        public VwUserRoleViewModel() { }

        public long Id { get; set; }
        [Display(Name="User Account"), Required]
        public string UserAccount { get; set; }
        [Display(Name="Nama Lengkap")]
        public string FullName { get; set; }
        [Display(Name="Email"), Required]
        public string Email { get; set; }
        public long RoleId { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public string RoleName { get; set; }

    }
}
