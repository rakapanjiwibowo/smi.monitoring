﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class MappingRoleUserViewModel : IModel
    {
        public MappingRoleUserViewModel() { }

        public long Id { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
