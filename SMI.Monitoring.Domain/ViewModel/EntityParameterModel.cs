﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain.ViewModel
{
    public class EntityParameterModel
    {
        public EntityParameterModel() { }
        public EntityParameterModel(long id, string value) { Id = id; Value = value; }
        public long Id { get; set; }
        public string Value { get; set; }
    }
}
