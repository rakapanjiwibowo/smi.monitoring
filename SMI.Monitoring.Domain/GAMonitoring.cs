//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class GAMonitoring
    {
        public long ID { get; set; }
        public Nullable<long> SPPD_ID { get; set; }
        public Nullable<long> Travel_Detail_ID { get; set; }
        public string Hotel_Status { get; set; }
        public Nullable<long> Hotel_Vendor { get; set; }
        public string Hotel_Remark { get; set; }
        public Nullable<decimal> Hotel_Actual_Amount { get; set; }
        public string Hotel_Invoice { get; set; }
        public string Ticket_Status { get; set; }
        public Nullable<long> Ticket_Vendor { get; set; }
        public string Ticket_Remark { get; set; }
        public Nullable<decimal> Ticket_Actual_Amount { get; set; }
        public string Ticket_Invoice { get; set; }
        public string Rental_Status { get; set; }
        public Nullable<long> Rental_Vendor { get; set; }
        public string Rental_Remark { get; set; }
        public Nullable<decimal> Rental_Actual_Amount { get; set; }
        public string Rental_Invoice { get; set; }
        public string Passport_Status { get; set; }
        public Nullable<long> Passport_Vendor { get; set; }
        public string Passport_Remark { get; set; }
        public Nullable<decimal> Passport_Actual_Amount { get; set; }
        public string Passport_Invoice { get; set; }
        public string Visa_Status { get; set; }
        public Nullable<long> Visa_Vendor { get; set; }
        public string Visa_Remark { get; set; }
        public Nullable<decimal> Visa_Actual_Amount { get; set; }
        public string Visa_Invoice { get; set; }
        public string Insuranec_Status { get; set; }
        public Nullable<long> Insurance_Vendor { get; set; }
        public string Insurance_Remark { get; set; }
        public Nullable<decimal> Insurance_Actual_Amount { get; set; }
        public string Insurance_Invoice { get; set; }
        public string Diem_Status { get; set; }
        public string Diem_Invoice { get; set; }
        public string ReIssued_Status { get; set; }
        public Nullable<long> ReIssued_Vendor { get; set; }
        public string ReIssued_Remark { get; set; }
        public Nullable<decimal> ReIssued_Actual_Amount { get; set; }
        public string ReIssued_Invoice { get; set; }
        public string VAT_Hotel { get; set; }
        public Nullable<decimal> VAT_Rate_Hotel { get; set; }
        public Nullable<decimal> VAT_Adj_Hotel { get; set; }
        public string PPh_Hotel { get; set; }
        public Nullable<decimal> PPh_Rate_Hotel { get; set; }
        public string VAT_Ticket { get; set; }
        public Nullable<decimal> VAT_Rate_Ticket { get; set; }
        public Nullable<decimal> VAT_Adj_Ticket { get; set; }
        public string PPh_Ticket { get; set; }
        public Nullable<decimal> PPh_Rate_Ticket { get; set; }
        public string VAT_Rental { get; set; }
        public Nullable<decimal> VAT_Rate_Rental { get; set; }
        public Nullable<decimal> VAT_Adj_Rental { get; set; }
        public string PPh_Rental { get; set; }
        public Nullable<decimal> PPh_Rate_Rental { get; set; }
        public string VAT_Passport { get; set; }
        public Nullable<decimal> VAT_Rate_Passport { get; set; }
        public Nullable<decimal> VAT_Adj_Passport { get; set; }
        public string PPh_Passport { get; set; }
        public Nullable<decimal> PPh_Rate_Passport { get; set; }
        public string VAT_Visa { get; set; }
        public Nullable<decimal> VAT_Rate_Visa { get; set; }
        public Nullable<decimal> VAT_Adj_Visa { get; set; }
        public string PPh_Visa { get; set; }
        public Nullable<decimal> PPh_Rate_Visa { get; set; }
        public string VAT_Insurance { get; set; }
        public Nullable<decimal> VAT_Rate_Insurance { get; set; }
        public Nullable<decimal> VAT_Adj_Insurance { get; set; }
        public string PPh_Insurance { get; set; }
        public Nullable<decimal> PPh_Rate_Insurance { get; set; }
        public string VAT_Reissued { get; set; }
        public Nullable<decimal> VAT_Rate_Reissued { get; set; }
        public Nullable<decimal> VAT_Adj_Reissued { get; set; }
        public string PPh_Reissued { get; set; }
        public Nullable<decimal> PPh_Rate_Reissued { get; set; }
        public Nullable<long> Hotel_Vendor_Site_ID { get; set; }
        public Nullable<long> Ticket_Vendor_Site_ID { get; set; }
        public Nullable<long> Rental_Vendor_Site_ID { get; set; }
        public Nullable<long> Passport_Vendor_Site_ID { get; set; }
        public Nullable<long> Visa_Vendor_Site_ID { get; set; }
        public Nullable<long> Insurance_Vendor_Site_ID { get; set; }
        public Nullable<long> ReIssued_Vendor_Site_ID { get; set; }
        public string Hotel_Vendor_Name { get; set; }
        public string Ticket_Vendor_Name { get; set; }
        public string Rental_Vendor_Name { get; set; }
        public string Passport_Vendor_Name { get; set; }
        public string Visa_Vendor_Name { get; set; }
        public string Insurance_Vendor_Name { get; set; }
        public string Reissued_Vendor_Name { get; set; }
        public string Hotel_Vendor_Site_Name { get; set; }
        public string Ticket_Vendor_Site_Name { get; set; }
        public string Rental_Vendor_Site_Name { get; set; }
        public string Passport_Vendor_Site_Name { get; set; }
        public string Visa_Vendor_Site_Name { get; set; }
        public string Insurance_Vendor_Site_Name { get; set; }
        public string Reissued_Vendor_Site_Name { get; set; }
        public string SPM_Hotel { get; set; }
        public string SPM_Ticket { get; set; }
        public string SPM_Rental { get; set; }
        public string SPM_Passport { get; set; }
        public string SPM_Visa { get; set; }
        public string SPM_Insurance { get; set; }
        public string SPM_Reissue { get; set; }
        public Nullable<decimal> Hotel_Amount_0 { get; set; }
        public string VAT_Hotel_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Hotel_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Hotel_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Hotel_0 { get; set; }
        public string PPh_Hotel_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Hotel_0 { get; set; }
        public Nullable<decimal> Ticket_Amount_0 { get; set; }
        public string VAT_Ticket_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Ticket_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Ticket_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Ticket_0 { get; set; }
        public string PPh_Ticket_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Ticket_0 { get; set; }
        public Nullable<decimal> Rental_Amount_0 { get; set; }
        public string VAT_Rental_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Rental_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Rental_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Rental_0 { get; set; }
        public string PPh_Rental_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Rental_0 { get; set; }
        public Nullable<decimal> Passport_Amount_0 { get; set; }
        public string VAT_Passport_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Passport_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Passport_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Passport_0 { get; set; }
        public string PPh_Passport_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Passport_0 { get; set; }
        public Nullable<decimal> Visa_Amount_0 { get; set; }
        public string VAT_Visa_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Visa_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Visa_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Visa_0 { get; set; }
        public string PPh_Visa_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Visa_0 { get; set; }
        public Nullable<decimal> Insurance_Amount_0 { get; set; }
        public string VAT_Insurance_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Insurance_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Insurance_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Insurance_0 { get; set; }
        public string PPh_Insurance_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Insurance_0 { get; set; }
        public Nullable<decimal> Reissued_Amount_0 { get; set; }
        public string VAT_Reissued_0 { get; set; }
        public Nullable<decimal> VAT_Rate_Reissued_0 { get; set; }
        public Nullable<decimal> VAT_Adj_Reissued_0 { get; set; }
        public Nullable<decimal> VAT_Amount_Reissued_0 { get; set; }
        public string PPh_Reissued_0 { get; set; }
        public Nullable<decimal> PPh_Rate_Reissued_0 { get; set; }
    }
}
