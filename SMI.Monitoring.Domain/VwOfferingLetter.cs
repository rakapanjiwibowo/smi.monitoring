//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class VwOfferingLetter
    {
        public long Id { get; set; }
        public string FolioNumber { get; set; }
        public string OLCode { get; set; }
        public Nullable<long> ProcInsId { get; set; }
        public System.DateTime OfferingDate { get; set; }
        public long BankId { get; set; }
        public string BankCode { get; set; }
        public long BranchId { get; set; }
        public string BranchCode { get; set; }
        public string NoSurat { get; set; }
        public decimal MinimumAmount { get; set; }
        public string NoRekBank { get; set; }
        public string NoRekAccountName { get; set; }
        public decimal Rate1Month { get; set; }
        public decimal Rate3Month { get; set; }
        public Nullable<decimal> OldRate1Month { get; set; }
        public Nullable<decimal> OldRate3Month { get; set; }
        public string DayBasis { get; set; }
        public Nullable<int> DayBasisValue { get; set; }
        public string Currency { get; set; }
        public bool IsBreakable { get; set; }
        public string Notes { get; set; }
        public bool IsActive { get; set; }
        public long StatusId { get; set; }
        public string RejectReason { get; set; }
        public Nullable<bool> IsEdit { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string BankName { get; set; }
        public string BankRating { get; set; }
        public string BranchName { get; set; }
        public Nullable<bool> IsBankActive { get; set; }
        public string Status { get; set; }
        public string FileName { get; set; }
        public Nullable<int> VRCounterPartyId { get; set; }
        public string MappingCounterPartyCode { get; set; }
    }
}
