//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class VwDepositoPlacementHeader
    {
        public long Id { get; set; }
        public Nullable<long> ProcInsId { get; set; }
        public string ProcessFolioNumber { get; set; }
        public string NoMemo { get; set; }
        public long StatusId { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public Nullable<bool> IsEdited { get; set; }
        public string Guid { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
