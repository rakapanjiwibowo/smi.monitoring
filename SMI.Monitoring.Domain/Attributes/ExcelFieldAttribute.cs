﻿using System;

namespace SMI.Monitoring.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple = true)]
    public class ExcelFieldAttribute:Attribute
    {
        public ExcelFieldAttribute(){}
        public ExcelFieldAttribute(string name) { Name = name; }
        public ExcelFieldAttribute(string name, string dataType) { Name = name; DataType = dataType; }

        public string Name { get; set; }
        public string DataType { get; set; }
    }
}
