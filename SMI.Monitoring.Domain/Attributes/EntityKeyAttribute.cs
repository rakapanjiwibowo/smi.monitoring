﻿using System;

namespace SMI.Monitoring.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class EntityKeyAttribute : Attribute
    {
        public EntityKeyAttribute() { }
    }
}
