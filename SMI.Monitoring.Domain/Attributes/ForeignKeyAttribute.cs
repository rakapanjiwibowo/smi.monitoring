﻿using System;

namespace SMI.Monitoring.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class ForeignKeyAttribute : Attribute
    {
        public ForeignKeyAttribute(Type entity) { Entity = entity; }
        public Type Entity { get; set; }
    }
}
