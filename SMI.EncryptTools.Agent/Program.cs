﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.EncryptTools.Agent
{
    class Program
    {
        static void Main(string[] args)
        {
            string key = "";
            string input = @"This topic provides information to help you troubleshoot typical problems with configuring server instances for Always On availability groups. Typical configuration problems include Always On availability groups is disabled, accounts are incorrectly configured, the database mirroring endpoint does not exist, the endpoint is inaccessible (SQL Server Error 1418), network access does not exist, and a join database command fails (SQL Server Error 35250).";
            input = @"metadata=res://*/ProjectManagementModel.csdl|res://*/ProjectManagementModel.ssdl|res://*/ProjectManagementModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=192.168.29.45;initial catalog=SMI.ProjectManagement;user id=dummy_user;password=dummy_password;MultipleActiveResultSets=True;App=EntityFramework&quot;";
            
            var encrypted = AESCryptoWithBCHelper.EncryptAESBounceCastle(input, key);
            Console.WriteLine("encrypted: " + encrypted);
            Console.WriteLine();
            var decrypted = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypted, key);
            Console.WriteLine("decrypted: " + decrypted);
            Console.WriteLine();

            //AESCryptoWithBCHelper.TestBC();
        }
    }
}
