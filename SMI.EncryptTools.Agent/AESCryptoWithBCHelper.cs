﻿using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.EncryptTools.Agent
{
    /// <summary>
    /// https://stackoverflow.com/questions/29701401/encrypt-string-with-bouncy-castle-aes-cbc-pkcs7
    /// </summary>
    public class AESCryptoWithBCHelper
    {
        public static string keyString = "jDxESdRrcYKmSZi7IOW4lw==";

        public static string EncryptAESBounceCastle(string input, string key = null)
        {
            if(!string.IsNullOrEmpty(key))
            {
                keyString = key;
            }
            
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] iv = new byte[16]; //for the sake of demo

            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(blockCipher); //Default scheme is PKCS5/PKCS7
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(keyString));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);

            // Encrypt
            cipher.Init(true, keyParamWithIV);
            byte[] outputBytes = new byte[cipher.GetOutputSize(inputBytes.Length)];
            int length = cipher.ProcessBytes(inputBytes, outputBytes, 0);
            cipher.DoFinal(outputBytes, length); //Do the final block
            string encryptedInput = Convert.ToBase64String(outputBytes);

            Console.WriteLine("Encrypted string: {0}", encryptedInput);
            return encryptedInput;

        }

        public static string DecryptAESBounceCastle(string input, string key = null)
        {
            if (!string.IsNullOrEmpty(key))
            {
                keyString = key;
            }

            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] iv = new byte[16]; //for the sake of demo

            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(blockCipher); //Default scheme is PKCS5/PKCS7
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(keyString));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);
            
            //Decrypt            
            cipher.Init(false, keyParamWithIV);
            byte[] outputBytes = Convert.FromBase64String(input);
            byte[] comparisonBytes = new byte[cipher.GetOutputSize(outputBytes.Length)];
            int length = cipher.ProcessBytes(outputBytes, comparisonBytes, 0);
            cipher.DoFinal(comparisonBytes, length); //Do the final block

            string decryptedInput = Encoding.UTF8.GetString(comparisonBytes);
            Console.WriteLine("Decrypted string: {0}", decryptedInput);
            return decryptedInput;
        }

        public static void TestBC()
        {
            //Demo params
            string keyString = "jDxESdRrcYKmSZi7IOW4lw==";

            string input = "abc";
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] iv = new byte[16]; //for the sake of demo

            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(blockCipher); //Default scheme is PKCS5/PKCS7
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(keyString));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);

            // Encrypt
            cipher.Init(true, keyParamWithIV);
            byte[] outputBytes = new byte[cipher.GetOutputSize(inputBytes.Length)];
            int length = cipher.ProcessBytes(inputBytes, outputBytes, 0);
            cipher.DoFinal(outputBytes, length); //Do the final block
            string encryptedInput = Convert.ToBase64String(outputBytes);

            Console.WriteLine("Encrypted string: {0}", encryptedInput);

            //Decrypt            
            cipher.Init(false, keyParamWithIV);
            byte[] comparisonBytes = new byte[cipher.GetOutputSize(outputBytes.Length)];
            length = cipher.ProcessBytes(outputBytes, comparisonBytes, 0);
            cipher.DoFinal(comparisonBytes, length); //Do the final block

            Console.WriteLine("Decrypted string: {0}", Encoding.UTF8.GetString(comparisonBytes)); //Should be abc
        }

        /// <summary>
        /// deprecated
        /// </summary>
        /// <param name="input"></param>
        /// <param name="iv_base64"></param>
        /// <returns></returns>
        public static string BCEncrypt(string input, out string iv_base64)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            SecureRandom random = new SecureRandom();
            byte[] iv = new byte[16];
            random.NextBytes(iv);
            iv_base64 = Convert.ToBase64String(iv);
            string keyString = "mysecretkey12345";
            string keyStringBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(keyString));

            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CbcBlockCipher(engine), new Pkcs7Padding());
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(keyStringBase64));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);

            // Encrypt
            cipher.Init(true, keyParamWithIV);
            byte[] outputBytes = new byte[cipher.GetOutputSize(inputBytes.Length)];
            int length = cipher.ProcessBytes(inputBytes, outputBytes, 0);
            cipher.DoFinal(outputBytes, length); //Do the final block
            return Convert.ToBase64String(outputBytes);
        }

        /// <summary>
        /// deprecated
        /// </summary>
        /// <param name="input"></param>
        /// <param name="iv_base64"></param>
        /// <returns></returns>
        public static string BCDecrypt(string input, string iv_base64)
        {
            string keyString = "mysecretkey12345";
            string keyStringBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(keyString));
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] iv = Convert.FromBase64String(iv_base64);
            //Set up
            AesEngine engine = new AesEngine();
            CbcBlockCipher blockCipher = new CbcBlockCipher(engine); //CBC
            PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CbcBlockCipher(engine), new Pkcs7Padding());
            KeyParameter keyParam = new KeyParameter(Convert.FromBase64String(keyStringBase64));
            ParametersWithIV keyParamWithIV = new ParametersWithIV(keyParam, iv, 0, 16);

            //Decrypt            
            byte[] outputBytes = Convert.FromBase64String(input);
            cipher.Init(false, keyParamWithIV);
            byte[] comparisonBytes = new byte[cipher.GetOutputSize(outputBytes.Length)];
            int length = cipher.ProcessBytes(outputBytes, comparisonBytes, 0);
            cipher.DoFinal(comparisonBytes, length); //Do the final block
            return Encoding.UTF8.GetString(comparisonBytes, 0, comparisonBytes.Length);
        }

    }
}
