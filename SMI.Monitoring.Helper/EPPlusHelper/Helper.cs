﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SMI.Monitoring.Helper.EPPlusHelper
{
    public static class Helper
    {
        static Helper() { }

        /// <summary>
        /// Check if string is convertible to numeric value
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static bool isNumeric(this ExcelRange cell)
        {
            long val = 0;
            decimal decimalVal = 0;
            return cell.Value != null && (long.TryParse(cell.Value.ToString(), out val) || decimal.TryParse(cell.Value.ToString(), out decimalVal));
        }

        public static bool isNumeric(this string value)
        {
            long longVal = 0;
            decimal decimalVal = 0;
            return (!string.IsNullOrEmpty(value) && (long.TryParse(value, out longVal) || decimal.TryParse(value, out decimalVal)));
        }

        /// <summary>
        /// Check if cells contains any value for specific range except for any in string exception, ex cell[A1:F9] 
        /// </summary>
        /// <param name="cells"></param>
        /// <returns></returns>
        public static bool HaveAnyValue(this ExcelRange cells, string stringException = null)
        {
            foreach (var cell in cells)
            {
                if (cell.Value != null)
                {
                    if (!string.IsNullOrEmpty(stringException) && cell.Value.ToString().Equals(stringException, StringComparison.CurrentCultureIgnoreCase))   //by pass specific characters, like '-', etc
                    {
                        continue;
                    }
                    else if (!string.IsNullOrEmpty(cell.Value.ToString()))
                    {
                        return true;
                    }
                    else { }
                }
            }
            return false;
        }

        /// <summary>
        /// Get value from cell with formula. By default cell returns empty value if formula not evaluated first..
        /// </summary>
        /// <param name="cells"></param>
        /// <returns></returns>
        public static object GetCalculatedValue(this ExcelRange cells)
        {
            foreach (var cell in cells)
            {
                if (!string.IsNullOrEmpty(cell.Formula) && (cell.Value == null || string.IsNullOrEmpty(cell.Value.ToString()))) cell.Calculate();
            }
            return cells.Value;
        }

        /// <summary>
        /// Get value from collection of cells and merge into single string
        /// </summary>
        /// <param name="cells"></param>
        /// <returns></returns>
        public static string GetMergedCellCollectionsValue(this ExcelRange cells, bool ignoreFormat = false)
        {
            string result = string.Empty;
            foreach (var cell in cells)
            {
                if (!string.IsNullOrEmpty(cell.Formula) && (cell.Value == null || string.IsNullOrEmpty(cell.Value.ToString()))) cell.Calculate();
                if (cell.Value != null)
                {
                    if (cell.Value is double || cell.Value is decimal)
                    {
                        string temporary = string.Empty;
                        //check if datatype is datetime
                        try
                        {
                            string format = cell.Style.Numberformat.Format;
                            if (format.ToLower().Contains("m") || (!format.Equals("Number", StringComparison.CurrentCultureIgnoreCase) && !format.Equals("General", StringComparison.CurrentCultureIgnoreCase) && !format.Contains("#") && !format.Contains(".")))    //check if date or not                          
                            {
                                var tempResult = ConvertDateTime(cell.Value);
                                if (ignoreFormat)
                                {
                                    temporary = tempResult.ToString();
                                }
                                else
                                {
                                    temporary = tempResult.ToString(format);
                                }
                            }
                            else temporary = cell.Value.ToString();
                        }
                        catch
                        {
                            temporary = cell.Value.ToString();
                        }
                        result += temporary;
                    }
                    else if (!string.IsNullOrEmpty(cell.Value.ToString()))
                    {
                        result += cell.Value.ToString();
                    }
                }
                //if (cell.Value != null && !string.IsNullOrEmpty(cell.Value.ToString())) result += cell.Value.ToString();
            }
            return result;
        }

        /// <summary>
        /// Get interval row for specific cell range address.
        /// For range address only
        /// </summary>
        /// <param name="rangeAddress"></param>
        /// <returns></returns>
        public static int IntervalRows(this string rangeAddress)
        {
            var address = rangeAddress.Split(':');
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match1 = RegexCellAddress.Match(address[0]);
            string numericAddress1 = match1.Groups["Numeric"].Value;
            var match2 = RegexCellAddress.Match(address[1]);
            string numericAddress2 = match2.Groups["Numeric"].Value;
            return Math.Abs(Convert.ToInt32(numericAddress1) - Convert.ToInt32(numericAddress2));
        }

        public static int IntervalColumns(this string rangeAddress)
        {
            throw new NotImplementedException("Method Helper.IntervalColumns not implemented yet");
        }

        /// <summary>
        /// Set column address value for cell address
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string SetColumnAddress(this string cellAddress, string column)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else if (column.IndexOf("0123456789") >= 0)
            {
                throw new Exception("Invalid value. Column address cannot contain numeric value.");
            }
            else
            {
                return string.Format("{0}{1}", column, numericAddress);
            }
        }

        public static string GetColumnAddress(this string cellAddress)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            return wordAdress;
        }


        public static long GetRowAddress(this string cellAddress)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            return long.Parse(numericAddress);
        }

        /// <summary>
        /// Set row address value for cell address
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static string SetRowAddress(this string cellAddress, int row)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else if (row <= 0)
            {
                throw new Exception("Invalid value. Row address must be greater than zero.");
            }
            else
            {
                return string.Format("{0}{1}", wordAdress, row);
            }
        }

        /// <summary>
        /// Add specific column for cell address.
        /// For cell address only
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <returns></returns>
        public static string MoveColumns(this string cellAddress, int total)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else
            {
                long tempValue = ConvertColumnValue(wordAdress);
                if ((tempValue + total) <= 0)
                {
                    throw new Exception("Invalid value, unable to move cell address to specific columns. The minimum cell address column value must be 'A'");
                }
                else
                {
                    string newWordAddress = GetColumnAddress(tempValue + total);
                    return string.Format("{0}{1}", newWordAddress, numericAddress);
                }
            }
        }


        /// <summary>
        /// Add specific column for cell address.
        /// For cell address only
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <returns></returns>
        public static string MoveRows(this string cellAddress, int total)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else
            {
                int newNumericAddress = Convert.ToInt32(numericAddress) + total;
                return string.Format("{0}{1}", wordAdress, newNumericAddress);
            }
        }

        /// <summary>
        /// Move cell address to specific rows and columns.
        /// For cell address only
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static string MoveRowsAndColumns(this string cellAddress, int rows, int columns)
        {
            string tempAddress = MoveRows(cellAddress, rows);
            tempAddress = MoveColumns(tempAddress, columns);
            return tempAddress;
        }

        /// <summary>
        /// Move cell range address (ie:AA13:BH65) to specific rows and columns.
        /// For range address only, ie [A21:B45]
        /// </summary>
        /// <param name="cellRangeAddress"></param>
        /// <param name="row1"></param>
        /// <param name="column1"></param>
        /// <param name="row2"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        public static string MoveRowsAndColumns(this string cellRangeAddress, int row1, int column1, int row2, int column2)
        {
            //validate the range address value first..
            if (string.IsNullOrEmpty(cellRangeAddress))
            {
                throw new Exception("Invalid value for cell range address. Value cannot empty.");
            }
            else
            {
                var temp = cellRangeAddress.Split(':');
                if (temp.Count() != 2 || string.IsNullOrEmpty(temp[0]) || string.IsNullOrEmpty(temp[1]))
                {
                    throw new Exception("Invalid value for cell range address. The current format valus is not valid ([" + cellRangeAddress + "]). Format value must be, for ie: [A21:B65]");
                }
                else
                {
                    string result = string.Format("{0}:{1}", temp[0].MoveRowsAndColumns(row1, column1), temp[1].MoveRowsAndColumns(row2, column2));
                    return result;
                }
            }
        }

        /// <summary>
        /// Get cell datetime values
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeValue(this ExcelRange cell)
        {
            long serialDate = 0;
            DateTime result;
            string tempValue = cell.Value != null ? cell.Value.ToString() : string.Empty;
            bool isNumeric = long.TryParse(tempValue, out serialDate);
            if (isNumeric)
            {
                result = DateTime.FromOADate(serialDate);
            }
            else
            {
                DateTime.TryParse(tempValue, out result);
            }
            return result;
        }

        /// <summary>
        /// See @ http://stackoverflow.com/questions/297213/translate-a-column-index-into-an-excel-column-name
        /// </summary>
        public static string GetColumnAddress(long value)
        {
            char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            value -= 1; //adjust so it matches 0-indexed array rather than 1-indexed column
            long quotient = value / 26;
            if (quotient > 0)
                return GetColumnAddress(quotient) + chars[value % 26].ToString();
            else
                return chars[value % 26].ToString();
        }

        /// <summary>
        /// Convert the column address to 26 based integer
        /// </summary>
        /// <param name="columnAddress"></param>
        /// <returns></returns>
        public static long ConvertColumnValue(string columnAddress)
        {
            long result = 0;
            string abjad = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (int i = 0; i < columnAddress.Length; i++)
            {
                result += (abjad.IndexOf(columnAddress[columnAddress.Length - (i + 1)].ToString()) + 1) * Convert.ToInt64(Math.Pow(26, i));
            }
            return result;
        }


        public static bool IsNullableType(this Type type)
        {
            return type.IsGenericType
            && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        public static DateTime ConvertDateTime(object value)
        {
            DateTime result;
            if (value is double)
            {
                result = DateTime.FromOADate((double)value);
            }
            else
            {
                DateTime.TryParse(value.ToString(), out result);
            }
            return result;
        }

        public static bool IsConvertibleToDateTime(object value)
        {
            bool result = false;
            DateTime date;
            try
            {
                if (value == null)
                {
                    result = false;
                }
                else if (value is double)
                {
                    date = DateTime.FromOADate((double)value);
                    result = true;
                }
                else
                {
                    result = DateTime.TryParse(value.ToString(), out date);
                }
            }
            catch { }
            return result;
        }

        public static void LogError(Exception e, string headerMessage = "")
        {
            try
            {
                string logPath = string.Format(@"{0}\{1}", Directory.GetCurrentDirectory(), "LogApplication.log");
                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    string message = string.Empty;
                    message += "===========================================================================" + Environment.NewLine;
                    message += DateTime.Now.ToString("dd MMMM yyyy hh:mm tt") + Environment.NewLine;
                    message += "Found Error in Application" + Environment.NewLine;
                    message += string.IsNullOrEmpty(headerMessage) ? string.Empty : string.Format("Header message is {0}{1}", headerMessage, Environment.NewLine);
                    message += string.Format("Message is {0}.{1}", e.Message, Environment.NewLine);
                    message += string.Format("Stacktrace is {0}.{1}", e.StackTrace, Environment.NewLine);
                    if (e.InnerException != null)
                    {
                        message += "Inner Exception: " + Environment.NewLine;
                        message += string.Format("  Message is {0}.{1}", e.InnerException.Message, Environment.NewLine);
                        message += string.Format("  Stacktrace is {0}.{1}", e.InnerException.StackTrace, Environment.NewLine);
                    }
                    message += "===========================================================================" + Environment.NewLine;
                    writer.Write(message);
                }
            }
            catch (Exception) { }
        }
    }
}
