﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.Samples.EntityDataReader;

namespace SMI.LOSDeposito.Helper.SQLBulkInsert
{
   public class SqlBulkHelper<T>
    {
       string connectionString = string.Empty;
       string tableName = string.Empty;
       
       public SqlBulkHelper(){}

       public SqlBulkHelper(string _connectionString, string _tableName)
       {
           connectionString = _connectionString;
           tableName = _tableName; 
       }

       public void InsertManyItem(List<T> type)
       {
           SqlBulkCopy bulkCopy = new SqlBulkCopy(connectionString);
           bulkCopy.DestinationTableName = tableName;
           bulkCopy.WriteToServer(type.AsDataReader());
           bulkCopy.Close();
       }

    }
}
