﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using SMI.Monitoring.Helper.ExcelHelper.ValidateAction;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.Domain.Attributes;

namespace SMI.Monitoring.Helper.ExcelHelper
{
    public class ReadExceHelper<T> : IDisposable where T : class
    {
        protected string sheetName = string.Empty;
        protected List<T> uploadData;
        protected Dictionary<string, string> errorMessage;
        protected Dictionary<Type, IList<EntityParameterModel>> entitiesParameter;
        protected Dictionary<string, Func<string>> validateActionCollections;
        private string strConnExcel2003 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1;\"";
        private string strConnExcel2007 = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=Yes;IMEX=1;\"";

        public ReadExceHelper() { }

        private string GetExcelFieldAttribute(PropertyInfo property)
        {
            ExcelFieldAttribute attribute = (ExcelFieldAttribute)property.GetCustomAttributes(typeof(ExcelFieldAttribute), true).FirstOrDefault();
            return attribute == null ? null : attribute.Name;
        }

        private ForeignKeyAttribute GetForeignKeyAttribute(PropertyInfo property)
        {
            ForeignKeyAttribute attribute = (ForeignKeyAttribute)property.GetCustomAttributes(typeof(ForeignKeyAttribute), true).FirstOrDefault();
            return attribute == null ? null : attribute;
        }

        private bool IsErrorMessageAttributeProperty(PropertyInfo property)
        {
            return (ErrorMessageAttribute)property.GetCustomAttributes(typeof(ErrorMessageAttribute), true).FirstOrDefault() != null ? true : false;
        }

        /// <summary>
        /// Handle reading excel worksheet and cast it into an object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        protected T ObjectMapper(T obj, DbDataReader dataReader)
        {
            Type originalEntityType = obj.GetType();
            PropertyInfo[] sourceprop = originalEntityType.GetProperties();
            foreach (PropertyInfo pi in sourceprop)
            {
                try
                {
                    PropertyInfo property = originalEntityType.GetProperty(pi.Name);
                    string excelField = GetExcelFieldAttribute(property);
                    if (!String.IsNullOrEmpty(excelField))
                    {
                        object value = this.GetValue(dataReader[excelField]);
                        //check if this is a foreign key property
                        ForeignKeyAttribute foreignKey = GetForeignKeyAttribute(property);
                        if (foreignKey != null)
                        {
                            var result = entitiesParameter.Where(x => x.Key == foreignKey.Entity).FirstOrDefault();
                            if (result.Key != null)
                            {
                                IList<EntityParameterModel> parameterEntity = result.Value;
                                EntityParameterModel item = parameterEntity.Where(x => x.Value.Equals(value.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                                if (item != null)
                                {
                                    value = item.Id;
                                }
                                else
                                {
                                    if (!property.PropertyType.IsNullableType())
                                        throw new InvalidDataException("Found invalid data. The given value for field " + excelField + " didn't match with parameter from " + foreignKey.Entity.FullName);
                                }
                            }
                            else throw new InvalidOperationException("Cannot find parameter entity for Type " + foreignKey.Entity.FullName);
                        }
                        //handle nullable type
                        Type targetType = property.PropertyType.IsNullableType() ? Nullable.GetUnderlyingType(property.PropertyType) : property.PropertyType;
                        var convertedValue = (value == null ? null : Convert.ChangeType(value, targetType));
                        originalEntityType.GetProperty(pi.Name).SetValue(obj, convertedValue, null);
                    }
                }
                catch (NullReferenceException) { }
            }
            return obj;
        }

        private List<string> GetListWorkSheet(OleDbConnection connection )
        {
            if (connection != null || connection.State != ConnectionState.Open) throw new Exception("The current excel conenction is null or not ready.");
            List<string> listSheet = new List<string>();
            DataTable myTableName = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            foreach (DataRow drSheet in myTableName.Rows)
            {
                if (drSheet["TABLE_NAME"].ToString().Contains("$"))//checks whether row contains '_xlnm#_FilterDatabase' or sheet name(i.e. sheet name always ends with $ sign)
                {
                    listSheet.Add(drSheet["TABLE_NAME"].ToString());
                }
            }
            return listSheet;
        }

        public virtual List<T> ReadExceFile(string filename, ParseMode mode = ParseMode.Default)
        {
            FileInfo file = new FileInfo(filename);
            if (!file.Exists) throw new Exception("Missing file reference.");
            if (!(file.Extension.ToLower() == ".xls" || file.Extension.ToLower() == ".xlsx")) throw new Exception("File is not valid. Must be an Excel file");
            OleDbConnection connection = new OleDbConnection();
            OleDbCommand command = new OleDbCommand();
            OleDbDataReader reader;
            if (file.Extension.ToLower() == ".xls")
            {
                connection = new OleDbConnection(string.Format(strConnExcel2003, filename));
            }
            else connection = new OleDbConnection(string.Format(strConnExcel2007, filename));

            this.uploadData = new List<T>();
            try
            {
                connection.Open();
                //DataTable myTableName = connection.GetSchema("Tables");
                string workSheet = String.Format("SELECT * FROM [{0}]", "Data$");
                command = new OleDbCommand(workSheet, connection);
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        var item = (T)Activator.CreateInstance(typeof(T));
                        ObjectMapper(item, reader);
                        this.uploadData.Add(item);
                    }
                    catch (FormatException) { }
                }
                return this.uploadData;
            }
            catch (Exception e)
            {
                string errorMessage = string.Format("Error: {0}{1}Stacktrace: {2}", e.Message, Environment.NewLine, e.StackTrace);
                return this.uploadData;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        public void SendError(string field, string message)
        {
            if (errorMessage == null) errorMessage = new Dictionary<string, string>();
            errorMessage.Add(field, message);
        }

        public void RegisterValidateAction(IValidation action)
        {
            RegisterValidateAction(string.Empty, action);
        }

        public void RegisterValidateAction(string fieldName, IValidation action)
        {
            if (!string.IsNullOrEmpty(fieldName))
            {

            }
            else { }
        }

        public virtual void RegisterEntityParameter(Type entityType, IList<EntityParameterModel> entityParamater)
        {
            if (entitiesParameter == null) entitiesParameter = new Dictionary<Type, IList<EntityParameterModel>>();
            entitiesParameter.Add(entityType, entityParamater);
        }

        protected string clearValue(string value)
        {
            value = value.Trim();
            if (!string.IsNullOrEmpty(value))
            {
                if (value.ToLower() == "null")
                {
                    value = string.Empty;
                }
            }
            return value;
        }

        private object GetValue(object oreader)
        {
            return (oreader == DBNull.Value ? null : oreader);
        }

        public void Dispose()
        {
            this.uploadData = null;
        }
    }

    public enum ParseMode
    {
        Default = 0,
        Custom
    }
}
