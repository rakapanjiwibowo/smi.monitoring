﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telerik.Windows.Documents.Spreadsheet.Model;

namespace SMI.Monitoring.Helper.TelerikSpreadsheetHelper
{
    public static class Helper
    {
        static Helper() { }

        /// <summary>
        /// Check if string is convertible to numeric value
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public static bool isNumeric(this string value)
        {
            long longVal = 0;
            decimal decimalVal = 0;
            return (!string.IsNullOrEmpty(value) && (long.TryParse(value, out longVal) || decimal.TryParse(value, out decimalVal)));
        }

        /// <summary>
        /// Get interval row for specific cell range address.
        /// For range address only
        /// </summary>
        /// <param name="rangeAddress"></param>
        /// <returns></returns>
        public static int IntervalRows(this string rangeAddress)
        {
            var address = rangeAddress.Split(':');
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match1 = RegexCellAddress.Match(address[0]);
            string numericAddress1 = match1.Groups["Numeric"].Value;
            var match2 = RegexCellAddress.Match(address[1]);
            string numericAddress2 = match2.Groups["Numeric"].Value;
            return Math.Abs(Convert.ToInt32(numericAddress1) - Convert.ToInt32(numericAddress2));
        }

        public static int IntervalColumns(this string rangeAddress)
        {
            throw new NotImplementedException("Method Helper.IntervalColumns not implemented yet");
        }

        /// <summary>
        /// Set column address value for cell address
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string SetColumnAddress(this string cellAddress, string column)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else if (column.IndexOf("0123456789") >= 0)
            {
                throw new Exception("Invalid value. Column address cannot contain numeric value.");
            }
            else
            {
                return string.Format("{0}{1}", column, numericAddress);
            }
        }

        public static string GetColumnAddress(this string cellAddress)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            return wordAdress;
        }


        public static long GetRowAddress(this string cellAddress)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            return long.Parse(numericAddress);
        }

        /// <summary>
        /// Set row address value for cell address
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static string SetRowAddress(this string cellAddress, int row)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else if (row <= 0)
            {
                throw new Exception("Invalid value. Row address must be greater than zero.");
            }
            else
            {
                return string.Format("{0}{1}", wordAdress, row);
            }
        }

        public static string ConvertToFixCellAddress(this string cellAddress, bool isColumn, bool isRow)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else
            {
                if (isColumn) wordAdress = "$"+ wordAdress;
                if (isRow) numericAddress = "$"+ numericAddress;

                return string.Format("{0}{1}", wordAdress, numericAddress);
            }
        }

        /// <summary>
        /// Add specific column for cell address.
        /// For cell address only
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <returns></returns>
        public static string MoveColumns(this string cellAddress, int total)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else
            {
                long tempValue = ConvertColumnValue(wordAdress);
                if ((tempValue + total) <= 0)
                {
                    throw new Exception("Invalid value, unable to move cell address to specific columns. The minimum cell address column value must be 'A'");
                }
                else
                {
                    string newWordAddress = GetColumnAddress(tempValue + total);
                    return string.Format("{0}{1}", newWordAddress, numericAddress);
                }
            }
        }


        /// <summary>
        /// Add specific column for cell address.
        /// For cell address only
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <returns></returns>
        public static string MoveRows(this string cellAddress, int total)
        {
            Regex RegexCellAddress = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = RegexCellAddress.Match(cellAddress);

            string wordAdress = match.Groups["Alpha"].Value;
            string numericAddress = match.Groups["Numeric"].Value;
            if (string.IsNullOrEmpty(wordAdress) || string.IsNullOrEmpty(numericAddress))
            {
                throw new Exception("Invalid cell address. Cell address must contains words and number. ie: [A1]");
            }
            else
            {
                int newNumericAddress = Convert.ToInt32(numericAddress) + total;
                return string.Format("{0}{1}", wordAdress, newNumericAddress);
            }
        }

        /// <summary>
        /// Move cell address to specific rows and columns.
        /// For cell address only
        /// </summary>
        /// <param name="cellAddress"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static string MoveRowsAndColumns(this string cellAddress, int rows, int columns)
        {
            string tempAddress = MoveRows(cellAddress, rows);
            tempAddress = MoveColumns(tempAddress, columns);
            return tempAddress;
        }

        /// <summary>
        /// Move cell range address (ie:AA13:BH65) to specific rows and columns.
        /// For range address only, ie [A21:B45]
        /// </summary>
        /// <param name="cellRangeAddress"></param>
        /// <param name="row1"></param>
        /// <param name="column1"></param>
        /// <param name="row2"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        public static string MoveRowsAndColumns(this string cellRangeAddress, int row1, int column1, int row2, int column2)
        {
            //validate the range address value first..
            if (string.IsNullOrEmpty(cellRangeAddress))
            {
                throw new Exception("Invalid value for cell range address. Value cannot empty.");
            }
            else
            {
                var temp = cellRangeAddress.Split(':');
                if (temp.Count() != 2 || string.IsNullOrEmpty(temp[0]) || string.IsNullOrEmpty(temp[1]))
                {
                    throw new Exception("Invalid value for cell range address. The current format valus is not valid ([" + cellRangeAddress + "]). Format value must be, for ie: [A21:B65]");
                }
                else
                {
                    string result = string.Format("{0}:{1}", temp[0].MoveRowsAndColumns(row1, column1), temp[1].MoveRowsAndColumns(row2, column2));
                    return result;
                }
            }
        }

        /// <summary>
        /// See @ http://stackoverflow.com/questions/297213/translate-a-column-index-into-an-excel-column-name
        /// </summary>
        public static string GetColumnAddress(long value)
        {
            char[] chars = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            value -= 1; //adjust so it matches 0-indexed array rather than 1-indexed column
            long quotient = value / 26;
            if (quotient > 0)
                return GetColumnAddress(quotient) + chars[value % 26].ToString();
            else
                return chars[value % 26].ToString();
        }

        /// <summary>
        /// Convert the column address to 26 based integer
        /// </summary>
        /// <param name="columnAddress"></param>
        /// <returns></returns>
        public static long ConvertColumnValue(string columnAddress)
        {
            long result = 0;
            string abjad = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            for (int i = 0; i < columnAddress.Length; i++)
            {
                result += (abjad.IndexOf(columnAddress[columnAddress.Length - (i + 1)].ToString()) + 1) * Convert.ToInt64(Math.Pow(26, i));
            }
            return result;
        }

        /// <summary>
        /// Convert string based cell address to array cell index
        /// Ex: 'A2'
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CellIndex ToCellIndex(this string value)
        {
            var columnIndex = (int)ConvertColumnValue(value.GetColumnAddress());
            var rowIndex = (int)value.GetRowAddress();
            return new CellIndex(rowIndex - 1, columnIndex - 1);
        }

        /// <summary>
        /// Convert string based cell range address to array cell range
        /// Ex: 'A2:B6'
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static CellRange ToCellRange(this string value)
        {
            var temp = value.Split(':');
            if (temp.Count() == 2)
            {
                var fromCellAddress = temp[0];
                var toCellAddress = temp[1];
                var fromColumnIndex = (int)ConvertColumnValue(fromCellAddress.GetColumnAddress());
                var fromRowIndex = (int)fromCellAddress.GetRowAddress();
                var toColumnIndex = (int)ConvertColumnValue(toCellAddress.GetColumnAddress());
                var toRowIndex = (int)toCellAddress.GetRowAddress();
                 var fromCellIndex = new CellIndex(fromRowIndex - 1, fromColumnIndex - 1);
                var toCellIndex = new CellIndex(toRowIndex - 1, toColumnIndex - 1);
                return new CellRange(fromCellIndex, toCellIndex );
            }
            else
            {
                throw new Exception("Invalid format cell address. Must be for example: A2:D5");
            }
        }

        public static double GetSmaller(this double value, double divider)
        {
            if(value != 0 && divider > 0)
            {
                return value / divider;
            }
            return value;
        }


        public static double GetSmaller(this double? value, double divider)
        {
            if (value.GetValueOrDefault(0) != 0 && divider > 0)
            {
                return value.Value / divider;
            }
            return value.GetValueOrDefault(0);
        }


        public static decimal GetSmaller(this decimal value, decimal divider)
        {
            if (value != 0 && divider > 0)
            {
                return value / divider;
            }
            return value;
        }


        public static decimal GetSmaller(this decimal? value, decimal divider)
        {
            if (value.GetValueOrDefault(0) != 0 && divider > 0)
            {
                return value.Value / divider;
            }
            return value.GetValueOrDefault(0);
        }

        public static bool IsNullableType(this Type type)
        {
            return type.IsGenericType
            && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));
        }

        public static DateTime ConvertDateTime(object value)
        {
            DateTime result;
            if (value is double)
            {
                result = DateTime.FromOADate((double)value);
            }
            else
            {
                DateTime.TryParse(value.ToString(), out result);
            }
            return result;
        }

        public static bool IsConvertibleToDateTime(object value)
        {
            bool result = false;
            DateTime date;
            try
            {
                if (value == null)
                {
                    result = false;
                }
                else if (value is double)
                {
                    date = DateTime.FromOADate((double)value);
                    result = true;
                }
                else
                {
                    result = DateTime.TryParse(value.ToString(), out date);
                }
            }
            catch { }
            return result;
        }

        public static void LogError(Exception e, string headerMessage = "")
        {
            try
            {
                string logPath = string.Format(@"{0}\{1}", Directory.GetCurrentDirectory(), "LogApplication.log");
                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    string message = string.Empty;
                    message += "===========================================================================" + Environment.NewLine;
                    message += DateTime.Now.ToString("dd MMMM yyyy hh:mm tt") + Environment.NewLine;
                    message += "Found Error in Application" + Environment.NewLine;
                    message += string.IsNullOrEmpty(headerMessage) ? string.Empty : string.Format("Header message is {0}{1}", headerMessage, Environment.NewLine);
                    message += string.Format("Message is {0}.{1}", e.Message, Environment.NewLine);
                    message += string.Format("Stacktrace is {0}.{1}", e.StackTrace, Environment.NewLine);
                    if (e.InnerException != null)
                    {
                        message += "Inner Exception: " + Environment.NewLine;
                        message += string.Format("  Message is {0}.{1}", e.InnerException.Message, Environment.NewLine);
                        message += string.Format("  Stacktrace is {0}.{1}", e.InnerException.StackTrace, Environment.NewLine);
                    }
                    message += "===========================================================================" + Environment.NewLine;
                    writer.Write(message);
                }
            }
            catch (Exception) { }
        }
    }
}
