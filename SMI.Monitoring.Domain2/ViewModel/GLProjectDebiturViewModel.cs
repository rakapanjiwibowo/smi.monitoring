﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class GLProjectDebiturViewModel
    {
        public string INT_SOURCE_ID { get; set; }
        public decimal INT_BATCH_ID { get; set; }
        public string INT_STAGING_ID { get; set; }
        public string DEBITUR_CODE { get; set; }
        public string DEBITUR_DESC { get; set; }
        public string CREATED_BY { get; set; }
        public System.DateTime CREATION_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public Nullable<decimal> INT_REF_ID { get; set; }
        public Nullable<System.DateTime> INTERFACE_DATE { get; set; }
        public string INT_STATUS { get; set; }
        public Nullable<decimal> INT_REQUEST_ID { get; set; }

        #region Tambahan
        public string SourceName { get; set; }

        public string INT_SOURCE_NAME { get; set; }
        public string INT_NAME { get; set; }
        public string MODUL { get; set; }
        public string SOURCE_VALUE { get; set; }
        #endregion
    }
}
