﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class sgt_ap_invoice_stagingViewModel
    {
        public decimal int_batch_id { get; set; }
        public decimal int_staging_id { get; set; }
        public string dist_code_combination { get; set; }
        public string dist_code_description { get; set; }
        public Nullable<decimal> line_number { get; set; }
        public string invoice_batch { get; set; }
        public string invoice_no { get; set; }
        public string invoice_description { get; set; }
        public string line_description { get; set; }
        public Nullable<System.DateTime> invoice_date { get; set; }
        public Nullable<System.DateTime> gl_date { get; set; }
        public Nullable<System.DateTime> terms_date { get; set; }
        public string invoice_type { get; set; }
        public Nullable<double> dist_amount { get; set; }
        public string currency_code { get; set; }
        public Nullable<double> currency_rate { get; set; }
        public Nullable<System.DateTime> rate_date { get; set; }
        public string rate_type { get; set; }
        public Nullable<decimal> vendor_id { get; set; }
        public Nullable<decimal> vendor_site_id { get; set; }
        public Nullable<decimal> org_id { get; set; }
        public string BATCH_ATTRIBUTE1 { get; set; }
        public string INVOICE_ATTRIBUTE1 { get; set; }
        public string LINE_ATTRIBUTE1 { get; set; }
        public string LINE_ATTRIBUTE2 { get; set; }
        public string LINE_ATTRIBUTE3 { get; set; }
    }
}
