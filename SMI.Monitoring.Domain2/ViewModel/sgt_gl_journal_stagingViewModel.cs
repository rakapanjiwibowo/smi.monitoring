﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
    public class sgt_gl_journal_stagingViewModel
    {
        public decimal int_batch_id { get; set; }
        public decimal int_staging_id { get; set; }
        public Nullable<System.DateTime> gl_date { get; set; }
        public string currency_code { get; set; }
        public Nullable<double> currency_rate { get; set; }
        public Nullable<System.DateTime> rate_date { get; set; }
        public string rate_type { get; set; }
        public string code_combination { get; set; }
        public string code_description { get; set; }
        public Nullable<double> dr_amount { get; set; }
        public Nullable<double> cr_amount { get; set; }
        public string batch_name { get; set; }
        public string journal_name { get; set; }
        public string journal_category { get; set; }
        public string line_description { get; set; }
        public string actual_flag { get; set; }
        public Nullable<decimal> ledger_id { get; set; }
    }
}
