﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class ARInvoiceLogViewModel
    {
        public string INT_SOURCE_ID { get; set; }
        public decimal INT_BATCH_ID { get; set; }
        public string INT_STAGING_ID { get; set; }
        public string BATCH_SOURCE_NAME { get; set; }
        public Nullable<long> SET_OF_BOOKS_ID { get; set; }
        public Nullable<long> ORG_ID { get; set; }
        public string CURRENCY_CODE { get; set; }
        public Nullable<long> CUST_TRX_TYPE_ID { get; set; }
        public Nullable<long> ORIG_SYSTEM_BILL_CUSTOMER_ID { get; set; }
        public Nullable<long> ORIG_SYSTEM_BILL_ADDRESS_ID { get; set; }
        public Nullable<long> TERM_ID { get; set; }
        public string CONVERSION_TYPE { get; set; }
        public Nullable<System.DateTime> CONVERSION_DATE { get; set; }
        public Nullable<decimal> CONVERSION_RATE { get; set; }
        public Nullable<long> CUSTOMER_TRX_ID { get; set; }
        public Nullable<System.DateTime> TRX_DATE { get; set; }
        public Nullable<System.DateTime> GL_DATE { get; set; }
        public Nullable<long> DOCUMENT_NUMBER { get; set; }
        public string TRX_NUMBER { get; set; }
        public Nullable<long> LINE_NUMBER { get; set; }
        public Nullable<long> INVENTORY_ITEM_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<decimal> QUANTITY { get; set; }
        public Nullable<decimal> UNIT_SELLING_PRICE { get; set; }
        public string TAX_CODE { get; set; }
        public Nullable<long> CODE_COMBINATION_ID { get; set; }
        public Nullable<decimal> PERCENT { get; set; }
        public string INVOICING_RULE_NAME { get; set; }
        public string ACCOUNTING_RULE_NAME { get; set; }
        public Nullable<long> ACCOUNTING_RULE_DURATION { get; set; }
        public Nullable<System.DateTime> RULE_START_DATE { get; set; }
        public Nullable<long> PRIMARY_SALESREP_ID { get; set; }
        public string HEADER_ATTRIBUTE_CATEGORY { get; set; }
        public string HEADER_ATTRIBUTE1 { get; set; }
        public string HEADER_ATTRIBUTE2 { get; set; }
        public string HEADER_ATTRIBUTE3 { get; set; }
        public string HEADER_ATTRIBUTE4 { get; set; }
        public string HEADER_ATTRIBUTE5 { get; set; }
        public string LINE_CONTEXT { get; set; }
        public string LINE_ATTRIBUTE1 { get; set; }
        public string LINE_ATTRIBUTE2 { get; set; }
        public string LINE_ATTRIBUTE3 { get; set; }
        public string LINE_ATTRIBUTE4 { get; set; }
        public string LINE_ATTRIBUTE5 { get; set; }
        public string DIST_ATTRIBUTE_CATEGORY { get; set; }
        public string DISTRIBUTION_ATTRIBUTE1 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE2 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE3 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE4 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE5 { get; set; }
        public System.DateTime CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public Nullable<decimal> INT_REF_ID { get; set; }
        public Nullable<System.DateTime> INTERFACE_DATE { get; set; }
        public string INT_STATUS { get; set; }
        public Nullable<decimal> INT_REQUEST_ID { get; set; }
        public Nullable<decimal> IMPORT_REQUEST_ID { get; set; }
        public Nullable<decimal> AMOUNT { get; set; }
        public string CODE_COMBINATION { get; set; }
        public string REC_CODE_COMBINATION { get; set; }
        public Nullable<decimal> REC_CCID { get; set; }
        public Nullable<decimal> INT_INVOICE_LINE_ID { get; set; }
        public Nullable<decimal> REF_LINE_ID { get; set; }

        #region Tambahan
        public string SourceName { get; set; }
        public string Status { get; set; }
        public string INT_SOURCE_NAME { get; set; }
        public string INT_NAME { get; set; }
        public string MODUL { get; set; }
        public string SOURCE_VALUE { get; set; }
        #endregion
    }
}
