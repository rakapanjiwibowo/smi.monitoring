﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
    public class Int_Validation_LogViewModel
    {
      
        public string INT_SOURCE_ID { get; set; }

        public decimal INT_BATCH_ID { get; set; }

        public decimal INT_STAGING_ID { get; set; }

        public decimal INT_LOG_SEQ { get; set; }

        public string VALIDATION_LOG { get; set; }

        public string CREATED_BY { get; set; }

        public DateTime? CREATION_DATE { get; set; }

        public string LAST_UPDATED_BY { get; set; }

        public DateTime? LAST_UPDATE_DATE { get; set; }

        public decimal? INT_REQUEST_ID { get; set; }
    }
}
