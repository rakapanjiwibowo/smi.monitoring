﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class APSuplierLogViewModel
    {

        public string INT_SOURCE_ID { get; set; }
        public decimal INT_BATCH_ID { get; set; }
        public string INT_STAGING_ID { get; set; }
        public string VENDOR_NAME { get; set; }
        public string VENDOR_NAME_ALT { get; set; }
        public string VAT_REGISTRATION_NUM { get; set; }
        public string VENDOR_SITE_CODE { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string ADDRESS_LINE4 { get; set; }
        public string CITY { get; set; }
        public string COUNTY { get; set; }
        public string STATE { get; set; }
        public string PROVINCE { get; set; }
        public string ZIP { get; set; }
        public string COUNTRY { get; set; }
        public string ALLOW_AWT_FLAG { get; set; }
        public string AREA_CODE { get; set; }
        public string PHONE { get; set; }
        public string FAX_AREA_CODE { get; set; }
        public string FAX { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public Nullable<decimal> ORG_ID { get; set; }
        public string ATTRIBUTE_CATEGORY { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public string ATTRIBUTE6 { get; set; }
        public string ATTRIBUTE7 { get; set; }
        public string ATTRIBUTE8 { get; set; }
        public string ATTRIBUTE9 { get; set; }
        public string PURCHASING_SITE_FLAG { get; set; }
        public string PAY_SITE_FLAG { get; set; }
        public string ADDRESS_ATTRIBUTE_CATEGORY { get; set; }
        public string ADDRESS_ATTRIBUTE1 { get; set; }
        public string ADDRESS_ATTRIBUTE2 { get; set; }
        public string ADDRESS_ATTRIBUTE3 { get; set; }
        public string ADDRESS_ATTRIBUTE4 { get; set; }
        public string ADDRESS_ATTRIBUTE5 { get; set; }
        public string ADDRESS_ATTRIBUTE6 { get; set; }
        public string ADDRESS_ATTRIBUTE7 { get; set; }
        public string ADDRESS_ATTRIBUTE8 { get; set; }
        public string ADDRESS_ATTRIBUTE9 { get; set; }
        public string ADDRESS_ATTRIBUTE10 { get; set; }
        public string CONTACT_FIRST_NAME { get; set; }
        public string CONTACT_LAST_NAME { get; set; }
        public string CONTACT_AREA_CODE { get; set; }
        public string CONTACT_PHONE_NUMBER { get; set; }
        public string CONTACT_EMAIL_ADDRESS { get; set; }
        public Nullable<long> BANK_ID { get; set; }
        public Nullable<long> BRANCH_ID { get; set; }
        public string CURRENCY_CODE { get; set; }
        public string BANK_ACCOUNT_NUM { get; set; }
        public string BANK_ACCOUNT_NAME { get; set; }
        public System.DateTime CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public Nullable<decimal> INT_REF_ID { get; set; }
        public Nullable<System.DateTime> INTERFACE_DATE { get; set; }
        public Nullable<decimal> INT_REQUEST_ID { get; set; }
        public Nullable<decimal> IMPORT_REQUEST_ID { get; set; }
        public string INT_STATUS { get; set; }
        public string UPLOAD_TYPE { get; set; }
        public string CONTACT_MIDDLE_NAME { get; set; }
        public string VENDOR_TYPE_LOOKUP_CODE { get; set; }
        public string RFQ_ONLY_SITE_FLAG { get; set; }
        public Nullable<decimal> ACCTS_PAY_CODE_COMBINATION_ID { get; set; }
        public Nullable<decimal> PREPAY_CODE_COMBINATION_ID { get; set; }
        public string PAYMENT_METHOD_LOOKUP_CODE { get; set; }
        public string FREIGHT_TERMS_LOOKUP_CODE { get; set; }
        public Nullable<decimal> BILL_TO_LOCATION_ID { get; set; }
        public string BILL_TO_LOCATION_CODE { get; set; }
        public Nullable<decimal> SHIP_TO_LOCATION_ID { get; set; }
        public string SHIP_TO_LOCATION_CODE { get; set; }
        public string SHIP_VIA_LOOKUP_CODE { get; set; }
        public Nullable<System.DateTime> INACTIVE_DATE { get; set; }

        #region Tambahan
        public string SourceName { get; set; }
        public string Status { get; set; }
        public string INT_SOURCE_NAME { get; set; }
        public string INT_NAME { get; set; }
        public string MODUL { get; set; }
        public string SOURCE_VALUE { get; set; }
        #endregion
    }
}
