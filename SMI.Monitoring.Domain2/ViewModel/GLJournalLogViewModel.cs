﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class GLJournalLogViewModel
    {
        public string INT_SOURCE_ID { get; set; }
        public decimal INT_BATCH_ID { get; set; }
        public string INT_STAGING_ID { get; set; }
        public System.DateTime GL_DATE { get; set; }
        public string CURRENCY_CODE { get; set; }
        public Nullable<double> CURRENCY_RATE { get; set; }
        //public Nullable<System.DateTime> RATE_DATE { get; set; }

        public string RATE_TYPE { get; set; }
        public string CODE_COMBINATION { get; set; }
        public Nullable<double> DR_AMOUNT { get; set; }
        public Nullable<double> CR_AMOUNT { get; set; }
        public string BATCH_NAME { get; set; }
        public string JOURNAL_NAME { get; set; }
        public string JOURNAL_CATEGORY { get; set; }
        public string LINE_DESCRIPTION { get; set; }
        public Nullable<long> INT_GROUP_ID { get; set; }
        public string INT_STATUS { get; set; }
        public string ACTUAL_FLAG { get; set; }
        public System.DateTime CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public Nullable<long> CCID { get; set; }
        public Nullable<long> JE_BATCH_ID { get; set; }
        public Nullable<long> JE_HEADER_ID { get; set; }
        public Nullable<long> JE_LINE_NUM { get; set; }
        public Nullable<long> INT_REQUEST_ID { get; set; }
        public Nullable<System.DateTime> INTERFACE_DATE { get; set; }
        public Nullable<long> INT_REF_ID { get; set; }
        public decimal LEDGER_ID { get; set; }
        public string JOURNAL_ATTRIBUTE1 { get; set; }
        public string JOURNAL_ATTRIBUTE2 { get; set; }
        public string JOURNAL_ATTRIBUTE3 { get; set; }
        public string JOURNAL_ATTRIBUTE4 { get; set; }
        public string JOURNAL_ATTRIBUTE5 { get; set; }
        public string BATCH_ATTRIBUTE1 { get; set; }
        public string BATCH_ATTRIBUTE2 { get; set; }
        public string BATCH_ATTRIBUTE3 { get; set; }
        public string BATCH_ATTRIBUTE4 { get; set; }
        public string BATCH_ATTRIBUTE5 { get; set; }
        public Nullable<long> IMPORT_REQUEST_ID { get; set; }
        public string BATCH_CONTEXT { get; set; }
        public string JOURNAL_CONTEXT { get; set; }
        public string PERIOD_NAME { get; set; }
        public string JOURNAL_SOURCE { get; set; }
        public Nullable<long> ENCUMBRANCE_TYPE_ID { get; set; }
        public Nullable<long> BUDGET_VERSION_ID { get; set; }
        public Nullable<long> LAST_UPDATE_LOGIN { get; set; }
        public string POSTED_FLAG { get; set; }


        #region Tambahan
        public string SourceName { get; set; }
        public string Status { get; set; }
        public string INT_SOURCE_NAME { get; set; }
        public string INT_NAME { get; set; }
        public string MODUL { get; set; }
        public string SOURCE_VALUE { get; set; }
        #endregion
    }
}
