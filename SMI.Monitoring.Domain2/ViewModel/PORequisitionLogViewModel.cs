﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class PORequisitionLogViewModel
    {
        public string INT_SOURCE_ID { get; set; }
        public decimal INT_BATCH_ID { get; set; }
        public string INT_STAGING_ID { get; set; }
        public string REQUISITION_TYPE { get; set; }
        public string HEADER_DESCRIPTION { get; set; }
        public Nullable<int> PREPARER_ID { get; set; }
        public Nullable<decimal> LINE_NUM { get; set; }
        public Nullable<decimal> CATEGORY_ID { get; set; }
        public string LINE_TYPE { get; set; }
        public Nullable<decimal> ITEM_ID { get; set; }
        public string ITEM_DESCRIPTION { get; set; }
        public string UNIT_OF_MEASURE { get; set; }
        public Nullable<decimal> QUANTITY { get; set; }
        public string CURRENCY_CODE { get; set; }
        public Nullable<decimal> UNIT_PRICE { get; set; }
        public Nullable<System.DateTime> NEED_BY_DATE { get; set; }
        public string DESTINATION_TYPE { get; set; }
        public Nullable<decimal> DELIVER_TO_REQUESTOR_ID { get; set; }
        public Nullable<decimal> ORG_ID { get; set; }
        public Nullable<decimal> DELIVER_TO_LOCATION_ID { get; set; }
        public string CHARGE_ACCOUNT { get; set; }
        public string BUDGET_ACCOUNT { get; set; }
        public string ACCRUAL_ACCOUNT { get; set; }
        public string VARIANCE_ACCOUNT { get; set; }
        public Nullable<decimal> RATE { get; set; }
        public Nullable<System.DateTime> RATE_DATE { get; set; }
        public string RATE_TYPE { get; set; }
        public Nullable<decimal> SUGGESTED_VENDOR_ID { get; set; }
        public Nullable<decimal> SUGGESTED_VENDOR_SITE_ID { get; set; }
        public string PREVENT_ENCUMBRANCE_FLAG { get; set; }
        public string HEADER_ATTRIBUTE_CATEGORY { get; set; }
        public string HEADER_ATTRIBUTE1 { get; set; }
        public string HEADER_ATTRIBUTE2 { get; set; }
        public string HEADER_ATTRIBUTE3 { get; set; }
        public string HEADER_ATTRIBUTE4 { get; set; }
        public string HEADER_ATTRIBUTE5 { get; set; }
        public string LINE_ATTRIBUTE_CATEGORY { get; set; }
        public string LINE_ATTRIBUTE1 { get; set; }
        public string LINE_ATTRIBUTE2 { get; set; }
        public string LINE_ATTRIBUTE3 { get; set; }
        public string LINE_ATTRIBUTE4 { get; set; }
        public string LINE_ATTRIBUTE5 { get; set; }
        public string DIST_ATTRIBUTE_CATEGORY { get; set; }
        public string DISTRIBUTION_ATTRIBUTE1 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE2 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE3 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE4 { get; set; }
        public string DISTRIBUTION_ATTRIBUTE5 { get; set; }
        public System.DateTime CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public Nullable<decimal> INT_REF_ID { get; set; }
        public Nullable<System.DateTime> INTERFACE_DATE { get; set; }
        public string INT_STATUS { get; set; }
        public Nullable<decimal> INT_REQUEST_ID { get; set; }
        public Nullable<decimal> IMPORT_REQUEST_ID { get; set; }
        public string PR_NUMBER { get; set; }
        public string CANCEL_FLAG { get; set; }
        public Nullable<decimal> CHARGE_ACCOUNT_ID { get; set; }
        public Nullable<decimal> BUDGET_ACCOUNT_ID { get; set; }
        public Nullable<decimal> ACCRUAL_ACCOUNT_ID { get; set; }
        public Nullable<decimal> VARIANCE_ACCOUNT_ID { get; set; }
        public Nullable<decimal> LINE_TYPE_ID { get; set; }
        public Nullable<decimal> DIST_NUM { get; set; }
        public Nullable<decimal> DIST_QUANTITY { get; set; }
        public Nullable<System.DateTime> GL_DATE { get; set; }
        public Nullable<decimal> IMPORT_TRX_ID { get; set; }

        #region Tambahan
        public string SourceName { get; set; }
        public string Status { get; set; }
        public string INT_SOURCE_NAME { get; set; }
        public string INT_NAME { get; set; }
        public string MODUL { get; set; }
        public string SOURCE_VALUE { get; set; }
        #endregion
    }
}
