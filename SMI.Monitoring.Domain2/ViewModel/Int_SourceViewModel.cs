﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Domain2.ViewModel
{
   public class Int_SourceViewModel 
    {

        public string INT_SOURCE_ID { get; set; }

        public string INT_SOURCE_NAME { get; set; }

    
        public string INT_NAME { get; set; }

       
        public string MODUL { get; set; }

        public string SOURCE_VALUE { get; set; }

    
        public string PARAMETER1 { get; set; }

    
        public string PARAMETER2 { get; set; }

     
        public string PARAMETER3 { get; set; }

 
        public string PARAMETER4 { get; set; }

    
        public string PARAMETER5 { get; set; }


        public string CREATED_BY { get; set; }

        public DateTime? CREATION_DATE { get; set; }

  
        public string LAST_UPDATED_BY { get; set; }

        public DateTime? LAST_UPDATE_DATE { get; set; }
    }
}
