﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Domain2.Services
{
   public interface Isgt_ap_invoice_stagingService
    {
        #region 
        sgt_ap_invoice_stagingViewModel GetById(int int_batch_id, int int_staging_id);
        IEnumerable<sgt_ap_invoice_stagingViewModel>List(Expression<Func<stg_ap_invoice_staging, bool>> predicate = null);
        //IEnumerable<GLJournalLogViewModel> ListSource(Expression<Func<SMI_GL_JOURNAL_STAGING, bool>> predicate = null);
        //IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null);



        #endregion

        #region ValidationLog
        Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID);
        IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null);
        #endregion
    }
}
