﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Domain2.Services
{
  public interface IintSourceService
    {
        Int_SourceViewModel GetById(string INT_SOURCE_ID);
        IEnumerable<Int_SourceViewModel> List(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null);
        //IEnumerable<vwInt_SourceViewModel> ListIntSource(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null);

        //void Insert(IEnumerable<Int_Batch_ProcessViewModel> list);
        //void Edit(Int_Batch_ProcessViewModel item);
        //void Edit(IEnumerable<Int_Batch_ProcessViewModel> list);
        //void Delete(string Id);
        //void Delete(IEnumerable<Int_Batch_ProcessViewModel> list);
    }
}
