﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Domain2.Services
{
  public interface IIntBatchProcessService
    {
        Int_Batch_ProcessViewModel GetById(string INT_SOURCE_ID);
        IEnumerable<Int_Batch_ProcessViewModel> List(Expression<Func<SMI_INT_BATCH_PROCESS, bool>> predicate = null);
        //void Insert(IEnumerable<Int_Batch_ProcessViewModel> list);
        //void Edit(Int_Batch_ProcessViewModel item);
        //void Edit(IEnumerable<Int_Batch_ProcessViewModel> list);
        //void Delete(string Id);
        //void Delete(IEnumerable<Int_Batch_ProcessViewModel> list);
    }
}
