﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Domain2.Services
{
   public interface IAPSuplierLogService
    {
        #region APSupplierLog
        APSuplierLogViewModel GetById(string INT_SOURCE_ID);
        IEnumerable<APSuplierLogViewModel> List();
        IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null);
        IEnumerable<APSuplierLogViewModel> ListSource(Expression<Func<SMI_AP_SUPPLIER_STAGING, bool>> predicate = null);

        #endregion


        #region ValidationLog
        Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID);
        IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null);
        #endregion
    }
}
