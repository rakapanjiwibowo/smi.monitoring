//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain2
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMI_FA_REDCOAT_EXPENSED_STG
    {
        public Nullable<decimal> TRANSACTION_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<long> ASSET_CATEGORY_ID { get; set; }
        public Nullable<System.DateTime> DATE_PLACED_IN_SERVICE { get; set; }
        public Nullable<decimal> FIXED_ASSETS_COST { get; set; }
        public Nullable<decimal> PAYABLES_UNITS { get; set; }
        public Nullable<decimal> FIXED_ASSETS_UNITS { get; set; }
        public Nullable<decimal> LOCATION_ID { get; set; }
        public Nullable<decimal> PAYABLES_COST { get; set; }
        public string ASSET_TYPE { get; set; }
        public Nullable<decimal> ASSET_KEY_CCID { get; set; }
        public string TAG_NUMBER { get; set; }
        public string PO_NUMBER { get; set; }
        public Nullable<long> PO_VENDOR_ID { get; set; }
        public Nullable<decimal> USER_ID { get; set; }
        public Nullable<long> ASSIGNED_TO { get; set; }
        public Nullable<decimal> ADD_TO_ASSET_ID { get; set; }
        public string AMORTIZE_FLAG { get; set; }
        public string NEW_MASTER_FLAG { get; set; }
        public string ERROR_MESSAGE { get; set; }
        public string STATUS { get; set; }
    }
}
