//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMI.Monitoring.Domain2
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMI_AR_RECEIPT_STAGING
    {
        public string INT_SOURCE_ID { get; set; }
        public decimal INT_BATCH_ID { get; set; }
        public decimal INT_STAGING_ID { get; set; }
        public string RECEIPT_NUMBER { get; set; }
        public System.DateTime RECEIPT_DATE { get; set; }
        public Nullable<decimal> ORG_ID { get; set; }
        public string CURRENCY_CODE { get; set; }
        public string CONVERSION_TYPE { get; set; }
        public Nullable<System.DateTime> CONVERSION_DATE { get; set; }
        public Nullable<decimal> CONVERSION_RATE { get; set; }
        public Nullable<decimal> AMOUNT { get; set; }
        public Nullable<System.DateTime> GL_DATE { get; set; }
        public Nullable<decimal> CUSTOMER_ID { get; set; }
        public Nullable<long> RECEIPT_METHOD_ID { get; set; }
        public Nullable<decimal> CUSTOMER_TRX_ID { get; set; }
        public Nullable<long> CASH_RECEIPT_ID { get; set; }
        public string REVERSAL_CATEGORY_CODE { get; set; }
        public string REASON_CODE { get; set; }
        public System.DateTime CREATION_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
        public Nullable<decimal> INT_REF_ID { get; set; }
        public Nullable<System.DateTime> INTERFACE_DATE { get; set; }
        public string INT_STATUS { get; set; }
        public Nullable<decimal> INT_REQUEST_ID { get; set; }
        public Nullable<decimal> IMPORT_REQUEST_ID { get; set; }
        public string RECEIPT_TYPE { get; set; }
        public Nullable<decimal> ACTIVITY_ID { get; set; }
    }
}
