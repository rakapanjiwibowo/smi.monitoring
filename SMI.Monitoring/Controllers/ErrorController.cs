﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;

namespace SMI.Monitoring.Controllers
{
    public class ErrorController : BaseController
    {
        public ErrorController(ILoggingService _loggingService,
                                IUserService _userService,
                                IRoleService _roleService,
                                IActiveDirectoryService _activeDirectoryService,
                                IConfigureService _configureService
            )                
            : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {  }

        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            return View();
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult HttpError404()
        {
            return View();
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult HttpError500()
        {
            return View();
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult NotAuthorized()
        {
            return View();
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult NotRegistered()
        {
            return View();
        }
    }    
}