﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;

namespace SMI.Monitoring.Controllers
{
    public class LoginController : BaseController
    {
        //private readonly IActiveDirectoryService activeDirectoryService;
        private readonly IModuleService moduleService;
        private readonly IMappingRoleModuleService mappingRoleModuleService;

        public LoginController(ILoggingService _loggingService,
                                IUserService _userService,
                                IRoleService _roleService,
                                IModuleService _moduleService,
                                IMappingRoleModuleService _mappingRoleModuleService,
                                IActiveDirectoryService _activeDirectoryService,
                                IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService,_configureService)
        {
            //activeDirectoryService = _activeDirectoryService;
            moduleService = _moduleService;
            mappingRoleModuleService = _mappingRoleModuleService;
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult SignIn(string username = "", string password = "")
        {
            //return View();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            System.Web.Security.FormsAuthentication.SignOut();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction("Login");
        }

    }
}