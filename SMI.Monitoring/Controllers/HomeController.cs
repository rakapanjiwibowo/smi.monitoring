﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;

namespace SMI.Monitoring.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IModuleService moduleService;
        private readonly IMappingRoleModuleService mappingRoleModuleService;

        public HomeController(ILoggingService _loggingService,
                              IUserService _userService,
                              IRoleService _roleService,
                              IModuleService _moduleService,
                              IMappingRoleModuleService _mappingRoleModuleService,
                              IActiveDirectoryService _activeDirectoryService,
                              IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            moduleService = _moduleService;
            mappingRoleModuleService = _mappingRoleModuleService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [ChildActionOnly]
        public ActionResult _GenerateMenu()
        {
            //var results = string.Empty;
            var results = UserMenu;
            return PartialView(results);
        }
    }
}