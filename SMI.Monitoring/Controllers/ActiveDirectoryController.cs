﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.IO;

namespace SMI.Monitoring.Controllers
{
    public class ActiveDirectoryController : BaseController
    {
        //private readonly IActiveDirectoryService activeDirectoryService;

        public ActiveDirectoryController(ILoggingService _loggingService,
                                            IRoleService _roleService,
                                            IUserService _userDirectoryService,
                                            IActiveDirectoryService _activeDirectoryService,
                                            IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userDirectoryService, _activeDirectoryService, _configureService)
        {
            //activeDirectoryService = _activeDirectoryService;
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            return View();
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, string realName)
        {
            try
            {
                //fake data
                //var tempData = new List<ActiveDirectoryViewModel>();
                //tempData.Add(new ActiveDirectoryViewModel
                //{
                //    AccountName = "user1",
                //    Name = "User 1",
                //    Email = "user1@mail.com",
                //    Title = "Staff"

                //});
                //tempData.Add(new ActiveDirectoryViewModel
                //{
                //    AccountName = "user2",
                //    Name = "User 2",
                //    Email = "user2@mail.com",
                //    Title = "Staff"

                //});
                //return Json(tempData.ToDataSourceResult(request));

                string domain = "DCSMI";
                var result = new List<ActiveDirectoryViewModel>();
                if (!string.IsNullOrEmpty(realName) && realName.Length >= 3)
                {
                    result = activeDirectoryService.GetListPersonalInformations(realName, domain).ToList();
                }
                return Json(result.ToDataSourceResult(request));
            }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while reading data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }

    }
}