﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;

namespace SMI.Monitoring.Controllers
{
    public class MappingRoleModuleController : BaseController
    {
        private readonly IMappingRoleModuleService mappingRoleModuleService;
        private readonly IModuleService moduleService;

        public MappingRoleModuleController(ILoggingService _loggingService,
                                IMappingRoleModuleService _mappingRoleModuleService,
                                IModuleService _moduleService,
                                IRoleService _roleService,
                                IUserService _userService,
                                IActiveDirectoryService _activeDirectoryService,
                                 IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            //activeDirectoryService = _activeDirectoryService;
            mappingRoleModuleService = _mappingRoleModuleService;
            moduleService = _moduleService;
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            ViewBag.Module = moduleService.List().ToList();
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, long roleId = 0)
        {
            IEnumerable<VwMappingRoleModuleViewModel> results = mappingRoleModuleService.ListView(x => x.RoleId == roleId).OrderBy(x => x.ModuleOrderMenu).ToList();
            return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<MappingRoleModuleViewModel> products, long clientId = 0)
        {
            List<MappingRoleModuleViewModel> results = new List<MappingRoleModuleViewModel>();
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    var listData = mappingRoleModuleService.List().ToList();
                    foreach (MappingRoleModuleViewModel product in products)
                    {
                        if (product.Id == 0)
                        {
                            //validation
                            bool IsExist = listData.Where(x => x.ModuleId == product.ModuleId && x.RoleId == product.RoleId).Any();
                            if (IsExist) throw new Exception("error, Modul sudah terdaftar");

                            Mapper(product);
                            results.Add(product);
                        }
                    }
                    if (ModelState.IsValid)
                        mappingRoleModuleService.Insert(results);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
            }
            // Return the newly created products and the ModelState (in case there are any validation errors)
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<MappingRoleModuleViewModel> products)
        {
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    List<MappingRoleModuleViewModel> results = new List<MappingRoleModuleViewModel>();
                    var listData = mappingRoleModuleService.List().ToList();
                    foreach (MappingRoleModuleViewModel product in products)
                    {
                        if (product.Id > 0)
                        {
                            if (!TryUpdateModel(product))
                            {
                                ModelState.AddModelError("", new Exception("Model is not valid!"));
                            }
                            else
                            {
                                //validation
                                bool IsExist = listData.Where(x => x.Id != product.Id && x.ModuleId == product.ModuleId && x.RoleId == product.RoleId).Any();
                                if (IsExist) throw new Exception("error, Modul sudah terdaftar");

                                Mapper(product);
                                results.Add(product);
                            }
                        }
                    }
                    if (ModelState.IsValid)
                        mappingRoleModuleService.Edit(results);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Error while saving data. Please fill all required field.");
            }

            // Return the ModelState in case there are any validation errors
            return Json(ModelState.ToDataSourceResult());
        }

        [HttpPost]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<MappingRoleModuleViewModel> products)
        {
            if (products.Any())
            {
                try
                {
                    List<MappingRoleModuleViewModel> results = new List<MappingRoleModuleViewModel>();
                    foreach (MappingRoleModuleViewModel product in products)
                    {
                        results.Add(product);
                    }
                    mappingRoleModuleService.Delete(results);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }

            return Json(ModelState.ToDataSourceResult());
        }

        [HttpGet]
        public ActionResult GetList()
        {
            var results = moduleService.List().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }

    }
}