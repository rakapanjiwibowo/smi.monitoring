﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.IO;
using SMI.Monitoring.Controllers.Attributes;

namespace SMI.Monitoring.Controllers
{
    public class UserController : BaseController
    {
        //private readonly IActiveDirectoryService activeDirectoryService;
        private readonly IMappingRoleUserService mappingRoleUserService;

        public UserController(ILoggingService _loggingService,
                                IRoleService _roleService,
                                IUserService _userService,
                                IMappingRoleUserService _mappingRoleUserService,
                                IActiveDirectoryService _activeDirectoryService,
                                IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            //activeDirectoryService = _activeDirectoryService;
            mappingRoleUserService = _mappingRoleUserService;
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            ViewBag.Role = roleService.List().ToList();
            return View();
        }

        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Detail(long Id = 0)
        {
            var item = userService.List(x => x.Id == Id).FirstOrDefault();
            return View(item);
        }

        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Add(UserViewModel data)
        {
            try
            {
                //validation
                //chek if code already exist
                var item = userService.List(x => x.UserAccount.Equals(data.UserAccount, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                if (item != null)
                {
                    throw new Exception("Error, User account sudah terdaftar");
                }

                //save request
                Mapper(data);
                var result = userService.Insert(data);

                var role = roleService.List(x => x.Id == data.RoleId).FirstOrDefault();
                if (role != null)
                {
                    var mappingItem = new MappingRoleUserViewModel
                    {
                        UserId = result.Id,
                        RoleId = role.Id
                    };
                    Mapper(mappingItem);
                    mappingRoleUserService.Insert(mappingItem);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }

                GeneralMessage.Add("Data berhasil disimpan");
                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException ve)
            {
                string message = PrintEntityValidationError(ve);
                GeneralErrorMessage.Add(message);
                return RedirectToAction("Add");
            }
            catch (Exception e)
            {
                GeneralErrorMessage.Add(e.Message);
                return RedirectToAction("Add");
            }
        }

        /// <summary>
        /// deprecated
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Edit(long Id = 0)
        {
            var item = userService.List(x => x.Id == Id).FirstOrDefault();
            return View(item);
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Edit(UserViewModel data)
        {
            try
            {
                //validation
                //chek if code already exist
                var item = userService.List(x => x.UserAccount.Equals(data.UserAccount, StringComparison.CurrentCultureIgnoreCase) && x.Id != data.Id).FirstOrDefault();
                if (item != null)
                {
                    throw new Exception("Error, User account sudah dipakai");
                }

                //save request
                Mapper(data);
                userService.Edit(data);

                GeneralMessage.Add("Data berhasil disimpan");
                return RedirectToAction("Index");
            }
            catch (DbEntityValidationException ve)
            {
                string message = PrintEntityValidationError(ve);
                GeneralErrorMessage.Add(message);
                return RedirectToAction("Edit", new { @Id = data.Id });
            }
            catch (Exception e)
            {
                GeneralErrorMessage.Add(e.Message);
                return RedirectToAction("Edit", new { @Id = data.Id });
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Delete(long Id = 0)
        {
            try
            {
                var item = userService.GetById(Id);
                if (item != null)
                {
                    userService.Delete(Id);
                }
                return Json(new { Error = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { Error = true, Message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                var query = mappingRoleUserService.ListView();
                var result = query.ToList();
                return Json(result.ToDataSourceResult(request));
            }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while reading data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }

        [HttpPost]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserViewModel> products)
        {
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    List<UserViewModel> results = new List<UserViewModel>();
                    List<MappingRoleUserViewModel> listNewMappingRoleUser = new List<MappingRoleUserViewModel>();
                    List<MappingRoleUserViewModel> listExistingMappingRoleUser = new List<MappingRoleUserViewModel>();

                    var listData = userService.List().ToList();
                    foreach (UserViewModel product in products)
                    {
                        if (product.Id > 0)
                        {
                            if (!TryUpdateModel(product))
                            {
                                ModelState.AddModelError("", new Exception("Model is not valid!"));
                            }
                            else
                            {
                                //validation
                                bool IsExist = listData.Where(x => x.UserAccount.Equals(product.UserAccount, StringComparison.CurrentCultureIgnoreCase) && x.Id != product.Id).Any();
                                if (IsExist) throw new Exception("Error, User account sudah ada");

                                Mapper(product);
                                results.Add(product);

                            }
                        }
                    }
                    if (ModelState.IsValid)
                        userService.Edit(results);

                    //update mapping role user
                    if (listNewMappingRoleUser.Any())
                        mappingRoleUserService.Insert(listNewMappingRoleUser);

                    if (listExistingMappingRoleUser.Any())
                        mappingRoleUserService.Edit(listExistingMappingRoleUser);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }
            }
            catch (DbEntityValidationException ve)
            {
                string message = PrintEntityValidationError(ve);
                ModelState.AddModelError("", "Error while saving data. The message is " + ve.Message);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
            }

            // Return the ModelState in case there are any validation errors
            return Json(ModelState.ToDataSourceResult());
        }

        [HttpPost]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<UserViewModel> products)
        {
            if (products.Any())
            {
                try
                {
                    List<UserViewModel> results = new List<UserViewModel>();
                    List<MappingRoleUserViewModel> listMappingRoleUser = new List<MappingRoleUserViewModel>();
                    foreach (UserViewModel product in products)
                    {
                        results.Add(product);

                        var mappingItem = mappingRoleUserService.List(x => x.UserId == product.Id).FirstOrDefault();
                        if (mappingItem != null)
                        {
                            listMappingRoleUser.Add(mappingItem);
                        }
                    }
                    mappingRoleUserService.Delete(listMappingRoleUser);
                    userService.Delete(results);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
                }
            }

            return Json(ModelState.ToDataSourceResult());
        }
    }
}