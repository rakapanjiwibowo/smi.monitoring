﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Controllers
{
    public class IntSourceController : BaseController
    {
        // GET: IntSource
        private readonly IintSourceService intSourceService;

        public IntSourceController(
                      ILoggingService _loggingService,
                      IRoleService _roleService,
                      IUserService _userService,
                      IActiveDirectoryService _activeDirectoryService,
                      IConfigureService _configureService,
                      IintSourceService _intSourceService
                      )
         : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            intSourceService = _intSourceService;
        }


        // GET: IntBatchProcess
        [AuthorizationFilter("IntSource", ModuleAction.View)]
        public ActionResult Index()
        {
            //ViewBag.ParentModule = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            //ViewBag.Module = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            return View();
        }

        [AuthorizationFilter("IntSource", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)

        {
            IEnumerable<Int_SourceViewModel> results = intSourceService.List().ToList();
            return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AuthorizationFilter("IntSource", ModuleAction.View)]
        public ActionResult GetListIntSource()
        {
            var data = intSourceService.List().OrderBy(x => x.INT_SOURCE_ID).ToList();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("IntSource", ModuleAction.View)]
        //[CacheControl(HttpCacheability.NoCache)]
        //public ActionResult ListFilterIntSource()
        //{
        //    List<Int_SourceViewModel> data = intSourceService.ListIntSource().ToList();
        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        //public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RatingRuleViewModel> products)
        //{
        //    List<Int_Batch_ProcessViewModel> results = new List<Int_Batch_ProcessViewModel>();
        //    try
        //    {
        //        if (products != null && ModelState.IsValid)
        //        //if (products != null)
        //        {
        //            var listData = intBatchProcessService.List().ToList();
        //            foreach (Int_Batch_ProcessViewModel product in products)
        //            {
        //                if (Convert.ToInt32(product.INT_SOURCE_ID) == 0)
        //                {
        //                    var isExist = listData.Where(x => x.INT_SOURCE_ID.Equals(product.INT_SOURCE_ID, StringComparison.CurrentCultureIgnoreCase)).Any();
        //                    if (isExist) throw new Exception("Found duplicate Rating");

        //                    //product.Rate = product.Rate / 100;

        //                    Mapper(product);
        //                    results.Add(product);
        //                }
        //            }

        //            if (ModelState.IsValid)
        //            {
        //                intBatchProcessService.Insert(results);
        //            }
        //        }
        //    }
        //    catch (DbEntityValidationException ve)
        //    {
        //        string message = PrintEntityValidationError(ve);
        //        ModelState.AddModelError("", "Error while saving data. Message is " + message);
        //        loggingService.LogError(ve, message, this.CurrentUser);
        //    }
        //    catch (Exception e)
        //    {
        //        ModelState.AddModelError("", "Error while saving data. Message is " + e.Message);
        //        loggingService.LogError(e, e.Message, this.CurrentUser);
        //    }

        //    return Json(results.ToDataSourceResult(request, ModelState));
        //}

        //[AuthorizationFilter("RatingRule", ModuleAction.Update)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        //public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RatingRuleViewModel> products)
        //{
        //    try
        //    {
        //        if (products != null & ModelState.IsValid)
        //        //if (products != null)
        //        {
        //            List<RatingRuleViewModel> results = new List<RatingRuleViewModel>();
        //            var listData = ratingRuleService.List().ToList();
        //            foreach (RatingRuleViewModel product in products)
        //            {
        //                if (product.Id > 0)
        //                {
        //                    if (!TryValidateModel(product))
        //                    {
        //                        ModelState.AddModelError("", new Exception("Model Not Valid"));
        //                    }
        //                    else
        //                    {
        //                        //custom validation
        //                        var isExist = listData.Where(x => x.Id != product.Id && x.RatingBank.Equals(product.RatingBank, StringComparison.CurrentCultureIgnoreCase)).Any();
        //                        if (isExist) throw new Exception("Found Duplicate Rating");

        //                        Mapper(product);
        //                        results.Add(product);
        //                    }
        //                }
        //            }

        //            if (ModelState.IsValid)
        //                ratingRuleService.Edit(products);
        //        }
        //    }
        //    catch (DbEntityValidationException ve)
        //    {
        //        string message = PrintEntityValidationError(ve);
        //        ModelState.AddModelError("", "Error while saving data. Message is " + message);
        //        loggingService.LogError(ve, message, this.CurrentUser);
        //    }
        //    catch (Exception e)
        //    {
        //        ModelState.AddModelError("", "Error while saving data. Message is " + e.Message);
        //        loggingService.LogError(e, e.Message, this.CurrentUser);
        //    }
        //    // Return the ModelState in case there are any validation errors
        //    return Json(ModelState.ToDataSourceResult());
        //}

        //[AuthorizationFilter("RatingRule", ModuleAction.Delete)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        //public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RatingRuleViewModel> products)
        //{
        //    if (products.Any())
        //    {
        //        try
        //        {
        //            List<RatingRuleViewModel> results = new List<RatingRuleViewModel>();
        //            foreach (RatingRuleViewModel product in products)
        //            {
        //                if (product.Id > 0)
        //                {
        //                    ratingRuleService.Delete(product.Id);
        //                }
        //            }
        //            //ratingRuleService.Delete(results);
        //        }
        //        catch (DbEntityValidationException ve)
        //        {
        //            string message = PrintEntityValidationError(ve);
        //            ModelState.AddModelError("", "Error while saving data. Message is " + message);
        //            loggingService.LogError(ve, message, this.CurrentUser);
        //        }
        //        catch (Exception e)
        //        {
        //            ModelState.AddModelError("", "Error while saving data. Message is " + e.Message);
        //            loggingService.LogError(e, e.Message, this.CurrentUser);
        //        }
        //    }
        //    return Json(ModelState.ToDataSourceResult());
        //}


    }
}