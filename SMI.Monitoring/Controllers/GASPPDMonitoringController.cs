﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.IO;

namespace SMI.Monitoring.Controllers
{
    public class GASPPDMonitoringController : BaseController
    {
        public readonly IGASPPDService gasppdService;

        public GASPPDMonitoringController(
                    ILoggingService _loggingService,
                    IRoleService _roleService,
                    IUserService _userService,
                    IActiveDirectoryService _activeDirectoryService,
                    IConfigureService _configureService,
                    IGASPPDService _gasppdService
                    )
         : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            gasppdService = _gasppdService;
        }

        // GET: GASPPDMonitoring
        public ActionResult Index()
        {

            ViewBag.GASPPDMonitoring = gasppdService.List().OrderBy(x => x.ID).ToList();
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var result = gasppdService.List().ToList();
                return Json(result.ToDataSourceResult(request));
            }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while reading data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }

        }

        public ActionResult GetList()
        {
            var results = gasppdService.List().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }
}