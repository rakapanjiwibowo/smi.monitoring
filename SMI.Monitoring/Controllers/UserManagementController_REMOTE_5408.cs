﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.LOSDeposito.Domain.Services;
using SMI.LOSDeposito.Domain.ViewModel;
using SMI.LOSDeposito.Web.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.IO;

namespace SMI.LOSDeposito.Web.Controllers
{
    public class UserManagementController : BaseController
    {
        private readonly IMappingRoleUserService mappingRoleUserService;
        private readonly IMappingRoleUserService mappingRoleUser;
        private readonly IModuleService moduleService;

        public UserManagementController(ILoggingService _loggingService,
                                IRoleService _roleService,
                                IUserService _userService,
                                IMappingRoleUserService _mappingRoleUserService,
                                IActiveDirectoryService _activeDirectoryService,
                                IMappingRoleUserService _mappingRoleUser,
                                IModuleService _moduleService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService)
        {
            mappingRoleUser = _mappingRoleUser;
            mappingRoleUserService = _mappingRoleUserService;
            moduleService = _moduleService;
        }

        #region User
        //[AuthorizationFilter("UserMenu", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Users()
        {
            ViewBag.Role = roleService.List().ToList();
            return View();
        }

        //[AuthorizationFilter("UserMenu", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadUser([DataSourceRequest] DataSourceRequest request)
        {
            var listUser = mappingRoleUser.ListView().ToList();
            return Json(listUser.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("UserMenu", ModuleAction.Create)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult AddUser(VwMappingRoleUserViewModel data)
        {
            var error = false;
            var message = string.Empty;
            try
            {
                UserViewModel dataUser = new UserViewModel();
                MappingRoleUserViewModel dataRole = new MappingRoleUserViewModel();

                var isExist = userService.List(x => x.UserAccount.Equals(data.UserAccount, StringComparison.CurrentCultureIgnoreCase)).Any();
                if (isExist)
                {
                    throw new Exception("User already exist");
                }

                //Insert User
                dataUser.UserAccount = data.UserAccount;
                dataUser.FullName = data.FullName;
                dataUser.Email = data.Email;
                dataUser.IsActive = data.IsActive;

                Mapper(dataUser);
                var rstUser = userService.Insert(dataUser);
                //end

                //insert Role
                dataRole.UserId = rstUser.Id;
                dataRole.RoleId = data.RoleId;

                Mapper(dataRole);
                mappingRoleUser.Insert(dataRole);
                //end

                message = "Insert Success";
            }
            catch (Exception e)
            {
                error = false;
                message = "Failed Insert : " + e.Message;
                loggingService.LogError(e, e.Message, CurrentUser);
            }

            return Json(new { err = error, msg = message }, JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("UserMenu", ModuleAction.Update)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult UpdateUser([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VwMappingRoleUserViewModel> products)
        {
            try
            {
                List<MappingRoleUserViewModel> listData = new List<MappingRoleUserViewModel>();
                List<UserViewModel> listDataUser = new List<UserViewModel>();
                MappingRoleUserViewModel data = new MappingRoleUserViewModel();
                UserViewModel dataUser = new UserViewModel();

                if (products != null && ModelState.IsValid)
                {
                    foreach (VwMappingRoleUserViewModel product in products)
                    {
                        if (product.Id > 0)
                        {
                            data.Id = product.Id;
                            data.RoleId = product.RoleId;
                            data.UserId = product.UserId;
                            data.CreatedBy = product.CreatedBy;
                            data.CreatedDate = product.CreatedDate;
                            data.UpdatedBy = this.CurrentUser;
                            data.UpdatedDate = DateTime.Now;
                            listData.Add(data);

                            dataUser.Id = product.UserId;
                            dataUser.UserAccount = product.UserAccount;
                            dataUser.FullName = product.FullName;
                            dataUser.Email = product.Email;
                            dataUser.IsActive = product.IsActive;
                            dataUser.CreatedBy = product.CreatedBy;
                            dataUser.CreatedDate = product.CreatedDate;
                            dataUser.UpdatedBy = this.CurrentUser;
                            dataUser.UpdatedDate = DateTime.Now;
                            listDataUser.Add(dataUser);
                        }
                    }

                    if (ModelState.IsValid)
                    {
                        mappingRoleUser.Edit(listData);
                        userService.Edit(listDataUser);

                        //reload authorization
                        IsAuthorizationChanged = true;
                    }
                }
            }
            catch (DbEntityValidationException ve)
            {
                string message = PrintEntityValidationError(ve);
                ModelState.AddModelError("", "Error while updating data. Message is " + message);
                loggingService.LogError(ve, message, this.CurrentUser);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while updating data");
                loggingService.LogError(e, e.Message, this.CurrentUser);
            }

            // Return the ModelState in case there are any validation errors
            return Json(ModelState.ToDataSourceResult());
        }

        //[AuthorizationFilter("UserMenu", ModuleAction.Delete)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult DeleteUser([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<VwMappingRoleUserViewModel> products)
        {
            if (products.Any())
            {
                try
                {
                    List<MappingRoleUserViewModel> rstMapping = new List<MappingRoleUserViewModel>();
                    List<UserViewModel> rstUser = new List<UserViewModel>();
                    UserViewModel dataUser = new UserViewModel();

                    foreach (VwMappingRoleUserViewModel product in products)
                    {
                        var dataMapping = mappingRoleUser.List(x => x.Id == product.Id).FirstOrDefault();
                        if (dataMapping != null)
                        {
                            rstMapping.Add(dataMapping);
                        }

                        dataUser.Id = product.UserId;
                        rstUser.Add(dataUser);
                    }
                    mappingRoleUser.Delete(rstMapping);
                    userService.Delete(rstUser);

                    //reload authorization
                    IsAuthorizationChanged = true;

                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Error while Deleting data");
                    loggingService.LogError(e, e.Message, this.CurrentUser);
                }
            }
            // Return the ModelState in case there are any validation errors
            return Json(ModelState.ToDataSourceResult());
        }

        //[AuthorizationFilter("UserMenu", ModuleAction.View)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadListEmployee([DataSourceRequest] DataSourceRequest request)
        {
            var data = userService.ListLookUpLoginUserAccount().ToList();
            return Json(data.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Module
        //[AuthorizationFilter("Module", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Module()
        {
            ViewBag.ParentModule = moduleService.List().OrderBy(x => x.OrderMenu).ToList();
            ViewBag.Module = moduleService.List().OrderBy(x => x.OrderMenu).ToList();
            return View();
        }

        //[AuthorizationFilter("Module", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadModule([DataSourceRequest] DataSourceRequest request)
        {
            List<VwModuleViewModel> results = moduleService.ListView().OrderBy(x => x.OrderMenu).ToList();
            results.ForEach(x =>
            {
                if (x.ParentId == null)
                {
                    x.ParentId = 0;
                }
            });
            return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("Module", ModuleAction.Create)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult CreateModule([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ModuleViewModel> products, long clientId = 0)
        {
            List<ModuleViewModel> results = new List<ModuleViewModel>();
            try
            {

                if (products != null && ModelState.IsValid)
                {
                    foreach (ModuleViewModel product in products)
                    {
                        if (product.Id == 0)
                        {
                            product.ParentId = (product.ParentId == null ? 0 : product.ParentId);
                            Mapper(product);
                            results.Add(product);
                        }
                    }

                    if (ModelState.IsValid)
                    {
                        moduleService.Insert(results);
                    }
                }
            }
            catch (DbEntityValidationException ve)
            {
                string message = PrintEntityValidationError(ve);
                ModelState.AddModelError("", "Error while saving data. The message is " + message);
                loggingService.LogError(ve, ve.Message, CurrentUser);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
                loggingService.LogError(e, e.Message, CurrentUser);
            }
            return Json(results.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("Module", ModuleAction.Update)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult UpdateModule([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ModuleViewModel> products)
        {
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    List<ModuleViewModel> results = new List<ModuleViewModel>();
                    var listData = moduleService.List().ToList();
                    foreach (ModuleViewModel product in products)
                    {
                        if (product.Id > 0)
                        {
                            if (!TryValidateModel(product))
                            {
                                ModelState.AddModelError("", new Exception("Model is not valid!"));
                            }
                            else
                            {
                                //validation
                                bool isExist = listData.Where(x => x.Id != product.Id && x.Code.Equals(product.Code, StringComparison.CurrentCultureIgnoreCase)).Any();
                                if (isExist) throw new Exception("Model already registered");

                                //parent Id cannot refer to itself
                                bool isParentItSelf = listData.Where(x => x.Id == product.ParentId && x.Id == product.Id).Any();
                                if (isParentItSelf) throw new Exception("error, Parent Menu cannot refer to itself");

                                Mapper(product);
                                results.Add(product);
                            }
                        }
                    }

                    if (ModelState.IsValid)
                    {
                        moduleService.Edit(results);

                        //reload authorization
                        IsAuthorizationChanged = true;
                    }
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while updating data. Please fill all required field.");
                loggingService.LogError(e, e.Message, CurrentUser);
            }
            return Json(ModelState.ToDataSourceResult());
        }

        //[AuthorizationFilter("Module", ModuleAction.Delete)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult DeleteModule([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ModuleViewModel> products)
        {
            if (products.Any())
            {
                try
                {
                    List<ModuleViewModel> results = new List<ModuleViewModel>();
                    foreach (ModuleViewModel product in products)
                    {
                        results.Add(product);
                    }
                    moduleService.Delete(results);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                    loggingService.LogError(e, e.Message, CurrentUser);
                }
            }
            return Json(ModelState.ToDataSourceResult());
        }

        //[AuthorizationFilter("Module", ModuleAction.View)]
        [HttpGet]
        public ActionResult GetListModule()
        {
            var results = moduleService.List().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Role
        //[AuthorizationFilter("RoleMenu", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Role()
        {
            ViewBag.Module = moduleService.List().OrderBy(x => x.OrderMenu).ToList();
            return View();
        }

        //[AuthorizationFilter("RoleMenu", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadRole([DataSourceRequest] DataSourceRequest request)
        {
            try
            {
                var result = roleService.List().ToList();
                return Json(result.ToDataSourceResult(request));
            }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while reading data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }

        //[AuthorizationFilter("RoleMenu", ModuleAction.Create)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult CreateRole([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleViewModel> products)
        {
            List<RoleViewModel> results = new List<RoleViewModel>();
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    foreach (RoleViewModel product in products)
                    {
                        if (product.Id == 0)
                        {
                            Mapper(product);
                            results.Add(product);
                        }
                    }
                    if (ModelState.IsValid)
                    {
                        roleService.Insert(results);
                    }
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
                loggingService.LogError(e, e.Message, CurrentUser);
            }
            return Json(results.ToDataSourceResult(request, ModelState), JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("RoleMenu", ModuleAction.Update)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult UpdateRole([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleViewModel> products)
        {
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    List<RoleViewModel> results = new List<RoleViewModel>();
                    var listData = roleService.List().ToList();

                    foreach (RoleViewModel product in products)
                    {
                        if (product.Id > 0)
                        {
                            if (!TryUpdateModel(product))
                            {
                                ModelState.AddModelError("", new Exception("Model is not valid!"));
                            }
                            else
                            {
                                //validation
                                bool isExist = listData.Where(x => x.RoleName.Equals(product.RoleName, StringComparison.CurrentCultureIgnoreCase) && x.Id != product.Id).Any();
                                if (isExist) throw new Exception("Error, Role sudah ada");

                                Mapper(product);
                                results.Add(product);
                            }
                        }
                    }

                    if (ModelState.IsValid)
                    {
                        roleService.Edit(results);

                        //reload authorization
                        IsAuthorizationChanged = true;
                    }
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
                loggingService.LogError(e, e.Message, CurrentUser);
            }
            return Json(ModelState.ToDataSourceResult());
        }

        //[AuthorizationFilter("RoleMenu", ModuleAction.Delete)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult DeleteRole([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleViewModel> products)
        {
            if (products.Any())
            {
                try
                {
                    List<RoleViewModel> results = new List<RoleViewModel>();
                    foreach (RoleViewModel product in products)
                    {
                        results.Add(product);
                    }
                    roleService.Delete(results);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
                    loggingService.LogError(e, e.Message, CurrentUser);
                }
            }
            return Json(ModelState.ToDataSourceResult());
        }

        //[AuthorizationFilter("RoleMenu", ModuleAction.View)]
        [HttpGet]
        public ActionResult GetListRole()
        {
            var results = roleService.List().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}