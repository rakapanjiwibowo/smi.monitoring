﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Controllers
{
    public class POOrderLogController : BaseController
    {
        // GET: POOrderLog
            private readonly IPOOrderLogService pOOrderLogService;
            private readonly IIntValidationLogService intValidationLogService;


        public POOrderLogController(
                          ILoggingService _loggingService,
                          IRoleService _roleService,
                          IUserService _userService,
                          IActiveDirectoryService _activeDirectoryService,
                          IConfigureService _configureService,
                          IPOOrderLogService _pOOrderLogService,
                          IIntValidationLogService _intValidationLogService
                          )
             : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
            {
                pOOrderLogService = _pOOrderLogService;
                intValidationLogService = _intValidationLogService;
    }

        #region POOrderLog

        //[AuthorizationFilter("POOrderLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            //ViewBag.ParentModule = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            //ViewBag.Module = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            return View();
        }

        //[AuthorizationFilter("POOrderLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, long filters = 0, DateTime? startDate = null, DateTime? endDate = null, long filterB = 0, string statuS = null)


        {
            try
            {

                if (startDate == null || endDate == null)
                {
                    var now = DateTime.Now.Date;
                    startDate = now.AddMonths(-1);
                    endDate = now;
                }
                else
                {
                    startDate = startDate.Value.Date;
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                }
                //IEnumerable<APInvoiceLogViewModel> results = aPInvoiceLogService.List().ToList();
                //return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
                List<POOrderLogViewModel> result = new List<POOrderLogViewModel>();

                var data = pOOrderLogService.List().Where(x => startDate <= x.CREATION_DATE && x.CREATION_DATE <= endDate).ToList();
                result = data.ToList();

                if (filters != 0)
                {
                    if (filterB != 0)
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_BATCH_ID == filterB).ToList();
                    }

                    if (statuS != "")
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_STATUS == statuS).ToList();
                    }
                    if (statuS != "" && filterB != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB && Convert.ToInt32(x.INT_SOURCE_ID) == filters).ToList();
                    }

                }

                if (filterB != 0)
                {
                    if (filters != 0)
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_BATCH_ID == filterB).ToList();
                    }
                    if (statuS != "")
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB).ToList();
                    }
                    if (statuS != "" && filters != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB && Convert.ToInt32(x.INT_SOURCE_ID) == filters).ToList();
                    }
                    else
                    {
                        result = data.Where(x => x.INT_BATCH_ID == filterB).ToList();
                    }
                }

                //if (filters == 0 && filterB == 0 && statuS == "")
                //{
                //    result = data.ToList();
                //}

                if (statuS != "")
                {
                    if (filters != 0)
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_STATUS == statuS).ToList();
                    }
                    if (filterB != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB).ToList();
                    }

                    if (filterB != 0 && filters != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB && Convert.ToInt32(x.INT_SOURCE_ID) == filters).ToList();
                    }
                    else
                    {
                        result = data.Where(x => x.INT_STATUS == statuS).ToList();
                    }

                }


                return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while saving data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
    }

        //[AuthorizationFilter("POOrderLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult GetList()
        {
            var data = pOOrderLogService.List().OrderBy(x => x.INT_STAGING_ID).ToList();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }

        //[AuthorizationFilter("POOrderLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult GetListSourceName()
        {
            var data = pOOrderLogService.ListSource().Select(x => new { Id = x.INT_SOURCE_ID, SourceName = x.INT_SOURCE_ID + " - " + x.SourceName }).OrderBy(x => x.SourceName).ToList();
            if (data.Count > 0)
            {
                return Json(data.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //[AuthorizationFilter("POOrderLog", ModuleAction.Download)]
        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Excel_Export(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }


        #endregion

        #region ValidationLog
        //[AuthorizationFilter("POOrderLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadValidationLog([DataSourceRequest] DataSourceRequest request, long SourceID = 0, long BatchID = 0, long StagingID = 0)

        {
            //IEnumerable<Int_Validation_LogViewModel> results = intValidationLogService.List().ToList();
            //return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet

            List<Int_Validation_LogViewModel> result = new List<Int_Validation_LogViewModel>();

            var data = intValidationLogService.List().ToList();
            if (SourceID != 0)
            {
                result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == SourceID && x.INT_BATCH_ID == BatchID && Convert.ToInt64(x.INT_STAGING_ID) == StagingID).ToList();

                return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            else
            {
                result = null;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //[AuthorizationFilter("POOrderLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult GetListValidation()
        {
            var data = intValidationLogService.List().OrderBy(x => x.INT_SOURCE_ID).ToList();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}