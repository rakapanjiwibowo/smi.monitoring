﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Routing;
using System.Security.Principal;
using SourceCode.Workflow.Client;
using System.Net.Mail;
using Kendo.Mvc;
using Kendo.Mvc.Extensions;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.IOC;
using SMI.Monitoring.Services;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.Configuration;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Controllers
{
    public class K2MonitoringController : BaseController
    {

        private readonly IGLJournalLogService gLJournalLogService;

        private string K2PackageSPPD = ConfigurationManager.AppSettings["K2PackageSPPD"];
        private string K2PackageFPPD = ConfigurationManager.AppSettings["K2PackageFPPD"];
        private string URLDetail = ConfigurationManager.AppSettings["Url_Detail"];
        private string SPPDUrl = ConfigurationManager.AppSettings["SPPD_Url"];
        private string FPPDUrl = ConfigurationManager.AppSettings["FPPD_Url"];

        public K2MonitoringController(IActiveDirectoryService _activeDirectoryService,
                                      ILoggingService _loggingService,
                                      IRoleService _roleService,
                                      IUserService _userService,
                                      IConfigureService _configureService,
                                      IGLJournalLogService _gLJournalLogService
                                    )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            gLJournalLogService = _gLJournalLogService;
        }
        // GET: K2Monitoring
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult PendingOracle()
        {
            //ViewBag.ListDivisi = sppdMonitoringService.ListDivisi();
            return View();
        }

        [HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadPendingListOracle([DataSourceRequest] DataSourceRequest request)
        {
            string message = string.Empty;
            K2BinaryService k2Service = new K2BinaryService();

            var listGLJournal = gLJournalLogService.PendingGLJournalLog().ToList();
            var listEmployee = sppdMonitoringService.ListEmployee().ToList();
            var Listworklist = k2Service.ListAllK2WorklistItem(K2PackageSPPD, "-", null, 0, ref message).ToList();

            var data = from list in listGLJournal
                       from worklist in Listworklist.Where(x => x.Folio.Contains(list.No_SPPD))
                       select new GLJournalLogViewModel
                       {
                           Id = list.Id,
                           No_SPPD = list.No_SPPD,
                           Requestor_Id = list.Requestor_Id,
                           Nama_Requestor = list.Nama_Requestor,
                           Divisi = list.Divisi,
                           Email = list.Email,
                           Destination = list.Destination,
                           Date_From = list.Date_From,
                           Date_To = list.Date_To
                       };
            var result = data.OrderByDescending(x => x.Id).ToList();
            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            //end
        }

    }
}