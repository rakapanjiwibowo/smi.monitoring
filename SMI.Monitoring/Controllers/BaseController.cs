﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using System.IO;
using Ionic.Zip;
using SMI.Monitoring.Helper;
using System.Configuration;

namespace SMI.Monitoring.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ILoggingService loggingService;
        protected readonly IRoleService roleService;
        protected readonly IUserService userService;
        protected readonly IActiveDirectoryService activeDirectoryService;
        protected readonly ICustomQueryMaxFlowService customQueryMaxFlowService;
        protected readonly IAuditTrailMasterService auditTrailMasterService;
        protected readonly IConfigureService configureService;

        /*
        protected readonly string k2Host = ConfigurationManager.AppSettings["K2Host"];
        protected readonly string k2Port = ConfigurationManager.AppSettings["K2Port"];
        protected readonly string k2SecurityLabel = ConfigurationManager.AppSettings["K2SecurityLabel"];
        protected readonly string K2UrlWebServices = ConfigurationManager.AppSettings["K2UrlWebServices"];
        protected readonly string K2PackageRequestCar = ConfigurationManager.AppSettings["K2PackageRequestCar"];
        */

        private string _errorMessage;

        protected string K2Host
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("K2Host", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string k2Host = data.Value;
                return k2Host;
            }
        }
        protected string K2Port
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("K2Port", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string K2Port = data.Value;
                return K2Port;
            }
        }
        protected string k2SecurityLabel
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("k2SecurityLabel", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string k2SecurityLabel = data.Value;
                return k2SecurityLabel;
            }
        }
        protected string K2UrlWebServices
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("K2UrlWebServices", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string K2UrlWebServices = data.Value;
                return K2UrlWebServices;
            }
        }

        //protected string K2PackageRequestCar
        //{
        //    get
        //    {
        //        var data = ListConfigure.Where(x => x.Description.Equals("K2PackageRequestCar", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
        //        string K2PackageRequestCar = data.Value;
        //        return K2PackageRequestCar;
        //    }
        //}

        protected string SystemEmailAddress
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("SystemEmailAddress", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string SystemEmailAddress = data.Value;
                return SystemEmailAddress;
            }
        }
        protected string SMTPServer
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("SmtpServer", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string SMTPServer = data.Value;
                return SMTPServer;
            }
        }
        protected string SMTPPort
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("SMTPPort", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string SMTPPort = data.Value;
                return SMTPPort;
            }
        }
        protected string NetworkCredentialAdmin
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("NetworkCredentialAdmin", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string NetworkCredentialAdmin = data.Value;
                return NetworkCredentialAdmin;
            }
        }
        protected string NetworkCredentialPassword
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("NetworkCredentialPassword", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string NetworkCredentialPassword = data.Value;
                return NetworkCredentialPassword;
            }
        }
        protected string TestMode
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("TestMode", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string TestMode = data.Value;
                return TestMode;
            }
        }





        public BaseController(ILoggingService loggingService,
                                  IRoleService roleService,
                                  IUserService userService,
                                  IActiveDirectoryService activeDirectoryService,
                                  IConfigureService configureService)
        {
            this.loggingService = loggingService;
            this.userService = userService;
            this.roleService = roleService;
            this.activeDirectoryService = activeDirectoryService;
            this.configureService = configureService;
        }

        public BaseController(ILoggingService loggingService,
                                IRoleService roleService,
                                IUserService userService,
                                IActiveDirectoryService activeDirectoryService,
                                IAuditTrailMasterService auditTrailMasterService,
                                IConfigureService configureService)
            : this(loggingService, roleService, userService, activeDirectoryService, configureService)
        {
            this.auditTrailMasterService = auditTrailMasterService;
        }


        protected string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        protected string UserDomain
        {
            get
            {
                return "DCSMI";
            }
        }

        protected string DirectoryTemplate
        {
            get
            {
                string directory = System.Configuration.ConfigurationManager.AppSettings["DirectoryTemplate"];
                return directory;
            }
        }

        protected bool IsAuthorizationChanged
        {
            get
            {
                try
                {
                    bool obj = (bool)Session["Base.AuthorizationChanged"];
                    return obj;
                }
                catch (NullReferenceException)
                {
                    Session["Base.AuthorizationChanged"] = false;
                }
                return (bool)Session["Base.AuthorizationChanged"];
            }
            set
            {
                Session["Base.AuthorizationChanged"] = value;
            }
        }

        #region Menu
        protected List<VwMappingRoleModuleViewModel> UserMenu
        {
            get
            {
                try
                {
                    List<VwMappingRoleModuleViewModel> obj = Session["Base.Menu"] as List<VwMappingRoleModuleViewModel>;
                    if (obj == null || obj.Count == 0 || IsAuthorizationChanged)
                    {
                        Session["Base.Menu"] = obj = GetListUserMenu();
                    }
                    return (List<VwMappingRoleModuleViewModel>)obj;
                }
                catch (NullReferenceException)
                {
                    Session["Base.Menu"] = null;
                }
                return new List<VwMappingRoleModuleViewModel>();
            }
            set
            {
                Session["Base.Menu"] = value;
            }
        }

        protected List<VwMappingRoleModuleViewModel> GetListUserMenu()
        {
            var paramUserRole = string.Empty;
            if (string.IsNullOrEmpty(UserRole.Code))
            {
                paramUserRole = "User";
            }
            else
            {
                paramUserRole = UserRole.Code;
            }

            var results = roleService.ListMenu(paramUserRole).Where(x => x.IsActive).ToList();
            return results;
        }
        #endregion

        protected RoleViewModel UserRole
        {
            get
            {
                try
                {
                    RoleViewModel obj = Session["Base.UserRole"] as RoleViewModel;
                    if (obj == null)
                    {
                        Session["Base.UserRole"] = obj = roleService.GetByUserName(CurrentUser);
                    }
                    return (RoleViewModel)obj;
                }
                catch (NullReferenceException)
                {
                    Session["Base.UserRole"] = new RoleViewModel();
                }
                return new RoleViewModel();
            }
            set
            {
                Session["Base.UserRole"] = value;
            }
        }

        protected UserViewModel UserInfo
        {
            get
            {
                try
                {
                    UserViewModel obj = Session["Base.UserInfo"] as UserViewModel;
                    if (obj == null)
                    {
                        Session["Base.UserInfo"] = obj = userService.LookUpLoginUserAccount(CurrentUser);
                    }
                    return (UserViewModel)obj;
                }
                catch (NullReferenceException)
                {
                    Session["Base.UserInfo"] = new UserViewModel();
                }
                return new UserViewModel();
            }
            set
            {
                Session["Base.UserInfo"] = value;
            }
        }

        protected List<string> GeneralMessage
        {
            get
            {
                try
                {
                    List<string> obj = Session["GeneralMessage"] as List<string>;
                    if (obj == null || obj.Count == 0)
                    {
                        Session["GeneralMessage"] = obj = new List<string>();
                    }
                    return (List<string>)obj;
                }
                catch (NullReferenceException)
                {
                    Session["GeneralMessage"] = null;
                }
                return new List<string>();
            }
            set
            {
                Session["GeneralMessage"] = value;
            }
        }

        protected List<string> GeneralWarningMessage
        {
            get
            {
                try
                {
                    List<string> obj = Session["GeneralWarningMessage"] as List<string>;
                    if (obj == null || obj.Count == 0)
                    {
                        Session["GeneralWarningMessage"] = obj = new List<string>();
                    }
                    return (List<string>)obj;
                }
                catch (NullReferenceException)
                {
                    Session["GeneralWarningMessage"] = null;
                }
                return new List<string>();
            }
            set
            {
                Session["GeneralWarningMessage"] = value;
            }
        }

        protected List<string> GeneralErrorMessage
        {
            get
            {
                try
                {
                    List<string> obj = Session["GeneralErrorMessage"] as List<string>;
                    if (obj == null || obj.Count == 0)
                    {
                        Session["GeneralErrorMessage"] = obj = new List<string>();
                    }
                    return (List<string>)obj;
                }
                catch (NullReferenceException)
                {
                    Session["GeneralErrorMessage"] = null;
                }
                return new List<string>();
            }
            set
            {
                Session["GeneralErrorMessage"] = value;
            }
        }

        protected List<string> ListAdministrator
        {
            get
            {
                string admin = System.Configuration.ConfigurationManager.AppSettings["Administrator"];
                if (!string.IsNullOrEmpty(admin))
                {
                    var result = admin.Split(';').Where(x => !string.IsNullOrEmpty(x)).ToList();
                    return result;
                }
                return new List<string>();
            }
        }

        protected string CurrentUser
        {
            get
            {
                string currentUser = User.Identity.Name;
                if (string.IsNullOrEmpty(currentUser))
                {
                    currentUser = "System";
                }
                return currentUser;
            }
        }

        protected string TemplateDirectory
        {
            get
            {
                string directory = HttpContext.Server.MapPath("~/Template/");
                return directory;
            }
        }

        protected List<SelectListItem> GetListCurrencyOld()
        {
            List<SelectListItem> results = new List<SelectListItem>();
            results.Add(new SelectListItem
            {
                Text = "IDR",
                Value = "IDR"
            });
            results.Add(new SelectListItem
            {
                Text = "USD",
                Value = "USD"
            });
            return results;
        }

        /// <summary>
        /// Bisa digunakan untuk check authorization user sebelum masuk ke masing-masing controller
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.RouteData.Values["controller"];
        }

        protected bool UserIsAuthenticated
        {
            get
            {
                return System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        public String ControllerName
        {
            get
            {
                return RouteData.Values["Controller"].ToString();
            }
        }

        protected void Mapper(IModel model)
        {
            if (model.Id > 0)
            {
                model.UpdatedBy = CurrentUser;
                model.UpdatedDate = DateTime.Now;
            }
            else
            {
                model.CreatedBy = CurrentUser;
                model.CreatedDate = DateTime.Now;
            }
        }

        #region print error
        protected string PrintModelError(ModelStateDictionary ms)
        {
            StringBuilder result = new StringBuilder();
            var allErrors = ModelState.Values.SelectMany(v => v.Errors);
            foreach (ModelError error in allErrors)
            {
                result.AppendLine("- " + error.ErrorMessage + ". ");
            }
            return result.ToString();
        }

        protected string PrintEntityValidationError(DbEntityValidationException e)
        {
            StringBuilder result = new StringBuilder();
            foreach (var validationErrors in e.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    result.AppendFormat("Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                }
            }
            return result.ToString();
        }

        protected void ListEntityValidationErrorAsModelState(DbEntityValidationException e)
        {
            foreach (var validationErrors in e.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    ModelState.AddModelError(validationError.PropertyName, validationError.ErrorMessage);
                }
            }
        }
        #endregion

        public IEnumerable<decimal> SplitHour(decimal totalHour, int days)
        {
            var results = new List<decimal>();
            int hour = (int)(totalHour / days);
            int remain = (int)(totalHour % days);
            var numbers = Enumerable.Range(1, days)
                        .Select(x => (decimal)hour)
                        .ToList();
            numbers.ForEach(n =>
            {
                decimal h = 0;
                if (remain > 0)
                {
                    h = n + 1;
                    remain -= 1;
                }
                else h = n;
                results.Add(h);
            });

            return results.AsEnumerable();
        }

        protected CustomFileResult CustomFile(byte[] fileContents, string contentType, string fileDownloadName)
        {
            CustomFileResult customFile = new CustomFileResult(fileContents, contentType);
            customFile.FileDownloadName = fileDownloadName;
            customFile.Inline = true;
            return customFile;
        }

        /// <summary>
        /// Generate attachment content for download
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="content"></param>
        /// <param name="browser"></param>
        /// <returns></returns>
        protected FileContentResult GenerateAttachment(string filename, byte[] content, string browser = "")
        {
            browser = string.IsNullOrEmpty(browser) ? string.Empty : browser;
            switch (browser.ToLower())
            {
                case "ie":
                    switch (Path.GetExtension(filename).ToLower())
                    {
                        case ".pdf":
                            return File(content, "application/pdf", filename);
                        case ".jpg":
                            return File(content, "image/jpg", filename);
                        case ".jpeg":
                            return File(content, "image/jpg", filename);
                        case ".xls":
                            return File(content, "application/vnd.ms-excel", filename);
                        case ".xlsx":
                            return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                        case ".doc":
                            return File(content, "application/msword", filename);
                        case ".docx":
                            return File(content, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
                        default:
                            return File(content, "application/octet-stream", filename);
                    }
                default:
                    switch (Path.GetExtension(filename).ToLower())
                    {
                        case ".pdf":
                            return CustomFile(content, "application/pdf", filename);
                        case ".jpg":
                            return CustomFile(content, "image/jpg", filename);
                        case ".jpeg":
                            return CustomFile(content, "image/jpg", filename);
                        case ".xls":
                            return CustomFile(content, "application/vnd.ms-excel", filename);
                        case ".xlsx":
                            return CustomFile(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                        case ".doc":
                            return CustomFile(content, "application/msword", filename);
                        case ".docx":
                            return CustomFile(content, "application/vnd.openxmlformats-officedocument.wordprocessingml.document", filename);
                        default:
                            return CustomFile(content, "application/octet-stream", filename);
                    }
            }
        }

        /// <summary>
        /// Return directory location of uploaded file
        /// </summary>
        /// <param name="uploadFile"></param>
        /// <param name="fileContent"></param>
        /// <returns></returns>
        protected string ExtractUploadedFile(HttpPostedFileBase uploadFile, out byte[] fileContent)
        {
            string fileName = uploadFile.FileName;
            FileInfo file = new FileInfo(fileName);
            if (!(file.Extension.ToLower() == ".xls" || file.Extension.ToLower() == ".xlsx" || file.Extension.ToLower() == ".zip"))
                throw new Exception("Invalid format file. Please upload the file with the folowing format (.xls, .xlsx or .zip)");
            fileContent = new byte[uploadFile.ContentLength];
            uploadFile.InputStream.Read(fileContent, 0, uploadFile.ContentLength);
            //copy file to upload folder
            string directory = HttpContext.Server.MapPath("~/Temporary/") + Guid.NewGuid().ToString() + "\\";
            Directory.CreateDirectory(directory);
            string destinationFile = string.Format(directory + file.Name);
            using (Stream writer = System.IO.File.OpenWrite(destinationFile))
            {
                writer.Write(fileContent, 0, fileContent.Length);
            }
            //handle if zip file, extract it first..
            if (file.Extension.ToLower() == ".zip")
            {
                using (ZipFile zipFile = ZipFile.Read(destinationFile))
                {
                    foreach (ZipEntry entry in zipFile.Entries)
                    {
                        FileInfo f = new FileInfo(entry.FileName);
                        if (f.Extension.ToLower() == ".xls" || f.Extension.ToLower() == ".xlsx")
                        {
                            entry.Extract(directory);
                            destinationFile = string.Format(directory + entry.FileName);
                            break;
                        }
                    }
                }
            }
            return destinationFile;
        }

       

        public int MaxUploadSize
        {
            get
            {

                var data = ListConfigure.Where(x => x.Description.Equals("MaxUploadSize", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                int MaxUploadSize = Convert.ToInt32(data.Value);
                return MaxUploadSize;
            }
        }

        public string MaxUploadDesc
        {
            get
            {
                var data = ListConfigure.Where(x => x.Description.Equals("MaxUploadDesc", StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                string MaxUploadDesc = data.Value;
                return MaxUploadDesc;

            }
        }

        protected List<TblConfigureViewModel> ListConfigure
        {
            get
            {
                List<TblConfigureViewModel> listItem = new List<TblConfigureViewModel>();
                listItem = configureService.List().ToList();
                return listItem;
            }
        }
    }
}