﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;

namespace SMI.Monitoring.Controllers
{
    public class AuditTrailController : BaseController
    {

        public AuditTrailController(ILoggingService _loggingService,
                                IRoleService _roleService,
                                IUserService _userService,
                                IActiveDirectoryService _activeDirectoryService,
                                IAuditTrailMasterService _auditTrailMasterService,
                                IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService, _auditTrailMasterService, _configureService)
        {
        }

        [AuthorizationFilter("AuditTrail", ModuleAction.View)]
        [HttpGet, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            ViewBag.AuditTrailAction = auditTrailMasterService.ListAction().ToList();
            return View();
        }

        [AuthorizationFilter("AuditTrail", ModuleAction.View)]
        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                //validation 

                if (startDate == null || endDate == null)
                {
                    var now = DateTime.Now.Date;
                    startDate = now.AddMonths(-1);
                    endDate = now;
                }
                else
                {
                    startDate = startDate.Value.Date;
                    endDate = endDate.Value.Date.AddDays(1).AddMilliseconds(-1);
                }

                var results = auditTrailMasterService.ListView(x => startDate <= x.CreatedDate && x.CreatedDate <= endDate).ToList();
                return Json(results.ToDataSourceResult(request));
            }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while saving data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }

        [AuthorizationFilter("AuditTrail", ModuleAction.View)]
        public ActionResult Read_Detail([DataSourceRequest] DataSourceRequest request, long auditId = 0)
        {
            IEnumerable<VwAuditTrailMasterDetailViewModel> results = auditTrailMasterService.ListViewDetail(x => x.AuditTrailMasterId == auditId).ToList();
            return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AuthorizationFilter("AuditTrail", ModuleAction.View)]
        public ActionResult GetListAction()
        {
            var results = auditTrailMasterService.ListAction().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        [AuthorizationFilter("AuditTrail", ModuleAction.View)]
        public ActionResult GetListModuleName()
        {
            var results = auditTrailMasterService
                            .ListView()
                            .Select(x => new SelectListItem
                            {
                                Text = x.ModuleName,
                                Value = x.ModuleName
                            })
                            .Distinct().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }

    }
}