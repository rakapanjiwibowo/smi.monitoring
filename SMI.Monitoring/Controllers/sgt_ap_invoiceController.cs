﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

    namespace SMI.Monitoring.Controllers
{
    public class sgt_ap_invoiceController : BaseController
    {
        // GET: sgt_ap_invoice

         private readonly Isgt_ap_invoice_stagingService isgt_ap_invoice_stagingService;
        private readonly IIntValidationLogService intValidationLogService;
        private readonly IintSourceService intSourceService;

        public sgt_ap_invoiceController(
                      ILoggingService _loggingService,
                      IRoleService _roleService,
                      IUserService _userService,
                      IActiveDirectoryService _activeDirectoryService,
                      IConfigureService _configureService,
                     Isgt_ap_invoice_stagingService _isgt_ap_invoice_stagingService,
                      IIntValidationLogService _intValidationLogService,
                      IintSourceService _intSourceService
                      )
         : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            isgt_ap_invoice_stagingService = _isgt_ap_invoice_stagingService;
            intValidationLogService = _intValidationLogService;
            intSourceService = _intSourceService;
        }


        #region stg ap invoice staging

        //[AuthorizationFilter("sgt_ap_invoice", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            //ViewBag.ParentModule = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            //ViewBag.Module = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            return View();
        }

        //[AuthorizationFilter("sgt_ap_invoice", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, DateTime? startDate = null, DateTime? endDate = null, long stagingID = 0, long filterB = 0, string codeCombi = null)

        {
            try
            {

                if (startDate == null || endDate == null)
                {
                    var now = DateTime.Now.Date;
                    startDate = now.AddMonths(-2);
                    endDate = now;
                }

                 var data = isgt_ap_invoice_stagingService.List().Where(x => startDate <= x.gl_date && x.gl_date <= endDate).ToList();
               
                 var results = data.ToList();
                if (filterB != 0)
                {
                    if (stagingID != 0)
                    {
                        results = data.Where(x => Convert.ToInt32(x.int_batch_id) == filterB && x.int_staging_id == stagingID).ToList();
                    }
                    if (codeCombi != "")
                    {
                        results = data.Where(x => x.int_batch_id == filterB && x.dist_code_combination == codeCombi).ToList();
                    }
                    if (stagingID != 0 && codeCombi != "")
                    {
                        results = data.Where(x => x.int_batch_id == filterB && x.int_staging_id == stagingID && x.dist_code_combination == codeCombi).ToList();
                    }
                    else
                    {
                        results = data.Where(x => x.int_batch_id == filterB).ToList();
                    }
                }

                if (stagingID != 0)
                {
                    if (filterB != 0)
                    {
                        results = data.Where(x => x.int_batch_id == filterB && x.int_staging_id == stagingID).ToList();
                    }
                    if (codeCombi != "")
                    {
                        results = data.Where(x => x.int_staging_id == stagingID && x.dist_code_combination == codeCombi).ToList();
                    }
                    if (filterB != 0 && codeCombi != "")
                    {
                        results = data.Where(x => x.int_batch_id == filterB && x.int_staging_id == stagingID && x.dist_code_combination == codeCombi).ToList();
                    }
                    else
                    {
                        results = data.Where(x => x.int_staging_id == stagingID).ToList();
                    }
                }

                if (codeCombi != "")
                {
                    if (filterB != 0)
                    {
                        results = data.Where(x => x.dist_code_combination == codeCombi && x.int_batch_id == filterB).ToList();
                    }
                    if (stagingID != 0)
                    {
                        results = data.Where(x => x.dist_code_combination == codeCombi && x.int_staging_id == stagingID).ToList();
                    }
                    if (filterB != 0 && stagingID != 0)
                    {
                        results = data.Where(x => x.int_batch_id == filterB && x.int_staging_id == stagingID && x.dist_code_combination == codeCombi).ToList();
                    }
                    else
                    {
                        results = data.Where(x => x.dist_code_combination == codeCombi).ToList();
                    }
                }

                return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while saving data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }

        #region dropdown source name
        ////[AuthorizationFilter("APInvoiceLog", ModuleAction.View)]
        //public ActionResult GetListSourceName()
        //{
        //    var data = iGLJournalLogService.ListSource().Select(x => new { Id = x.INT_SOURCE_ID, SourceName = x.INT_SOURCE_ID + " - " + x.SourceName }).OrderBy(x => x.SourceName).ToList();
        //    if (data.Count > 0)
        //    {
        //        return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Json(null, JsonRequestBehavior.AllowGet);
        //    }
        //}


        #endregion

        //[AuthorizationFilter("sgt_ap_invoice", ModuleAction.Download)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Excel_Export(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }

        #endregion

        #region ValidationLog
        //[AuthorizationFilter("sgt_ap_invoice", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadValidationLog([DataSourceRequest] DataSourceRequest request, long BatchID = 0, long StagingID = 0)
        {
            //IEnumerable<Int_Validation_LogViewModel> results = intValidationLogService.List().ToList();
            //return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet

            List<Int_Validation_LogViewModel> result = new List<Int_Validation_LogViewModel>();

            var data = intValidationLogService.List().ToList();
            if (BatchID != 0)
            {
                result = data.Where(x => x.INT_BATCH_ID == BatchID && x.INT_STAGING_ID == StagingID).ToList();
                return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            else
            {
                result = null;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //[AuthorizationFilter("sgt_ap_invoice", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult GetListValidation()
        {
            var data = intValidationLogService.List().OrderBy(x => x.INT_SOURCE_ID).ToList();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }
        #endregion 
    }
}