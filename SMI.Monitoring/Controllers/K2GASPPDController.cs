﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.IO;

namespace SMI.Monitoring.Controllers
{
    public class K2GASPPDController : BaseController
    {
        // GET: K2GASPPD

        public readonly IMappingK2GASPPDService mappingk2gasppd;
        public readonly IGASPPDService gasppdService;

        public K2GASPPDController(
                    ILoggingService _loggingService,
                    IRoleService _roleService,
                    IUserService _userService,
                    IActiveDirectoryService _activeDirectoryService,
                    IConfigureService _configureService,
                    IMappingK2GASPPDService _mappingk2gasppd,
                     IGASPPDService _gasppdService
                    )
         : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            mappingk2gasppd = _mappingk2gasppd;
            gasppdService = _gasppdService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetList()
        {
            var results = mappingk2gasppd.List().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string Doc_ID = null)
        {
            IEnumerable<VwMappingK2GASPPDViewModel> results = gasppdService.ListK2().Where(x=>x.H02 == Doc_ID).ToList();
            return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}