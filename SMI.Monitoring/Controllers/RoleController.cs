﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using System.IO;

namespace SMI.Monitoring.Controllers
{
    public class RoleController : BaseController
    {
        //private readonly IActiveDirectoryService activeDirectoryService;
        private readonly IModuleService moduleService;

        public RoleController(ILoggingService _loggingService,
                                IModuleService _moduleService,
                                IRoleService _roleService,
                                IUserService _userService,
                                IActiveDirectoryService _activeDirectoryService,
                                IConfigureService _configureService
            )
            : base(_loggingService, _roleService, _userService, _activeDirectoryService,_configureService)
        {
            //activeDirectoryService = _activeDirectoryService;
            moduleService = _moduleService;
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            ViewBag.Module = moduleService.List().ToList();
            return View();
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult MappingDivision(long Id)
        {
            var role = roleService.List(x => x.Id == Id).FirstOrDefault();
            return View(role);
        }

        [CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                var result = roleService.List().ToList();
                return Json(result.ToDataSourceResult(request));
            }
            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while reading data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }

        [HttpPost]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleViewModel> products, long clientId = 0)
        {
            List<RoleViewModel> results = new List<RoleViewModel>();
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    var listData = roleService.List().ToList();
                    foreach (RoleViewModel product in products)
                    {
                        if (product.Id == 0)
                        {
                            //custom validation

                            Mapper(product);
                            results.Add(product);
                        }
                    }
                    if (ModelState.IsValid)
                        roleService.Insert(results);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
            }
            // Return the newly created products and the ModelState (in case there are any validation errors)
            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleViewModel> products)
        {
            try
            {
                if (products != null && ModelState.IsValid)
                {
                    List<RoleViewModel> results = new List<RoleViewModel>();
                    var listData = roleService.List().ToList();
                    foreach (RoleViewModel product in products)
                    {
                        if (product.Id > 0)
                        {
                            if (!TryUpdateModel(product))
                            {
                                ModelState.AddModelError("", new Exception("Model is not valid!"));
                            }
                            else
                            {
                                //validation
                                bool IsExist = listData.Where(x => x.RoleName.Equals(product.RoleName, StringComparison.CurrentCultureIgnoreCase) && x.Id != product.Id).Any();
                                if (IsExist) throw new Exception("Error, Role sudah ada");

                                Mapper(product);
                                results.Add(product);
                            }
                        }
                    }
                    if (ModelState.IsValid)
                        roleService.Edit(results);

                    //reload authorization
                    IsAuthorizationChanged = true;
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
            }

            // Return the ModelState in case there are any validation errors
            return Json(ModelState.ToDataSourceResult());
        }

        [HttpPost]
        public ActionResult Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<RoleViewModel> products)
        {
            if (products.Any())
            {
                try
                {
                    List<RoleViewModel> results = new List<RoleViewModel>();
                    foreach (RoleViewModel product in products)
                    {
                        results.Add(product);
                    }
                    roleService.Delete(results);
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", "Error while saving data. The message is " + e.Message);
                }
            }

            return Json(ModelState.ToDataSourceResult());
        }


        [HttpGet]
        public ActionResult GetList()
        {
            var results = roleService.List().ToList();
            return Json(results, JsonRequestBehavior.AllowGet);
        }


    }
}