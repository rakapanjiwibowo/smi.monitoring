﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Text;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Controllers.Attributes;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Controllers
{
    public class GLJournalLogController : BaseController
    {
        // GET: APInvoiceLog
        private readonly IGLJournalLogService iGLJournalLogService;
        private readonly IIntValidationLogService intValidationLogService;
        private readonly IintSourceService intSourceService;

        public GLJournalLogController(
                      ILoggingService _loggingService,
                      IRoleService _roleService,
                      IUserService _userService,
                      IActiveDirectoryService _activeDirectoryService,
                      IConfigureService _configureService,
                      IGLJournalLogService _iGLJournalLogService,
                      IIntValidationLogService _intValidationLogService,
                      IintSourceService _intSourceService
                      )
         : base(_loggingService, _roleService, _userService, _activeDirectoryService, _configureService)
        {
            iGLJournalLogService = _iGLJournalLogService;
            intValidationLogService = _intValidationLogService;
            intSourceService = _intSourceService;
        }


        #region GLJournalLog

        //[AuthorizationFilter("GLJournalLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Index()
        {
            //ViewBag.ParentModule = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            //ViewBag.Module = intBatchProcessService.List().OrderBy(x => x.INT_BATCH_ID).ToList();
            return View();
        }

        //[AuthorizationFilter("GLJournalLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, long filters = 0, DateTime? startDate = null, DateTime? endDate = null, long filterB = 0, string statuS = null)

        {
            try
            {

                if (startDate == null || endDate == null)
                {
                    var now = DateTime.Now.Date;
                    startDate = now.AddMonths(-2);
                    endDate = now;
                }
              

                //IEnumerable<APInvoiceLogViewModel> results = aPInvoiceLogService.List().ToList(); 
                //return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
                List<GLJournalLogViewModel> result = new List<GLJournalLogViewModel>();

                #region remark
                // var data = from glj in aPInvoiceLogService.List()
                //           join isn in intSourceService.List()
                //           on glj.INT_SOURCE_ID equals isn.INT_SOURCE_ID
                //           select new APInvoiceLogViewModel()
                //           {
                //               #region Data
                //               INT_SOURCE_ID = glj.INT_SOURCE_ID,
                //               SourceName = string.Format("{0}-{1}", isn.INT_SOURCE_NAME, isn.INT_NAME),
                //               INT_BATCH_ID = glj.INT_BATCH_ID,
                //               INT_STAGING_ID = glj.INT_STAGING_ID,
                //               CODE_COMBINATION = glj.CODE_COMBINATION,
                //               INVOICE_BATCH = glj.INVOICE_BATCH,
                //               INVOICE_DESCRIPTION = glj.INVOICE_DESCRIPTION,
                //               INVOICE_NO = glj.INVOICE_NO,
                //               GL_DATE = glj.GL_DATE,
                //               INVOICE_TYPE = glj.INVOICE_TYPE,
                //               INVOICE_AMOUNT = glj.INVOICE_AMOUNT,
                //               CURRENCY_CODE = glj.CURRENCY_CODE,
                //               CURRENCY_RATE = glj.CURRENCY_RATE,
                //               RATE_DATE = glj.RATE_DATE,
                //               RATE_TYPE = glj.RATE_TYPE,
                //               INTERFACE_DATE = glj.INTERFACE_DATE,
                //               INT_STATUS = glj.INT_STATUS,
                //               CREATION_DATE = glj.CREATION_DATE,
                //               CREATED_BY = glj.CREATED_BY,
                //               LAST_UPDATED_BY = glj.LAST_UPDATED_BY,
                //               LAST_UPDATE_DATE = glj.LAST_UPDATE_DATE,
                //               VENDOR_ID = glj.VENDOR_ID,
                //               INVOICE_ID = glj.INVOICE_ID,
                //               INT_GROUP_ID = glj.INT_GROUP_ID,
                //               VENDOR_SITE_ID = glj.VENDOR_SITE_ID,
                //               INVOICE_LINE_ID = glj.INVOICE_LINE_ID,
                //               TAX_CODE = glj.TAX_CODE,
                //               WHT_CODE = glj.WHT_CODE,
                //               INT_REF_ID = glj.INT_REF_ID,
                //               INT_REQUEST_ID = glj.INT_REQUEST_ID,
                //               AP_INVOICE_SOURCE = glj.AP_INVOICE_SOURCE,
                //               TERMS_DATE = glj.TERMS_DATE,
                //               LINE_NUMBER = glj. LINE_NUMBER,
                //               CCID = glj.CCID,
                //               ORG_ID = glj.ORG_ID,
                //               LINE_DESCRIPTION = glj.LINE_DESCRIPTION,
                //               PO_NUMBER = glj.PO_NUMBER,
                //               PO_LINE_NUMBER = glj.PO_LINE_NUMBER,
                //               VALIDATE_FLAG = glj.VALIDATE_FLAG,
                //               BATCH_CONTEXT = glj.BATCH_CONTEXT,
                //               BATCH_ATTRIBUTE1 = glj.BATCH_ATTRIBUTE1,
                //               BATCH_ATTRIBUTE2 = glj.BATCH_ATTRIBUTE2,
                //               BATCH_ATTRIBUTE3 = glj.BATCH_ATTRIBUTE3,
                //               BATCH_ATTRIBUTE4 = glj.BATCH_ATTRIBUTE4,
                //               BATCH_ATTRIBUTE5 = glj.BATCH_ATTRIBUTE5,
                //               BATCH_ATTRIBUTE6 = glj.BATCH_ATTRIBUTE6,
                //               INVOICE_CONTEXT = glj.INVOICE_CONTEXT,
                //               INVOICE_ATTRIBUTE1 = glj.INVOICE_ATTRIBUTE1,
                //               INVOICE_ATTRIBUTE2 = glj.INVOICE_ATTRIBUTE2,
                //               INVOICE_ATTRIBUTE3 = glj.INVOICE_ATTRIBUTE3,
                //               INVOICE_ATTRIBUTE4 = glj.INVOICE_ATTRIBUTE4,
                //               INVOICE_ATTRIBUTE5 = glj.INVOICE_ATTRIBUTE5,
                //               INVOICE_ATTRIBUTE6 = glj.INVOICE_ATTRIBUTE6,
                //               INVOICE_ATTRIBUTE7 = glj.INVOICE_ATTRIBUTE7,
                //               INVOICE_ATTRIBUTE8 = glj.INVOICE_ATTRIBUTE8,
                //               INVOICE_ATTRIBUTE9 = glj.INVOICE_ATTRIBUTE9,
                //               INVOICE_ATTRIBUTE10 = glj.INVOICE_ATTRIBUTE10,
                //               INVOICE_ATTRIBUTE11 = glj.INVOICE_ATTRIBUTE11,
                //               INVOICE_ATTRIBUTE12 = glj.INVOICE_ATTRIBUTE12,
                //               INVOICE_ATTRIBUTE13 = glj.INVOICE_ATTRIBUTE13,
                //               INVOICE_ATTRIBUTE14 = glj.INVOICE_ATTRIBUTE14,
                //               INVOICE_ATTRIBUTE15 = glj.INVOICE_ATTRIBUTE15,
                //               LINE_CONTEXT = glj.LINE_CONTEXT,
                //               LINE_ATTRIBUTE1 = glj.LINE_ATTRIBUTE1,
                //               LINE_ATTRIBUTE2 = glj.LINE_ATTRIBUTE2,
                //               LINE_ATTRIBUTE3 = glj.LINE_ATTRIBUTE3,
                //               LINE_ATTRIBUTE4 = glj.LINE_ATTRIBUTE4,
                //               LINE_ATTRIBUTE5 = glj.LINE_ATTRIBUTE5,
                //               LINE_ATTRIBUTE6 = glj.LINE_ATTRIBUTE6,
                //               LINE_ATTRIBUTE7 = glj.LINE_ATTRIBUTE7,
                //               LINE_ATTRIBUTE8 = glj.LINE_ATTRIBUTE8,
                //               LINE_ATTRIBUTE9 = glj.LINE_ATTRIBUTE9,
                //               IMPORT_REQUEST_ID = glj.IMPORT_REQUEST_ID,
                //               AWT_GROUP_ID = glj.AWT_GROUP_ID,
                //               CCID_LIABILITY = glj.CCID_LIABILITY,
                //               AMOUNT_INCL_TAX_FLAG = glj.AMOUNT_INCL_TAX_FLAG,
                //               RECEIPT_NUM = glj.RECEIPT_NUM,
                //               RCV_LINE_NUM = glj.RCV_LINE_NUM,
                //               RCV_TRX_ID = glj.RCV_TRX_ID,
                //               QUANTITY_INVOICED = glj.QUANTITY_INVOICED
                //               #endregion
                //           };
                #endregion

                //var data = iGLJournalLogService.List().ToList();
                var data = iGLJournalLogService.List().Where(x => startDate <= x.CREATION_DATE && x.CREATION_DATE <= endDate).ToList();
                //var data = iGLJournalLogService.List.(x => startDate <= x.CREATION_DATE && x.CREATION_DATE <= endDate).ToList();
                //var data = iGLJournalLogService.List().ToList();
                //#region filterDropdown
                result = data.ToList();

                if (filters != 0)
                {
                    if (filterB !=0)
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_BATCH_ID == filterB).ToList();
                    }

                    if(statuS !="")
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_STATUS == statuS).ToList();
                    }
                    if (statuS != "" && filterB != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB && Convert.ToInt32(x.INT_SOURCE_ID) == filters).ToList();
                    }

                }

                if (filterB != 0)
                {
                    if (filters != 0)
                    {
                        result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_BATCH_ID == filterB).ToList();
                    }
                    if (statuS != "")
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB).ToList();
                    }
                    if (statuS != "" && filters != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB && Convert.ToInt32(x.INT_SOURCE_ID) == filters).ToList();
                    }
                    else
                    {
                        result = data.Where(x => x.INT_BATCH_ID == filterB).ToList();
                    }
                }

                //if (filters == 0 && filterB == 0 && statuS == "")
                //{
                //    result = data.ToList();
                //}

                if (statuS != "")
                {
                    if (filters != 0)
                    {
                        result = data.Where(x=> Convert.ToInt32(x.INT_SOURCE_ID) == filters && x.INT_STATUS == statuS).ToList();
                    }
                    if (filterB != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB).ToList();
                    }

                    if (filterB != 0 && filters != 0)
                    {
                        result = data.Where(x => x.INT_STATUS == statuS && x.INT_BATCH_ID == filterB && Convert.ToInt32(x.INT_SOURCE_ID) == filters).ToList();
                    }
                    else
                    {
                        result = data.Where(x => x.INT_STATUS == statuS).ToList();
                    }

                }

          
             
                //else 
                //{
                //    result = data.ToList();
                //}
                return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);


                //IEnumerable<GLJournalLogViewModel> results = iGLJournalLogService.List().ToList();
                //return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
                //#endregion

            }

            catch (Exception e)
            {
                ModelState.AddModelError("grid_error", "Error while saving data. Message is " + e.Message);
                // Return the ModelState in case there are any validation errors
                return Json(ModelState.ToDataSourceResult());
            }
        }


        //[AuthorizationFilter("GLJournalLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult GetListSourceName()
        {
            var data = iGLJournalLogService.ListSource().Select(x => new { Id = x.INT_SOURCE_ID, SourceName = x.INT_SOURCE_ID + " - " + x.SourceName }).OrderBy(x => x.SourceName).ToList();
            if (data.Count > 0)
            {
                return Json(data.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        //[AuthorizationFilter("GLJournalLog", ModuleAction.Download)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult Excel_Export(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }


        #endregion
        //#endregion

        #region ValidationLog
        //[AuthorizationFilter("GLJournalLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult ReadValidationLog([DataSourceRequest] DataSourceRequest request, long SourceID = 0, long BatchID = 0, long StagingID = 0)
        {
            //IEnumerable<Int_Validation_LogViewModel> results = intValidationLogService.List().ToList();
            //return Json(results.ToDataSourceResult(request), JsonRequestBehavior.AllowGet

            List<Int_Validation_LogViewModel> result = new List<Int_Validation_LogViewModel>();
            
            var data = intValidationLogService.List().ToList();
            if (SourceID != 0)
            {
                result = data.Where(x => Convert.ToInt32(x.INT_SOURCE_ID) == SourceID && x.INT_BATCH_ID == BatchID && Convert.ToInt64(x.INT_STAGING_ID) == StagingID).ToList();
                return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            else
            {
                result = null;
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        //[AuthorizationFilter("GLJournalLog", ModuleAction.View)]
        //[HttpPost, CacheControl(HttpCacheability.NoCache)]
        public ActionResult GetListValidation()
        {
            var data = intValidationLogService.List().OrderBy(x => x.INT_SOURCE_ID).ToList();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }
        #endregion 


    }
}