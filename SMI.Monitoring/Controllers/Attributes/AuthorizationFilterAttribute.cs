﻿using SimpleInjector;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SMI.Monitoring.Controllers.Attributes
{
    public class AuthorizationFilterAttribute : AuthorizeAttribute
    {
        bool isAuthorized = false;
        string module;
        ModuleAction moduleAction;
        IUserService userService;
        IMappingRoleUserService mappingRoleUserService;
        IMappingRoleModuleService mappingRoleModuleService;
        List<VwMappingRoleModuleViewModel> ListOfAuthorizedModule = new List<VwMappingRoleModuleViewModel>();
        IActiveDirectoryService activeDirectoryService;
        static Container injectionContainer = null;

        public AuthorizationFilterAttribute()
            : base()
        {
            injectionContainer = SimpleInjectorInitializer.GetContainer();
        }

        public AuthorizationFilterAttribute(string _module)
        : this()
        {
            this.module = _module;
        }

        public AuthorizationFilterAttribute(string _module, ModuleAction _moduleAction)
            : this(_module)
        {
            this.moduleAction = _moduleAction;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            var sessionRole = filterContext.HttpContext.Session["Base.UserRole"] as string;
            isAuthorized = false;

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (injectionContainer != null)
                {
                    var currentUser = filterContext.HttpContext.User.Identity.Name;
                    userService = injectionContainer.GetInstance<IUserService>();
                    if (string.IsNullOrEmpty(sessionRole))
                    {
                        filterContext.HttpContext.Session["Base.UserRole"] = sessionRole = userService.GetUserRole(currentUser);
                    }
                    ListOfAuthorizedModule = filterContext.HttpContext.Session["Base.MappingRoleModuleViewModel"] as List<VwMappingRoleModuleViewModel>;
                    bool AuthorizationChanged = filterContext.HttpContext.Session["Base.AuthorizationChanged"] == null ? false : (bool)filterContext.HttpContext.Session["Base.AuthorizationChanged"];   //trigger reload authorization
                    if (ListOfAuthorizedModule == null || ListOfAuthorizedModule.Count == 0 || AuthorizationChanged)
                    {
                        mappingRoleModuleService = injectionContainer.GetInstance<IMappingRoleModuleService>();
                        var results = mappingRoleModuleService.ListView(x => x.RoleName.Equals(sessionRole, StringComparison.CurrentCultureIgnoreCase)).ToList();
                        filterContext.HttpContext.Session["Base.MappingRoleModuleViewModel"] = ListOfAuthorizedModule = results;
                    }
                    var currentModule = ListOfAuthorizedModule.Where(x => x.ModuleCode.Equals(module, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    {
                        if (currentModule != null)
                        {
                            switch (moduleAction)
                            {
                                case ModuleAction.View:
                                    if (currentModule.IsGrantedToView) isAuthorized = true;
                                    break;
                                case ModuleAction.Create:
                                    if (currentModule.IsGrantedToCreate) isAuthorized = true;
                                    break;
                                case ModuleAction.Update:
                                    if (currentModule.IsGrantedToUpdate) isAuthorized = true;
                                    break;
                                case ModuleAction.Delete:
                                    if (currentModule.IsGrantedToDelete) isAuthorized = true;
                                    break;
                                case ModuleAction.Detail:
                                    if (currentModule.IsGrantedToDetail) isAuthorized = true;
                                    break;
                                case ModuleAction.Download:
                                    if (currentModule.IsGrantedToDownload) isAuthorized = true;
                                    break;
                                case ModuleAction.Upload:
                                    if (currentModule.IsGrantedToUpload) isAuthorized = true;
                                    break;
                                case ModuleAction.Print:
                                    if (currentModule.IsGrantedToPrint) isAuthorized = true;
                                    break;
                            }
                        }
                    }
                }
            }

            if (!isAuthorized)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new
                    {
                        controller = "Error",
                        action = "NotAuthorized"
                    }));
            }
        }

    }
    public enum ModuleAction
    {
        View = 0,
        Create,
        Update,
        Delete,
        Detail,
        Download,
        Upload,
        Print
    }
}