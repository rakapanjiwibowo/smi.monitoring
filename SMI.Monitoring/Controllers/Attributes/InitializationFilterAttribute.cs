﻿using SimpleInjector;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMI.Monitoring.Controllers.Attributes
{
    public class InitializationFilterAttribute : ActionFilterAttribute
    {
        IUserService userService;
        IActiveDirectoryService activeDirectoryService;
        static Container container = null;

        public InitializationFilterAttribute()
            : base()
        {
            container = SimpleInjectorInitializer.GetContainer();
        }
        
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //set role
            string sessionRole = filterContext.HttpContext.Session["Base.UserRole"] as string;
            if (container != null)
            {
                var currentUser = filterContext.HttpContext.User.Identity.Name;
                userService = container.GetInstance<IUserService>();
                if (string.IsNullOrEmpty(sessionRole))
                {
                    filterContext.HttpContext.Session["Base.UserRole"] = sessionRole = userService.GetUserRole(currentUser);
                }

                var sessionUserInfo = filterContext.HttpContext.Session["Base.UserInfo"] as UserViewModel;
                if (sessionUserInfo == null)
                {
                    filterContext.HttpContext.Session["Base.UserInfo"] = sessionUserInfo = userService.LookUpLoginUserAccount(currentUser);

                }

            }
        }

    }
}