﻿using SimpleInjector;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.IOC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMI.Monitoring.Controllers.Attributes
{

    /// <summary>
    /// Deprecated
    /// </summary>
    public class CustomAuthenticationFilterAttribute : ActionFilterAttribute
    {
        string redirectPage = @"~/Home/NotAuthorized";
        string module;
        ModuleAction moduleAction;
        IUserService userService;
        IMappingRoleUserService mappingRoleUserService;
        IMappingRoleModuleService mappingRoleModuleService;
        List<VwMappingRoleModuleViewModel> ListOfAuthorizedModule = new List<VwMappingRoleModuleViewModel>();
        IActiveDirectoryService activeDirectoryService;
        static Container container = null;

        public CustomAuthenticationFilterAttribute()
            : base()
        {
            container = SimpleInjectorInitializer.GetContainer();
        }

        public CustomAuthenticationFilterAttribute(string _module)
        : this()
        {
            module = _module;
        }

        public CustomAuthenticationFilterAttribute(string _module, ModuleAction _moduleAction)
            : this(_module)
        {
            moduleAction = _moduleAction;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            var sessionRole = filterContext.HttpContext.Session["Base.UserRole"] as string;

            if (container != null)
            {
                var currentUser = filterContext.HttpContext.User.Identity.Name;
                userService = container.GetInstance<IUserService>();
                if (string.IsNullOrEmpty(sessionRole))
                {
                    filterContext.HttpContext.Session["Base.UserRole"] = sessionRole = userService.GetUserRole(currentUser);
                }
                ListOfAuthorizedModule = filterContext.HttpContext.Session["Base.MappingRoleModuleViewModel"] as List<VwMappingRoleModuleViewModel>;
                bool AuthorizationChanged = filterContext.HttpContext.Session["Base.AuthorizationChanged"] == null ? false: (bool)filterContext.HttpContext.Session["Base.AuthorizationChanged"];   //trigger reload authorization
                if (ListOfAuthorizedModule == null || ListOfAuthorizedModule.Count == 0 || AuthorizationChanged)
                {
                    mappingRoleModuleService = container.GetInstance<IMappingRoleModuleService>();
                    var results = mappingRoleModuleService.ListView(x => x.RoleName.Equals(sessionRole, StringComparison.CurrentCultureIgnoreCase)).ToList();
                    filterContext.HttpContext.Session["Base.MappingRoleModuleViewModel"] = ListOfAuthorizedModule = results;
                }
                var currentModule = ListOfAuthorizedModule.Where(x => x.ModuleCode.Equals(module, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                if (currentModule == null)
                {
                    ctx.Response.Redirect(redirectPage);
                }
                else
                {
                    switch (moduleAction)
                    {
                        case ModuleAction.View:
                            if (!currentModule.IsGrantedToView) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Create:
                            if (!currentModule.IsGrantedToCreate) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Update:
                            if (!currentModule.IsGrantedToUpdate) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Delete:
                            if (!currentModule.IsGrantedToDelete) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Detail:
                            if (!currentModule.IsGrantedToDetail) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Download:
                            if (!currentModule.IsGrantedToDownload) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Upload:
                            if (!currentModule.IsGrantedToUpload) ctx.Response.Redirect(redirectPage);
                            break;
                        case ModuleAction.Print:
                            if (!currentModule.IsGrantedToPrint) ctx.Response.Redirect(redirectPage);
                            break;
                    }
                    base.OnActionExecuting(filterContext);
                }
            }
            else ctx.Response.Redirect(redirectPage);
        }

    }

}