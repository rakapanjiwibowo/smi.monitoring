﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Collections.Generic;
using System.Collections.Specialized;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.IOC;
using SMI.Monitoring.Controllers;
using SimpleInjector;

namespace SMI.Monitoring
{
    public class MvcApplication : System.Web.HttpApplication
    {

        static Container container = null;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            container = SimpleInjectorInitializer.Initialize();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var httpContext = ((MvcApplication)sender).Context;
            var lastException = this.Server.GetLastError();
            if (lastException != null)
            {
                string errorInfo = "- Last url is " + this.Request.Url + Environment.NewLine;
                errorInfo += "- Current execution file path is " + this.Request.CurrentExecutionFilePath + Environment.NewLine;
                errorInfo += "- Virtual path of current request is " + this.Request.FilePath + Environment.NewLine;
                errorInfo += "- Physical file system path is " + this.Request.PhysicalPath + Environment.NewLine;
                errorInfo += "- QueryString is " + ExtractQueryString(this.Request.QueryString) + Environment.NewLine;
                errorInfo += "- Current User is " + this.User.Identity.Name + Environment.NewLine;

                //log error
                ILoggingService logService = container.GetInstance<ILoggingService>();
                logService.LogError(lastException, errorInfo, "System");
                logService.SendErrorNotification(lastException, errorInfo);

                RouteData routeData = new RouteData();
                routeData.Values.Add("Controller", "Error");
                //specific action for specific HTTP Code
                switch ((lastException as HttpException).GetHttpCode())
                {
                    case 404:
                        // Page not found.
                        routeData.Values.Add("action", "HttpError404");
                        break;
                    case 500:
                        // Server error.
                        routeData.Values.Add("action", "HttpError500");
                        break;

                    // Here you can handle Views to other error codes.
                    default:
                        routeData.Values.Add("action", "Index");
                        break;
                }

                httpContext.ClearError();
                httpContext.Response.Clear();
                httpContext.Response.TrySkipIisCustomErrors = true;

                IConfigureService configService = container.GetInstance<IConfigureService>();
                IUserService userService = container.GetInstance<IUserService>();
                IRoleService roleService = container.GetInstance<IRoleService>();
                IActiveDirectoryService activeDirectoryService = container.GetInstance<IActiveDirectoryService>();
                //Call target Controller and pass the routeData.
                IController errorController = new ErrorController(logService, userService, roleService, activeDirectoryService, configService);
                errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
            }
        }

        private string ExtractQueryString(NameValueCollection queryStrings)
        {
            string results = string.Empty;
            foreach (string item in queryStrings.Keys)
            {
                results += string.Format("Key = {0}: Value = {1}, ", item, queryStrings[item]);
            }
            return results;
        }
    }
}
