﻿

//Windows Management
function HideWindow(strData) {
    var wdw = $('#' + strData).data('kendoWindow');
    wdw.close();
}

function ShowWindow(obj, isReferesh) {
    var wdw = $('#' + obj);
    if (isReferesh == true) wdw.data('kendoWindow').refresh();
    wdw.data('kendoWindow').center().open();
}

function new_window(url) {
    link = window.open(url, "Link", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1,width=1024,height=768,left=40,top=50");
}

//grid custom method/functions
function setGridDataSourceProperty(gridName, property, value) {
    var uid, model = null;
    uid = $(".k-edit-form-container").closest("[data-role=window]").data("uid");
    if (uid != undefined && uid != null) model = $(gridName).data("kendoGrid").dataSource.getByUid(uid);
    if (model != undefined && model != null) model.set(property, value);
}


jQuery.fn.serializeObject = function () {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function () {
        var value;

        if (this.value != null) {
            value = this.value;
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });

    return objectData;
};