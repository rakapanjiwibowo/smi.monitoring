﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Configuration;
using System.Xml;
using System.Net;
using System.Security.Principal;
using SourceCode.Hosting.Client.BaseAPI;
using SourceCode.Workflow.Client;
using SMI.EncryptTools.Agent;

namespace SMI.Monitoring.Services
{
    /// <summary>
    /// Using Binary K2
    /// http://community.k2.com/pfxaw45692/attachments/pfxaw45692/K2_blackpearl/19296/1/300.NRT-K2_Workflow_APIs_and_Services-Runtime.pdf
    /// </summary>
    public class K2BinaryService : IK2BinaryService, IDisposable
    {

        private string ListConfigure(string param)
        {

            using (MainEntities1 ent = new MainEntities1())
            {

                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                ent.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = @"Select 
                                    Description,
                                    Value
                                   From TblConfigure
                              Where Description = '{0}'";
                query = string.Format(query, param);
                var item = ent.Database.SqlQuery<TblConfigureViewModel>(query).FirstOrDefault();
                return item.Value;
            }
        }

        #region remark
        /*
        private string userAccountServices = ConfigurationManager.AppSettings["UserAccountService"];
        private string passwordServices = ConfigurationManager.AppSettings["PasswordAccountService"];
        private string domainServices = ConfigurationManager.AppSettings["DomainService"];
        private string k2ConnectionString = ConfigurationManager.AppSettings["K2ConnectionString"];
        private string k2Host = ConfigurationManager.AppSettings["K2Host"];
        private string k2Port = ConfigurationManager.AppSettings["K2Port"];
        private string k2Port5555 = ConfigurationManager.AppSettings["K2Port5555"];
        private string k2SecurityLabel = ConfigurationManager.AppSettings["K2SecurityLabel"];
        //private string K2UrlWebServices = ConfigurationManager.AppSettings["K2UrlWebServices"];
        private string K2FolderLOSDeposito = ConfigurationManager.AppSettings["K2FolderLOSDeposito"];
        private string K2PackageLOSDeposito_OL = ConfigurationManager.AppSettings["K2PackageLOSDeposito_OfferingLetter"];
        private string K2PackageLOSDeposito_EditOL = ConfigurationManager.AppSettings["K2PackageLOSDeposito_EditOfferingLetter"];
        private string K2PackageLOSDeposito_Placement = ConfigurationManager.AppSettings["K2PackageLOSDeposito_Placement"];
        private string K2PackageLOSDeposito_EditPlacement = ConfigurationManager.AppSettings["K2PackageLOSDeposito_EditPlacement"];
        */
        #endregion

        private string userAccountServices
        {
            get
            {
                var data = ListConfigure("UserAccountService");
                return data;
            }
        }

        private string passwordServices
        {
            get
            {
                var data = ListConfigure("PasswordAccountService");
                return data;
            }
        }

        private string domainServices
        {
            get
            {
                var data = ListConfigure("DomainService");
                return data;
            }
        }

        private string k2ConnectionString
        {
            get
            {
                var data = ListConfigure("K2ConnectionString");
                return data;
            }
        }

        private string k2Host
        {
            get
            {
                var data = ListConfigure("K2Host");
                return data;
            }
        }

        private string k2Port
        {
            get
            {
                var data = ListConfigure("K2Port");
                return data;
            }
        }

        private string k2Port5555
        {
            get
            {
                var data = ListConfigure("K2Port5555");
                return data;
            }
        }

        private string k2SecurityLabel
        {
            get
            {
                var data = ListConfigure("K2SecurityLabel");
                return data;
            }
        }

        //private string K2UrlWebServices
        //{
        //    get
        //    {
        //        var data = ListConfigure("K2UrlWebServices");
        //        return data;
        //    }
        //}

        private string K2FolderLOSDeposito
        {
            get
            {
                var data = ListConfigure("K2FolderLOSDeposito");
                return data;
            }
        }

        private string K2PackageLOSDeposito_OL
        {
            get
            {
                var data = ListConfigure("K2PackageLOSDeposito_OfferingLetter");
                return data;
            }
        }


        private string K2PackageLOSDeposito_EditOL
        {
            get
            {
                var data = ListConfigure("K2PackageLOSDeposito_EditOfferingLetter");
                return data;
            }
        }

        private string K2PackageLOSDeposito_Placement
        {
            get
            {
                var data = ListConfigure("K2PackageLOSDeposito_Placement");
                return data;
            }
        }

        private string K2PackageLOSDeposito_EditPlacement
        {
            get
            {
                var data = ListConfigure("K2PackageLOSDeposito_EditPlacement");
                return data;
            }
        }

        private MapperConfiguration config;
        private IMapper mapper;
        private SCConnectionStringBuilder k2ConnectionBuilder;
        private SourceCode.Workflow.Client.Connection k2Connection;
        private SourceCode.Workflow.Management.WorkflowManagementServer k2WorkflowServer;
        //private Workli worklistNavigationService;
        //private ProcessNavigationServiceClient processNavigationService;
        private SourceCode.Workflow.Client.ProcessInstance procInst;
        private Dictionary<string, object> dataFieldsCollection;

        private string _defaultUserAccount;
        public string DefaultUserAccount
        {
            get
            {
                return _defaultUserAccount;
            }
            set
            {
                _defaultUserAccount = string.Format("{0}:{1}", k2SecurityLabel, value);
            }
        }

        public K2BinaryService()
        {
            InitiaK2Connection();
            procInst = null;
            dataFieldsCollection = new Dictionary<string, object>();
        }

        private void InitiaK2Connection()
        {
            k2Connection = new Connection();
            k2Connection.Open(k2Host, k2ConnectionString);

        }

        /// <summary>
        /// https://help.k2.com/onlinehelp/k2blackpearl/devref/4.6.7/webframe.html#start_a_workflow_using_WCF_service.html
        /// </summary>
        /// <returns></returns>
        public long StartProcessInstance(string folioNumber, ref string viewFlowUrl)
        {
            try
            {
                procInst = k2Connection.CreateProcessInstance(K2PackageLOSDeposito_OL);
                procInst.Folio = folioNumber;
                procInst.Priority = 1;
                foreach (var item in dataFieldsCollection)
                {
                    procInst.DataFields[item.Key].Value = item.Value;
                }


                //userAccount = string.Format("{0}:{1}",k2SecurityLabel, userAccount);
                k2Connection.ImpersonateUser(DefaultUserAccount);
                k2Connection.StartProcessInstance(procInst);
                //if (revertUser) k2Connection.RevertUser();
                //viewFlowUrl = procInst.ViewFlow;
                return procInst.ID;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return 0;
            }
        }

        public Dictionary<string, object> PopulateK2DataField(
            string folioNumber,
            long offeringLetterId,
            string offeringLetterDate,
            long bankId,
            string bankCode,
            string bankName,
            long branchId,
            string branchCode,
            string branchName,
            string offeringLetterCode,
            string minimumAmount,
            string noRekeningBank,
            string atasNamaRekening,
            decimal rate1Month,
            decimal rate3Month,
            decimal? oldRate1Month,
            decimal? oldRate3Month,
            string currency,
            string isBreakable,
            string notes,
            int statusId,
            string tlAccount,
            string tlName,
            string tlEmail,
            string creatorUserAccount,
            string creatorFullName,
            string creatorEmail,
            string urlApproval,
            string urlDetail,
            string urlRevise,
            string rejectReason,
            string isRevise
            )
        {
            dataFieldsCollection.Add("FolioNumber", folioNumber);
            dataFieldsCollection.Add("OfferingLetterId", offeringLetterId);
            dataFieldsCollection.Add("OfferingDate", offeringLetterDate);
            dataFieldsCollection.Add("BankId", bankId);
            dataFieldsCollection.Add("BankCode", bankCode);
            dataFieldsCollection.Add("BankName", bankName);
            dataFieldsCollection.Add("BranchId", branchId);
            dataFieldsCollection.Add("BranchCode", branchCode);
            dataFieldsCollection.Add("BranchName", branchName);
            dataFieldsCollection.Add("OfferingLetterCode", offeringLetterCode);
            dataFieldsCollection.Add("MinimumAmount", minimumAmount);
            dataFieldsCollection.Add("NoRekeningBank", noRekeningBank);
            dataFieldsCollection.Add("AtasNamaRekening", atasNamaRekening);
            dataFieldsCollection.Add("Rate1Month", rate1Month);
            dataFieldsCollection.Add("Rate3Month", rate3Month);
            dataFieldsCollection.Add("OldRate1Month", oldRate1Month);
            dataFieldsCollection.Add("OldRate3Month", oldRate3Month);
            dataFieldsCollection.Add("Currency", currency);
            dataFieldsCollection.Add("IsBreakable", isBreakable);
            dataFieldsCollection.Add("Notes", notes);
            dataFieldsCollection.Add("StatusId", statusId);
            dataFieldsCollection.Add("TLAccount", tlAccount);
            dataFieldsCollection.Add("TLName", tlName);
            dataFieldsCollection.Add("TLEmail", tlEmail);
            dataFieldsCollection.Add("CreatorUserAccount", creatorUserAccount);
            dataFieldsCollection.Add("CreatorFullName", creatorFullName);
            dataFieldsCollection.Add("CreatorEmail", creatorEmail);
            dataFieldsCollection.Add("ApplicationUrl", urlApproval);
            dataFieldsCollection.Add("UrlDetail", urlDetail);
            dataFieldsCollection.Add("UrlRevise", urlRevise);
            dataFieldsCollection.Add("RejectReason", rejectReason);
            dataFieldsCollection.Add("IsRevise", isRevise);
            return dataFieldsCollection;
        }

        //====================

        public long StartProcessInstancePlacement(string folioNumber, ref string viewFlowUrl)
        {
            try
            {
                procInst = k2Connection.CreateProcessInstance(K2PackageLOSDeposito_Placement);
                procInst.Folio = folioNumber;
                procInst.Priority = 1;
                foreach (var item in dataFieldsCollection)
                {
                    procInst.DataFields[item.Key].Value = item.Value;
                }

                k2Connection.ImpersonateUser(DefaultUserAccount);
                k2Connection.StartProcessInstance(procInst);
                return procInst.ID;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return 0;
            }

        }

        public Dictionary<string, object> PopulateK2DataFieldPlacement(
             long reqPlacementId,
             string reqPlacementNumber,
             string requestDate,
             string dirUserAccount,
             string dirFullName,
             string dirEmail,
             string creatorUserAccount,
             string creatorFullName,
             string creatorEmail,
             string depositoDetail,
             string urlDetail,
             string approvalUrl,
             string urlRevise,
             string isRevise
          )
        {
            dataFieldsCollection.Add("ReqPlacementId", reqPlacementId);
            dataFieldsCollection.Add("ReqPlacementNumber",reqPlacementNumber);
            dataFieldsCollection.Add("RequestDate", requestDate);
            dataFieldsCollection.Add("DirAccount", dirUserAccount);
            dataFieldsCollection.Add("DirFullName", dirFullName);
            dataFieldsCollection.Add("DirEmail", dirEmail);
            dataFieldsCollection.Add("CreatorUserAccount", creatorUserAccount);
            dataFieldsCollection.Add("CreatorFullName", creatorFullName);
            dataFieldsCollection.Add("CreatorEmail", creatorEmail);
            dataFieldsCollection.Add("DepositoDetail", depositoDetail);
            dataFieldsCollection.Add("UrlDetail", urlDetail);
            dataFieldsCollection.Add("ApplicationUrl", approvalUrl);
            dataFieldsCollection.Add("UrlRevise", urlRevise);
            dataFieldsCollection.Add("Is_Revise", isRevise);
            return dataFieldsCollection;
        }

        public IEnumerable<WorklistItem> ListK2WorklistItemPlacement(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message)
        {
            var listK2Worklist = new List<WorklistItem>();
            try
            {
                var criteria = new WorklistCriteria();
                if (!string.IsNullOrEmpty(processFolder))
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, processFolder);
                }
                else
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, K2FolderLOSDeposito);
                }
                if (!string.IsNullOrEmpty(folioNumber)) criteria.AddFilterField(WCField.ProcessFolio, WCCompare.Equal, folioNumber);
                if (!string.IsNullOrEmpty(serialNumber)) criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, serialNumber);

                if (procIntId > 0) criteria.AddFilterField(WCField.ProcessID, WCCompare.Equal, procIntId);
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var workList = k2Connection.OpenWorklist(criteria);
                listK2Worklist.AddRange(workList.Cast<WorklistItem>().ToList());
                return listK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return listK2Worklist;
            }
        }

        //====================

        public long StartProcessInstanceEditPlacement(string folioNumber, ref string viewFlowUrl)
        {
            try
            {
                procInst = k2Connection.CreateProcessInstance(K2PackageLOSDeposito_EditPlacement);
                procInst.Folio = folioNumber;
                procInst.Priority = 1;
                foreach (var item in dataFieldsCollection)
                {
                    procInst.DataFields[item.Key].Value = item.Value;
                }

                k2Connection.ImpersonateUser(DefaultUserAccount);
                k2Connection.StartProcessInstance(procInst);
                return procInst.ID;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return 0;
            }

        }

        public Dictionary<string, object> PopulateK2DataFieldEditPlacement(
             long reqPlacementId,
             string reqPlacementNumber,
             string requestDate,
             string dirUserAccount,
             string dirFullName,
             string dirEmail,
             string creatorUserAccount,
             string creatorFullName,
             string creatorEmail,
             string depositoDetail,
             string urlDetail,
             string approvalUrl,
             string urlRevise,
             string isRevise
          )
        {
            dataFieldsCollection.Add("ReqPlacementId", reqPlacementId);
            dataFieldsCollection.Add("ReqPlacementNumber", reqPlacementNumber);
            dataFieldsCollection.Add("RequestDate", requestDate);
            dataFieldsCollection.Add("DirAccount", dirUserAccount);
            dataFieldsCollection.Add("DirFullName", dirFullName);
            dataFieldsCollection.Add("DirEmail", dirEmail);
            dataFieldsCollection.Add("CreatorUserAccount", creatorUserAccount);
            dataFieldsCollection.Add("CreatorFullName", creatorFullName);
            dataFieldsCollection.Add("CreatorEmail", creatorEmail);
            dataFieldsCollection.Add("DepositoDetail", depositoDetail);
            dataFieldsCollection.Add("UrlDetail", urlDetail);
            dataFieldsCollection.Add("ApplicationUrl", approvalUrl);
            dataFieldsCollection.Add("UrlRevise", urlRevise);
            dataFieldsCollection.Add("Is_Revise", isRevise);
            return dataFieldsCollection;
        }

        public IEnumerable<WorklistItem> ListK2WorklistItemEditPlacement(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message)
        {
            var listK2Worklist = new List<WorklistItem>();
            try
            {
                var criteria = new WorklistCriteria();
                if (!string.IsNullOrEmpty(processFolder))
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, processFolder);
                }
                else
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, K2FolderLOSDeposito);
                }
                if (!string.IsNullOrEmpty(folioNumber)) criteria.AddFilterField(WCField.ProcessFolio, WCCompare.Equal, folioNumber);
                if (!string.IsNullOrEmpty(serialNumber)) criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, serialNumber);

                if (procIntId > 0) criteria.AddFilterField(WCField.ProcessID, WCCompare.Equal, procIntId);
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var workList = k2Connection.OpenWorklist(criteria);
                listK2Worklist.AddRange(workList.Cast<WorklistItem>().ToList());
                return listK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return listK2Worklist;
            }
        }

        //==============
        #region Default K2 Methods    

        public bool RevertUser()
        {
            var result = k2Connection.RevertUser();
            return result;
        }

        public string GetByProcessId(string folioNumber)
        {
            return "";
        }

        public ProcessInstance GetProcessInstances(int procInstId, ref string message)
        {
            message = "";
            try
            {
                k2Connection.ImpersonateUser(DefaultUserAccount);
                procInst = k2Connection.OpenProcessInstance(procInstId);
                return procInst;
            }
            catch (Exception e)
            {
                message = e.Message;
                return null;
            }
        }

        public ProcessInstance OpenProcessInstance(int processInstanceId)
        {
            procInst = k2Connection.OpenProcessInstance(processInstanceId);
            return procInst;
        }

        public bool UpdateProcessInstance(int procInstId, Dictionary<string, object> datafields, ref string message)
        {
            message = "";
            try
            {
                k2Connection.ImpersonateUser(userAccountServices);  //set back as admin
                var procInstance = k2Connection.OpenProcessInstance(procInstId);
                foreach (var item in datafields)
                {
                    //procInst.DataFields[item.Key].Value = item.Value;
                    procInstance.DataFields[item.Key].Value = item.Value;
                }
                procInstance.Update();
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        public WorklistItem K2WorklistItem(string serialNumber, ref string message)
        {
            message = "";
            try
            {
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var itemK2Worklist = k2Connection.OpenWorklistItem(serialNumber);
                return itemK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return null;
            }
        }

        public bool ExecuteActionBySerialNumber(string serialNumber, Dictionary<string, object> dataFields, string action, ref string message)
        {
            message = "";
            try
            {
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var worklist = k2Connection.OpenWorklistItem(serialNumber);
                if (dataFields != null)
                {
                    foreach (var item in dataFields)
                    {
                        worklist.ProcessInstance.DataFields[item.Key].Value = item.Value;
                    }
                }
                worklist.Actions[action].Execute();
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        public IEnumerable<WorklistItem> ListK2WorklistItem(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message)
        {
            var listK2Worklist = new List<WorklistItem>();
            try
            {
                var criteria = new WorklistCriteria();
                //if (!string.IsNullOrEmpty(processFolder)) criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, processFolder);
                if (!string.IsNullOrEmpty(processFolder))
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, processFolder);
                }
                else
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, K2FolderLOSDeposito);
                }
                if (!string.IsNullOrEmpty(folioNumber)) criteria.AddFilterField(WCField.ProcessFolio, WCCompare.Equal, folioNumber);
                if (!string.IsNullOrEmpty(serialNumber)) criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, serialNumber);
                
                if (procIntId > 0) criteria.AddFilterField(WCField.ProcessID, WCCompare.Equal, procIntId);
                k2Connection.ImpersonateUser(DefaultUserAccount);
                //k2Connection.ImpersonateUser(userAccountServices);
                var workList = k2Connection.OpenWorklist(criteria);
                listK2Worklist.AddRange(workList.Cast<WorklistItem>().ToList());
                return listK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return listK2Worklist;
            }
        }

        public IEnumerable<WorklistItem> ListK2WorklistItem(ref string message)
        {
            var listK2Worklist = new List<WorklistItem>();
            try
            {
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var workList = k2Connection.OpenWorklist();
                listK2Worklist.AddRange(workList.Cast<WorklistItem>().ToList());
                return listK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return listK2Worklist;
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        /// <param name="procInstId"></param>
        /// <param name="actionName"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool GoToActivity(int procInstId, string actionName, ref string message)
        {
            message = "";
            try
            {
                k2Connection.ImpersonateUser(DefaultUserAccount);
                k2WorkflowServer = new SourceCode.Workflow.Management.WorkflowManagementServer(k2Host, uint.Parse(k2Port5555));
                k2WorkflowServer.Open();
                k2WorkflowServer.GotoActivity(procInstId, actionName);
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        public bool GoToActivity(string serialNumber, string activity, ref string message)
        {
            message = "";
            try
            {
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var worklistItem = k2Connection.OpenWorklistItem(serialNumber);
                worklistItem.GotoActivity(activity);
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        /// <summary>
        /// List All Process Instances 
        /// Using SourceCode.Workflow.Management.WorkflowManagementServer
        /// </summary>
        /// <param name="packageName"></param>
        /// <param name="folio"></param>
        /// <param name="originator"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public List<SourceCode.Workflow.Management.ProcessInstance> FindProcessInstances(string packageName, string folio, string originator, string fromDate, string toDate, ref string message)
        {
            var result = new List<SourceCode.Workflow.Management.ProcessInstance>();
            message = "";
            try
            {
                k2Connection.ImpersonateUser(userAccountServices);
                k2WorkflowServer = new SourceCode.Workflow.Management.WorkflowManagementServer(k2Host, uint.Parse(k2Port5555));
                k2WorkflowServer.Open();
                var processInstances = k2WorkflowServer.GetProcessInstancesAll(packageName, folio, originator, fromDate, toDate);
                foreach (var processInstance in processInstances)
                {
                    result.Add((SourceCode.Workflow.Management.ProcessInstance)processInstance);
                }
                return result;
            }
            catch (Exception e)
            {
                message = e.Message;
                return result;
            }
        }

        #endregion

        public IEnumerable<SourceCode.Workflow.Management.WorklistItem> ListAllK2WorklistItem(string type = "")
        {
            string message = string.Empty;
            var listK2Worklist = new List<SourceCode.Workflow.Management.WorklistItem>(); ;
            try
            {
                string processName = string.Empty;
                if (type == "OL")
                {
                    processName = K2PackageLOSDeposito_OL;
                }
                else
                {
                    processName = K2PackageLOSDeposito_Placement;
                }
                string activityName = ""; //"NewOfferingLetter";

                k2WorkflowServer = new SourceCode.Workflow.Management.WorkflowManagementServer(k2Host, uint.Parse(k2Port5555));
                k2WorkflowServer.Open();
                var workList = k2WorkflowServer.GetWorklistItems(string.Empty, processName, activityName, string.Empty, string.Empty, string.Empty, string.Empty);
                listK2Worklist.AddRange(workList.Cast<SourceCode.Workflow.Management.WorklistItem>().ToList());
                return listK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return listK2Worklist;
            }
        }

        public void Dispose()
        {
            procInst = null;
            dataFieldsCollection.Clear();
            dataFieldsCollection = null;

            k2Connection.Close();
            k2Connection.Dispose();
            k2Connection = null;
        }

        #region Edit OfferingLetter
        public long StartProcessInstanceEditOL(string folioNumber, ref string viewFlowUrl)
        {
            try
            {
                procInst = k2Connection.CreateProcessInstance(K2PackageLOSDeposito_EditOL);
                procInst.Folio = folioNumber;
                procInst.Priority = 1;
                foreach (var item in dataFieldsCollection)
                {
                    procInst.DataFields[item.Key].Value = item.Value;
                }

                k2Connection.ImpersonateUser(DefaultUserAccount);
                k2Connection.StartProcessInstance(procInst);
                return procInst.ID;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return 0;
            }
        }

        public IEnumerable<WorklistItem> ListK2WorklistItemEditOL(string processFolder, string folioNumber, string serialNumber, int procIntId, ref string message)
        {
            var listK2Worklist = new List<WorklistItem>();
            try
            {
                var criteria = new WorklistCriteria();
                if (!string.IsNullOrEmpty(processFolder))
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, processFolder);
                }
                else
                {
                    criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, K2FolderLOSDeposito);
                }
                if (!string.IsNullOrEmpty(folioNumber)) criteria.AddFilterField(WCField.ProcessFolio, WCCompare.Equal, folioNumber);
                if (!string.IsNullOrEmpty(serialNumber)) criteria.AddFilterField(WCField.ProcessFolder, WCCompare.Equal, serialNumber);

                if (procIntId > 0) criteria.AddFilterField(WCField.ProcessID, WCCompare.Equal, procIntId);
                k2Connection.ImpersonateUser(DefaultUserAccount);
                var workList = k2Connection.OpenWorklist(criteria);
                listK2Worklist.AddRange(workList.Cast<WorklistItem>().ToList());
                return listK2Worklist;
            }
            catch (Exception e)
            {
                message = e.Message;
                return listK2Worklist;
            }
        }

        public Dictionary<string, object> PopulateK2DataFieldEditOL(
            string folioNumber,
            long offeringLetterId,
            string offeringLetterDate,
            long bankId,
            string bankCode,
            string bankName,
            long branchId,
            string branchCode,
            string branchName,
            string offeringLetterCode,
            string minimumAmount,
            string noRekeningBank,
            string atasNamaRekening,
            decimal rate1Month,
            decimal rate3Month,
            decimal? oldRate1Month,
            decimal? oldRate3Month,
            string currency,
            string isBreakable,
            string notes,
            int statusId,
            string tlAccount,
            string tlName,
            string tlEmail,
            string creatorUserAccount,
            string creatorFullName,
            string creatorEmail,
            string urlApproval,
            string urlDetail,
            string urlRevise,
            string rejectReason,
            string isRevise
            )
        {
            dataFieldsCollection.Add("FolioNumber", folioNumber);
            dataFieldsCollection.Add("OfferingLetterId", offeringLetterId);
            dataFieldsCollection.Add("OfferingDate", offeringLetterDate);
            dataFieldsCollection.Add("BankId", bankId);
            dataFieldsCollection.Add("BankCode", bankCode);
            dataFieldsCollection.Add("BankName", bankName);
            dataFieldsCollection.Add("BranchId", branchId);
            dataFieldsCollection.Add("BranchCode", branchCode);
            dataFieldsCollection.Add("BranchName", branchName);
            dataFieldsCollection.Add("OfferingLetterCode", offeringLetterCode);
            dataFieldsCollection.Add("MinimumAmount", minimumAmount);
            dataFieldsCollection.Add("NoRekeningBank", noRekeningBank);
            dataFieldsCollection.Add("AtasNamaRekening", atasNamaRekening);
            dataFieldsCollection.Add("Rate1Month", rate1Month);
            dataFieldsCollection.Add("Rate3Month", rate3Month);
            dataFieldsCollection.Add("OldRate1Month", oldRate1Month);
            dataFieldsCollection.Add("OldRate3Month", oldRate3Month);
            dataFieldsCollection.Add("Currency", currency);
            dataFieldsCollection.Add("IsBreakable", isBreakable);
            dataFieldsCollection.Add("Notes", notes);
            dataFieldsCollection.Add("StatusId", statusId);
            dataFieldsCollection.Add("TLAccount", tlAccount);
            dataFieldsCollection.Add("TLName", tlName);
            dataFieldsCollection.Add("TLEmail", tlEmail);
            dataFieldsCollection.Add("CreatorUserAccount", creatorUserAccount);
            dataFieldsCollection.Add("CreatorFullName", creatorFullName);
            dataFieldsCollection.Add("CreatorEmail", creatorEmail);
            dataFieldsCollection.Add("ApplicationUrl", urlApproval);
            dataFieldsCollection.Add("UrlDetail", urlDetail);
            dataFieldsCollection.Add("UrlRevise", urlRevise);
            dataFieldsCollection.Add("RejectReason", rejectReason);
            dataFieldsCollection.Add("IsRevise", isRevise);
            return dataFieldsCollection;
        }
        #endregion

    }

}
