﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Configuration;
using SMI.EncryptTools.Agent;

namespace SMI.Monitoring.Services
{
    public class ConfigureService : IConfigureService
    {
        private readonly OracleEntities context1;
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public ConfigureService(MainEntities1 _context)
        {
            context = _context;

            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblConfigure, TblConfigureViewModel>();
                cfg.CreateMap<TblConfigureViewModel, TblConfigure>();
            });
            mapper = config.CreateMapper();
        }

        public TblConfigureViewModel GetById(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.TblConfigures.Where(x => x.Id == Id).FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<TblConfigureViewModel>(result);
                    return entity;
                }
                return null;
            }
        }

        public IEnumerable<TblConfigureViewModel> List(Expression<Func<TblConfigure, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.TblConfigures.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<TblConfigureViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<TblConfigureViewModel>(config);
                    return result;
                }
            }
        }

        public TblConfigureViewModel Insert(TblConfigureViewModel item)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<TblConfigure>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }

        public void Insert(IEnumerable<TblConfigureViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<TblConfigure>(item);
                    context.Entry(entity).State = EntityState.Added;
                }
                context.SaveChanges();
            }
        }

        public void Edit(TblConfigureViewModel newItem)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.TblConfigures
                              where d.Id == newItem.Id
                              select d).FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<TblConfigureViewModel, TblConfigure>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Edit(IEnumerable<TblConfigureViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = (from d in context.TblConfigures
                                  where d.Id == item.Id
                                  select d).FirstOrDefault();

                    entity = mapper.Map(item, entity);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void Delete(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.TblConfigures
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void Delete(IEnumerable<TblConfigureViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
                foreach (var item in list)
                {
                    var entity = mapper.Map<TblConfigure>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }
    }
}
