﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.DirectoryServices;
using System.Configuration;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Domain2.ViewModel;



namespace SMI.Monitoring.Services
{
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        private DirectoryEntry getAdminDirectoryEntry(string username, ref string domain)
        {
            try
            {
                string connStr = string.Empty; //ConfigurationManager.AppSettings["ADConnectionString"];
                string adminUser = string.Empty; //ConfigurationManager.AppSettings["ADUsername"];
                string adminPassword = string.Empty; //ConfigurationManager.AppSettings["ADPassword"];
                string domainName = username.Split('\\').First().ToLower();

                DirectoryEntry directoryEntry = new DirectoryEntry(connStr, adminUser, adminPassword, AuthenticationTypes.Secure);
                domain = domainName;
                return directoryEntry;
            }
            catch (Exception ex)
            {
                throw new Exception("getAdminDirectoryEntry " + ex.InnerException.Message);
            }
        }

        public IEnumerable<ActiveDirectoryViewModel> GetListPersonalInformations(string username, string domain)
        {
            //const int ADS_UF_LOCKOUT = 0x00000010;
            List<ActiveDirectoryViewModel> ListADUser = new List<ActiveDirectoryViewModel>();
            try
            {
                if (!String.IsNullOrEmpty(username))
                {
                    string dom = "";
                    DirectoryEntry entry = getAdminDirectoryEntry(domain.ToLower(), ref dom);
                    DirectorySearcher dSearch = new DirectorySearcher(entry);

                    if (username.Contains('@'))
                    {
                        dSearch.Filter = "(mail=*" + username + ")";
                    }
                    else
                    {
                        dSearch.Filter = "(samaccountname=" + username + ")";
                    }
                    SearchResultCollection resCollection = dSearch.FindAll();
                    if (resCollection == null || resCollection.Count == 0)
                    {
                        dSearch.Filter = "(name=*" + username + "*)";
                        resCollection = dSearch.FindAll();
                    }
                    for (int i = 0; i < resCollection.Count; i++)
                    {
                        DirectoryEntry directoryEntry = resCollection[i].GetDirectoryEntry();
                        ActiveDirectoryViewModel advm = new ActiveDirectoryViewModel();
                        advm.Email = directoryEntry.Properties["mail"].Count <= 0 ? String.Empty : directoryEntry.Properties["mail"][0].ToString();
                        advm.AccountName = directoryEntry.Properties["samaccountname"].Count <= 0 ? String.Empty : dom + "\\" + directoryEntry.Properties["samaccountname"][0].ToString();
                        advm.Name = directoryEntry.Properties["name"].Count <= 0 ? String.Empty : directoryEntry.Properties["name"][0].ToString();
                        advm.Department = directoryEntry.Properties["department"].Count <= 0 ? String.Empty : directoryEntry.Properties["department"][0].ToString();
                        advm.Location = directoryEntry.Properties["physicalDeliveryOfficeName"].Count <= 0 ? String.Empty : directoryEntry.Properties["physicalDeliveryOfficeName"][0].ToString();
                        advm.Title = directoryEntry.Properties["title"].Count <= 0 ? String.Empty : directoryEntry.Properties["title"][0].ToString();
                        //advm.IsLocked = Convert.ToBoolean(directoryEntry.InvokeGet("IsAccountLocked"));
                        ListADUser.Add(advm);
                    }
                }
                return ListADUser;
            }
            catch (Exception ex)
            {
                throw new Exception("getIEGAccountInformation " + ex.Message);
            }
        }

        public ActiveDirectoryViewModel GetPersonalInformation(string username)
        {
            //const int ADS_UF_LOCKOUT = 0x00000010;
            ActiveDirectoryViewModel advm = null;
            try
            {
                if (!String.IsNullOrEmpty(username) && username.Contains("\\"))
                {
                    string dom = "";
                    string domain = username.Split('\\')[0];
                    username = username.Split('\\')[1];
                    DirectoryEntry entry = getAdminDirectoryEntry(domain.ToLower(), ref dom);
                    DirectorySearcher dSearch = new DirectorySearcher(entry);
                    dSearch.Filter = "(samaccountname=" + username + ")";
                    SearchResult res = dSearch.FindOne();
                    if (res == null)
                    {
                        dSearch.Filter = "(name=*" + username + "*)";
                        res = dSearch.FindOne();
                    }
                    DirectoryEntry directoryEntry = res.GetDirectoryEntry();
                    advm = new ActiveDirectoryViewModel();
                    advm.Email = directoryEntry.Properties["mail"].Count <= 0 ? String.Empty : directoryEntry.Properties["mail"][0].ToString();
                    advm.AccountName = directoryEntry.Properties["samaccountname"].Count <= 0 ? String.Empty : dom + "\\" + directoryEntry.Properties["samaccountname"][0].ToString();
                    advm.Name = directoryEntry.Properties["name"].Count <= 0 ? String.Empty : directoryEntry.Properties["name"][0].ToString();
                    advm.Department = directoryEntry.Properties["department"].Count <= 0 ? String.Empty : directoryEntry.Properties["department"][0].ToString();
                    advm.Location = directoryEntry.Properties["physicalDeliveryOfficeName"].Count <= 0 ? String.Empty : directoryEntry.Properties["physicalDeliveryOfficeName"][0].ToString();
                    advm.Title = directoryEntry.Properties["title"].Count <= 0 ? String.Empty : directoryEntry.Properties["title"][0].ToString();
                    //advm.IsLocked = Convert.ToBoolean(directoryEntry.InvokeGet("IsAccountLocked"));
                }
                return advm;
            }
            catch (Exception ex)
            {
                throw new Exception("getIEGAccountInformation " + ex.Message);
            }
        }

        public ActiveDirectoryViewModel GetPersonalInformation(string username, string domain)
        {
            //const int ADS_UF_LOCKOUT = 0x00000010;
            ActiveDirectoryViewModel advm = new ActiveDirectoryViewModel();
            try
            {
                if (!String.IsNullOrEmpty(username))
                {
                    string dom = "";
                    DirectoryEntry entry = getAdminDirectoryEntry(domain.ToLower(), ref dom);
                    DirectorySearcher dSearch = new DirectorySearcher(entry);

                    if (username.Contains('@'))
                    {
                        dSearch.Filter = "(mail=*" + username + ")";
                    }
                    else
                    {
                        dSearch.Filter = "(samaccountname=" + username + ")";
                    }
                    SearchResult res = dSearch.FindOne();
                    if (res == null)
                    {
                        dSearch.Filter = "(name=*" + username + "*)";
                        res = dSearch.FindOne();
                    }
                    DirectoryEntry directoryEntry = res.GetDirectoryEntry();
                    advm = new ActiveDirectoryViewModel();
                    advm.Email = directoryEntry.Properties["mail"].Count <= 0 ? String.Empty : directoryEntry.Properties["mail"][0].ToString();
                    advm.AccountName = directoryEntry.Properties["samaccountname"].Count <= 0 ? String.Empty : dom + "\\" + directoryEntry.Properties["samaccountname"][0].ToString();
                    advm.Name = directoryEntry.Properties["name"].Count <= 0 ? String.Empty : directoryEntry.Properties["name"][0].ToString();
                    advm.Department = directoryEntry.Properties["department"].Count <= 0 ? String.Empty : directoryEntry.Properties["department"][0].ToString();
                    advm.Location = directoryEntry.Properties["physicalDeliveryOfficeName"].Count <= 0 ? String.Empty : directoryEntry.Properties["physicalDeliveryOfficeName"][0].ToString();
                    advm.Title = directoryEntry.Properties["title"].Count <= 0 ? String.Empty : directoryEntry.Properties["title"][0].ToString();
                    //advm.IsLocked = Convert.ToBoolean(directoryEntry.InvokeGet("IsAccountLocked"));
                }
                return advm;
            }
            catch (Exception ex)
            {
                throw new Exception("getIEGAccountInformation " + ex.Message);
            }
        }
    }
}
