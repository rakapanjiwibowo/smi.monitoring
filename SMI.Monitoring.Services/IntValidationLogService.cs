﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Services
{
    public class IntValidationLogService : IIntValidationLogService
    {
        private readonly OracleEntities context1;
        //private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;


        public IntValidationLogService(OracleEntities _context)
        {
            context1 = _context;
            InitializeMapper();
        }

        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SMI_INT_VALIDATION_LOG, Int_Validation_LogViewModel>();
                cfg.CreateMap<Int_Validation_LogViewModel, SMI_INT_VALIDATION_LOG>();
            });
            mapper = config.CreateMapper();
        }

        public Int_Validation_LogViewModel GetById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_INT_VALIDATION_LOG.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<Int_Validation_LogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<Int_Validation_LogViewModel> List(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_VALIDATION_LOG.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            // }
        }

        //        public void Delete(IEnumerable<Int_Batch_ProcessViewModel> list)
        //        {
        //            using (OracleEntities context = new OracleEntities())
        //            {
        //                //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //                //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //                //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //                foreach (var item in list)
        //                {
        //                    var entity = mapper.Map<Int_Batch_ProcessViewModel>(item);
        //                    context.Entry(entity).State = EntityState.Deleted;
        //                }
        //                context.SaveChanges();
        //            }
        //        }

        //        public void Delete(string Id)
        //        {
        //            using (OracleEntities context = new OracleEntities())
        //            {
        //                //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //                //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);

        //                //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
        //                var entity = (from d in context.SMI_INT_BATCH_PROCESS
        //                              where d.INT_SOURCE_ID == Id
        //                              select d).FirstOrDefault();

        //                if (entity != null)
        //                {
        //                    context.Entry(entity).State = EntityState.Deleted;
        //                    context.SaveChanges();
        //                }
        //            }
        //        }

        //        public void Edit(IEnumerable<Int_Batch_ProcessViewModel> list)
        //        {
        //            using (OracleEntities context = new OracleEntities())
        //            {
        //                //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //                //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);

        //                //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
        //                foreach (var item in list)
        //                {
        //                    var entity = (from d in context.SMI_INT_BATCH_PROCESS
        //                                  where d.INT_SOURCE_ID == item.INT_SOURCE_ID
        //                                  select d).FirstOrDefault();

        //                    entity = mapper.Map(item, entity);
        //                    context.Entry(entity).State = EntityState.Modified;
        //                }
        //                context.SaveChanges();
        //            }
        //        }

        //        public void Edit(Int_Batch_ProcessViewModel newItem)
        //        {
        //            using (OracleEntities context = new OracleEntities())
        //            {
        //                //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //                //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //                //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //                var entity = (from d in context.SMI_INT_BATCH_PROCESS
        //                              where d.INT_SOURCE_ID == newItem.INT_SOURCE_ID
        //                              select d).FirstOrDefault();

        //                var localConfig = new MapperConfiguration(cfg =>
        //                {
        //                    cfg.CreateMap<Int_Batch_ProcessViewModel, SMI_INT_BATCH_PROCESS>()
        //                    .ForMember(src => src.CREATED_BY, opt => opt.Ignore())
        //                    .ForMember(src => src.CREATION_DATE, opt => opt.Ignore());
        //                });
        //                var localMapper = localConfig.CreateMapper();
        //                entity = localMapper.Map(newItem, entity);
        //                context.Entry(entity).State = EntityState.Modified;
        //                context.SaveChanges();
        //            }
        //        }

        //        public void Insert(IEnumerable<Int_Batch_ProcessViewModel> list)
        //        {
        //            using (OracleEntities context = new OracleEntities())
        //            {
        //                //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //                //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //                //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //                foreach (var item in list)
        //                {
        //                    var entity = mapper.Map<SMI_INT_BATCH_PROCESS>(item);
        //                    context.Entry(entity).State = EntityState.Added;
        //                }
        //                context.SaveChanges();
        //            }
        //        }

        //        public Int_Batch_ProcessViewModel Insert(Int_Batch_ProcessViewModel item)
        //        {
        //            using (OracleEntities context = new OracleEntities())
        //            {
        //                //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //                //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //                //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //                var entity = mapper.Map<SMI_INT_BATCH_PROCESS>(item);
        //                context.Entry(entity).State = EntityState.Added;
        //                context.SaveChanges();
        //                item.INT_SOURCE_ID = entity.INT_SOURCE_ID;
        //                return item;
        //            }
        //        }

    }
}
