﻿using AutoMapper;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Services
{
    public class MappingRoleModuleService : IMappingRoleModuleService
    {
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public MappingRoleModuleService(MainEntities1 _context)
        {
            context = _context;
            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MappingRoleModule, MappingRoleModuleViewModel>();
                cfg.CreateMap<MappingRoleModuleViewModel, MappingRoleModule>();
                cfg.CreateMap<VwMappingRoleModule, VwMappingRoleModuleViewModel>();
                cfg.CreateMap<VwMappingRoleModuleViewModel, VwMappingRoleModule>();
            });
            mapper = config.CreateMapper();
        }

        public MappingRoleModuleViewModel GetById(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.MappingRoleModules.Where(x => x.Id == Id).FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<MappingRoleModuleViewModel>(result);
                    return entity;
                }
                return null;
            }
        }

        public IEnumerable<MappingRoleModuleViewModel> List(Expression<Func<MappingRoleModule, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.MappingRoleModules.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<MappingRoleModuleViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<MappingRoleModuleViewModel>(config);
                    return result;
                }
            }
        }


        public IEnumerable<VwMappingRoleModuleViewModel> ListView(Expression<Func<VwMappingRoleModule, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.VwMappingRoleModules.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<VwMappingRoleModuleViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<VwMappingRoleModuleViewModel>(config);
                    return result;
                }
            }
        }

        public MappingRoleModuleViewModel Insert(MappingRoleModuleViewModel item)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<MappingRoleModule>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }


        public void Insert(IEnumerable<MappingRoleModuleViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<MappingRoleModule>(item);
                    context.Entry(entity).State = EntityState.Added;
                }
                context.SaveChanges();
            }
        }

        public void Edit(MappingRoleModuleViewModel newItem)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.MappingRoleModules
                              where d.Id == newItem.Id
                              select d).FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<MappingRoleModuleViewModel, MappingRoleModule>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Edit(IEnumerable<MappingRoleModuleViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = (from d in context.MappingRoleModules
                                  where d.Id == item.Id
                                  select d).FirstOrDefault();

                    entity = mapper.Map(item, entity);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void Delete(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.MappingRoleModules
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void Delete(IEnumerable<MappingRoleModuleViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<MappingRoleModule>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }
    }
}
