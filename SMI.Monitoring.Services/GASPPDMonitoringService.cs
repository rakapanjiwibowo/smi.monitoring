﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Services
{
    public class GASPPDMonitoringService : IGASPPDService
    {
        private readonly MainEntitiesSPPD context1;
        private readonly vr1_staging_prodEntities context2;

        private MapperConfiguration config;
        private IMapper mapper;


        public GASPPDMonitoringService(MainEntitiesSPPD _context)
        {
            context1 = _context;
            InitializeMapper();
        }


        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GASPPDMonitoringViewModel, SPPD>();
                cfg.CreateMap<SPPD, GASPPDMonitoringViewModel>();
            });

            mapper = config.CreateMapper();
        }


        #region GASPPDMonitoring
        public GASPPDMonitoringViewModel GetById( long ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SPPDs.Where(x => x.ID == ID ).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<GASPPDMonitoringViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        //public IEnumerable<GASPPDMonitoringViewModel> List(Expression<Func<SPPD, bool>> predicate = null)
        //{
        //    //using (oracleentities context = new oracleentities())
        //    //{
        //    //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //    //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //    //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //    var query = context1.SPPDs.AsQueryable().AsNoTracking();
        //    if (predicate != null)
        //    {
        //        var result = query.Where(predicate).ProjectToList<GASPPDMonitoringViewModel>(config);
        //        return result;
        //    }
        //    else
        //    {
        //        var result = query.ProjectToList<GASPPDMonitoringViewModel>(config);
        //        return result;
        //    }
        //}

        public IEnumerable<GASPPDMonitoringViewModel> List()
        {
            var query = string.Empty;
            using (MainEntitiesSPPD context = new MainEntitiesSPPD())
            {
                query = @"SELECT DISTINCT
                            SPPD.ID,
                         SPPD.Doc_Id ,
                         SPPD.Requestor_ID,
                            SPPD.Travel_Purpose_Desc,
                         SPPD.Status_WF,
                            SPPD.Created_Date
                      FROM SPPD
                      JOIN GAMonitoring_Oracle 
                      ON SPPD.Doc_Id = GAMonitoring_Oracle.H02
                      WHERE SPPD.Status_WF = 'Approved'
                ";

                #region Test Saja
                //query = @"SELECT
                //            GAMonitoring_Oracle.IFACE_CODE
                //      FROM  GAMonitoring_Oracle
                //";
                #endregion

                var result = context1.Database.SqlQuery<GASPPDMonitoringViewModel>(query).ToList();
                return result;

            }

        }

        public IEnumerable<VwMappingK2GASPPDViewModel> ListK2()
        {
            var query = string.Empty;
            query = @"SELECT [Seq]
                  ,[Group_ID]
                  ,[IFACE_CODE]
                  ,[CREATION_DATE]
                  ,[START_DATE]
                  ,[FINISH_DATE]
                  ,[STATUS]
                  ,[RESULT]
                  ,[NOTE]
                  ,[H01]
                  ,[H02]
                  ,[H03]
                  ,[H04]
                  ,[H05]
                  ,[H06]
                  ,[H07]
                  ,[H08]
                  ,[H09]
                  ,[H10]
                  ,[H11]
                  ,[H12]
                  ,[H13]
                  ,[H14]
                  ,[H15]
                  ,[H16]
                  ,[H17]
                  ,[H18]
                  ,[H19]
                  ,[H20]
                  ,[H21]
                  ,[H22]
                  ,[H23]
                  ,[H24]
                  ,[H25]
                  ,[H26]
                  ,[H27]
                  ,[H28]
                  ,[H29]
                  ,[H30]
                  ,[L01]
                  ,[L02]
                  ,[L03]
                  ,[L04]
                  ,[L05]
                  ,[L06]
                  ,[L07]
                  ,[L08]
                  ,[L09]
                  ,[L10]
                  ,[L11]
                  ,[L12]
                  ,[L13]
                  ,[L14]
                  ,[L15]
                  ,[L16]
                  ,[L17]
                  ,[L18]
                  ,[L19]
                  ,[L20]
                  ,[L21]
                  ,[L22]
                  ,[L23]
                  ,[L24]
                  ,[L25]
                  ,[Oracle_Seq]
                  ,[Copy_Date]
              FROM [SMI.SPPD].[dbo].[GAMonitoring_Oracle]
            ";
            var result = context1.Database.SqlQuery<VwMappingK2GASPPDViewModel>(query).ToList();
            return result;
        }

        #endregion


    }
}
