﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data.Entity;
using System.Net.Mail;
using System.Configuration;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.EncryptTools.Agent;

namespace SMI.Monitoring.Services
{
    public class LoggingService : ILoggingService
    {
        #region remark
        /*
        string userAccountService = ConfigurationManager.AppSettings["UserAccountService"];
        string passwordAccountService = ConfigurationManager.AppSettings["PasswordAccountService"];
        string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
        string SystemEmailAddress = ConfigurationManager.AppSettings["SystemEmailAddress"];
        string AdministratorEmail = ConfigurationManager.AppSettings["AdministratorEmail"];
        */
        #endregion
        private readonly OracleEntities context1;

        //private readonly MainEntities context;

        private readonly MainEntities1 context;

        private string ListConfigure(string param)
        {
            var query = @"Select 
                            Description,
                            Value
                            From TblConfigure
                        Where Description = '{0}'";
            query = string.Format(query, param);
            var item = context.Database.SqlQuery<TblConfigureViewModel>(query).FirstOrDefault();
            return item.Value;
        }

        string userAccountService
        {
            get
            {
                var data = ListConfigure("UserAccountService");
                return data;
            }
        }
        string passwordAccountService
        {
            get
            {
                var data = ListConfigure("PasswordAccountService");
                return data;
            }
        }
        string smtpServer
        {
            get
            {
                var data = ListConfigure("SmtpServer");
                return data;
            }
        }
        string SystemEmailAddress
        {
            get
            {
                var data = ListConfigure("SystemEmailAddress");
                return data;
            }
        }
        string AdministratorEmail
        {
            get
            {
                var data = ListConfigure("AdministratorEmail");
                return data;
            }
        }

        //public LoggingService(OracleEntities _context)
        //{
        //    context1 = _context;
        //}

        public LoggingService(MainEntities1 _context)
        {
            context = _context;
        }


        public void LogError(LogErrorViewModel item)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
               // Ini nanti bisa ke elmah, atau apapun logging driver-nya
                Mapper.CreateMap<LogErrorViewModel, LogError>();
                var entity = Mapper.Map<LogError>(item);
                context.Entry(entity).State = EntityState.Added;

                context.SaveChanges();
                item.Id = entity.Id;
           }
        }

        public void LogError(Exception e, string addMessage = null, string user = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                string detailException = GenerateMessageError(string.Empty, e);
                if (e.InnerException != null) detailException = GenerateMessageError(detailException, e.InnerException);
                if (!string.IsNullOrEmpty(addMessage))
                {
                    detailException = string.Format("{0}{1}Another message: {2}", detailException, Environment.NewLine, addMessage);
                }

                var item = new LogErrorViewModel
                {
                    Message = e.Message,
                    Description = detailException,
                    CreatedBy = user,
                    CreatedDate = DateTime.Now
                };

                LogError(item);
            }
        }

        public void SendErrorNotification(Exception e, string addMessage = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                string detailException = GenerateMessageError(string.Empty, e);
                if (e.InnerException != null) detailException = GenerateMessageError(detailException, e.InnerException);
                string content = string.Format("{0}{1}{2}{3}{4}{5}", "Found Unhadled Exception. Details: ", Environment.NewLine, detailException, Environment.NewLine, "Another Message ", addMessage);

                System.Net.Mail.MailMessage mailObject = new System.Net.Mail.MailMessage();
                SmtpClient SMTPServer = new SmtpClient(smtpServer);
                //SMTPServer.UseDefaultCredentials = true;
                SMTPServer.Credentials = new System.Net.NetworkCredential("", "");
                mailObject.From = new MailAddress(SystemEmailAddress);
                mailObject.Subject = "SMI Originasi Deposito Unhandled Exception";
                mailObject.Body = content;
                mailObject.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(AdministratorEmail))
                {
                    AdministratorEmail.Split(';').ToList().ForEach(email =>
                    {
                        if (!string.IsNullOrEmpty(email))
                            mailObject.To.Add(email);
                    });
                }

                try
                {
                    SMTPServer.Send(mailObject);
                }
                catch (Exception ex)
                {
                    LogError(ex, null, "System");
                }
            }
        }


        #region private method
        private string GenerateMessageError(string headerMessage, Exception e)
        {
            StringBuilder buffer = new StringBuilder();
            if (!string.IsNullOrEmpty(headerMessage)) buffer.AppendLine(headerMessage);
            buffer.AppendLine(string.Format("Message: {0} ", e.Message));
            buffer.AppendLine(string.Format("Stacktrace: {0} ", e.StackTrace));
            buffer.AppendLine(string.Format("SourceError: {0} ", e.Source));
            buffer.AppendLine(string.Format("Data: {0} ", ExtractDataException(e.Data)));
            buffer.AppendLine(string.Format("MethodName: {0} ", e.TargetSite));
            return string.Format("{0}", buffer.ToString());
        }

        private string ExtractDataException(IDictionary data)
        {
            string result = string.Empty;
            if (data.Count > 0)
            {
                result = "Data exception is : " + Environment.NewLine;
                foreach (DictionaryEntry de in data)
                    result = string.Format("Key: {0}, Value: {1} {2}", de.Key.ToString(), de.Value.ToString(), Environment.NewLine);
            }
            else result = "No data";
            return result;
        }
        #endregion
    }
}
