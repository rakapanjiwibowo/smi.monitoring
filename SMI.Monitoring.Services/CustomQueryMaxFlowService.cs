﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;

namespace SMI.Monitoring.Services
{
    public class CustomQueryMaxFlowService : ICustomQueryMaxFlowService, IDisposable
    {
        private readonly MaxFlowEntities context;
        private MapperConfiguration config;
        private IMapper mapper;

        public CustomQueryMaxFlowService(MaxFlowEntities _context)
        {
            context = _context;
        }

        public IEnumerable<T> ExecuteSqlQuery<T>(string query, params object[] args)
        {
            var results = context.Database.SqlQuery<T>(query, args);
            return results;
        }

        public IEnumerable<T> ExecuteQuery<T>(string query, params object[] args)
        {
            var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreQuery<T>(query, args);
            return results;
        }

        public int ExecuteStoreCommand(string query, params object[] args)
        {
            var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreCommand(query, args);
            return results;
        }

        #region Specific Custom Query

        public IEnumerable< UserViewModel> ListLookUpUserAccount()
        {
            var query = @" SELECT
                            Employee_ID EmployeeId, REPLACE( ISNULL(AD_Account, ''), 'K2:', '') UserAccount, Employee_Name FullName,  Email, Param1 AS NIK
                            FROM General_Master_Employee";
            var results = this.ExecuteQuery<UserViewModel>(query).ToList();
            return results;
        }

        public UserViewModel LookUpUserAccount(string userAccount)
        {
            var query = @" SELECT
                            Employee_ID EmployeeId, REPLACE( ISNULL(AD_Account, ''), 'K2:', '') UserAccount, Employee_Name FullName,  Email, Param1 AS NIK
                            FROM General_Master_Employee
                            WHERE REPLACE( ISNULL(AD_Account, ''), 'K2:', '') = '{0}'";
            var result = this.ExecuteQuery<UserViewModel>(query, userAccount).FirstOrDefault();
            return result;
        }

        #endregion

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
