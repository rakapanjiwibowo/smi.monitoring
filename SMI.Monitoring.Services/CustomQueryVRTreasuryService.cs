﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;

namespace SMI.Monitoring.Services
{
    public class CustomQueryVRTreasuryService : ICustomQueryVRTreasuryService, IDisposable
    {
        private readonly vr2_smiEntities context;
        private MapperConfiguration config;
        private IMapper mapper;

        public CustomQueryVRTreasuryService(vr2_smiEntities _context)
        {
            context = _context;
        }

        public IEnumerable<T> VRExecuteSqlQuery<T>(string query, params object[] args)
        {
            var results = context.Database.SqlQuery<T>(query, args);
            return results;
        }

        public IEnumerable<T> VRExecuteQuery<T>(string query, params object[] args)
        {
            var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreQuery<T>(query, args);
            return results;
        }

        public int VRExecuteStoreCommand(string query, params object[] args)
        {
            var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreCommand(query, args);
            return results;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
