﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.EncryptTools.Agent;
using System.Configuration;

namespace SMI.Monitoring.Services
{
    public class UserService : IUserService
    {
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public UserService(MainEntities1 _context)
        {
            context = _context;
            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserViewModel>();
                cfg.CreateMap<UserViewModel, User>();
                cfg.CreateMap<Role, RoleViewModel>();
                //cfg.CreateMap<VwMappingRoleUser, VwMappingRoleUserViewModel>();
                //cfg.CreateMap<VwMappingRoleUserViewModel, VwMappingRoleUser>();
            });
            mapper = config.CreateMapper();
        }

        public void Delete(IEnumerable<UserViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<User>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }

        public void Delete(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.Users
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void Edit(IEnumerable<UserViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = (from d in context.Users
                                  where d.Id == item.Id
                                  select d).FirstOrDefault();

                    entity = mapper.Map(item, entity);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void Edit(UserViewModel newItem)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.Users
                              where d.Id == newItem.Id
                              select d).FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UserViewModel, User>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public UserViewModel FindByUserAccount(string userAccount)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.Users.Where(x => x.UserAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase)).AsNoTracking().ProjectToFirstOrDefault<UserViewModel>(config);
                return result;
            }
        }

        public long FindIdByUserAccount(string userAccount)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.Users.Where(x => x.UserAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase)).AsNoTracking().FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<UserViewModel>(result);
                    return entity.Id;
                }
                return default(long);
            }
        }

        public UserViewModel GetById(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.Users.Where(x => x.Id == Id).FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<UserViewModel>(result);
                    return entity;
                }
                return null;
            }
        }

        public string GetUserRole(string userAccount)
        {
            //throw new NotImplementedException();

            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = from role in context.Roles
                            join mappingRoleUser in context.VwMappingRoleUsers on role.Id equals mappingRoleUser.RoleId
                            where mappingRoleUser.UserAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase)
                            select role;
                var defaultRole = query.ProjectToFirstOrDefault<RoleViewModel>(config);
                if (defaultRole == null)
                {
                    //set by default is Normal User
                    defaultRole = context.Roles.Where(x => x.Id == (int)EnumRole.User).ProjectToFirstOrDefault<RoleViewModel>(config);
                }
                return defaultRole.RoleName;
            }

        }

        public void Insert(IEnumerable<UserViewModel> list)
        {
            foreach (var item in list)
            {
                var entity = mapper.Map<User>(item);
                context.Entry(entity).State = EntityState.Added;
            }
            context.SaveChanges();
        }

        public UserViewModel Insert(UserViewModel item)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<User>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }

        public IEnumerable<UserViewModel> List(Expression<Func<User, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.Users.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<UserViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<UserViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<UserViewModel> ListLookUpLoginUserAccount()
        {
            using (MaxFlowEntities maxFlowContext = new MaxFlowEntities())
            {
                var query = @" SELECT
                            General_Master_Employee.Employee_ID EmployeeId, REPLACE( ISNULL(AD_Account, ''), 'K2:', '') UserAccount, Employee_Name FullName,  Email, Param1 AS NIK, Position_Name PositionName, Section Divisi, Department 
                            FROM MaxFlow.dbo.General_Master_Employee 
                            LEFT JOIN MaxFlow.dbo.General_Mapping_Employee_Position ON General_Mapping_Employee_Position.Employee_ID = General_Master_Employee.Employee_ID
                            LEFT JOIN MaxFlow.dbo.General_Master_Position ON General_Mapping_Employee_Position.Position_ID = General_Master_Position.Position_ID";

                var results = maxFlowContext.Database.SqlQuery<UserViewModel>(query).ToList();
                return results;
            }
        }

        public UserViewModel LookUpLoginUserAccount(string userAccount)
        {
            using (MaxFlowEntities maxFlowContext = new MaxFlowEntities())
            {
                var query = @"SELECT
                            General_Master_Employee.Employee_ID EmployeeId, REPLACE( ISNULL(AD_Account, ''), 'K2:', '') UserAccount, Employee_Name FullName,  Email, Param1 AS NIK, Position_Name PositionName, Section Divisi, Department 
                            FROM MaxFlow.dbo.General_Master_Employee 
                            LEFT JOIN MaxFlow.dbo.General_Mapping_Employee_Position ON General_Mapping_Employee_Position.Employee_ID = General_Master_Employee.Employee_ID
                            LEFT JOIN MaxFlow.dbo.General_Master_Position ON General_Mapping_Employee_Position.Position_ID = General_Master_Position.Position_ID
                            WHERE REPLACE( ISNULL(AD_Account, ''), 'K2:', '') = '{0}'";
                query = string.Format(query, userAccount);
                //var result = (maxFlowContext as IObjectContextAdapter).ObjectContext.ExecuteStoreQuery<UserViewModel>(query);
                var result = maxFlowContext.Database.SqlQuery<UserViewModel>(query).FirstOrDefault();
                return result;
            }
        }

        public IEnumerable<UserViewModel> ListDivision()
        {
            throw new NotImplementedException();
            /*
             using (MaxFlowEntities maxFlowContext = new MaxFlowEntities())
            {
                var query = @"SELECT
	                            Section Divisi
                            FROM MaxFlow.dbo.General_Master_Employee
                            LEFT JOIN MaxFlow.dbo.General_Mapping_Employee_Position
	                            ON General_Mapping_Employee_Position.Employee_ID = General_Master_Employee.Employee_ID
                            LEFT JOIN MaxFlow.dbo.General_Master_Position
	                            ON General_Mapping_Employee_Position.Position_ID = General_Master_Position.Position_ID
                            GROUP BY Section";
                query = string.Format(query);
                var result = maxFlowContext.Database.SqlQuery<UserViewModel>(query).ToList();
                return result;
            }
             */
        }

        public UserViewModel LookUpDirectReport(string userAccount)
        {
            throw new NotImplementedException();
            /*
            using (MaxFlowEntities maxFlowContext = new MaxFlowEntities())
            {
                var query = @"select 
                                top 1 REPLACE( ISNULL(AD_Account, ''), 'K2:', '') from MAXFLOW.dbo.FN_MaxFlow_SMI_General_Get_DirectReport('K2:{0}') a 
				                join MAXFLOW.dbo.General_Master_Position b on a.Position_ID = b.Position_ID
				                left join MAXFLOW.dbo.General_Mapping_Employee_Position c on b.Position_ID = c.Position_ID
				                left join MaxFlow.dbo.General_Master_Employee d on c.Employee_ID = d.Employee_ID
				                where Position_Name like '%Kepala Divisi%'
				                and (d.param6 = 'Active' or d.Param6 is null)";

                query = string.Format(query, userAccount);
                string managerUserAccount = maxFlowContext.Database.SqlQuery<string>(query).FirstOrDefault();


                ////if (!string.IsNullOrEmpty(managerUserAccount) && !managerUserAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase))
                ////{
                ////    var result = this.LookUpLoginUserAccount(managerUserAccount);
                ////    return result;
                ////}
                ////return null;


                if (!string.IsNullOrEmpty(managerUserAccount))
                {

                    check is any PLT
                    var query2 = @"SELECT
                                top 1
	                                REPLACE(ISNULL(AD_Account, ''), 'K2:', '')
                                from General_Master_Employee
                                join SMI_SKU on General_Master_Employee.Employee_ID = SMI_SKU.To_EmpID
                                where SMI_SKU.From_EmpID = (
	                                select Employee_ID from General_Master_Employee
	                                where AD_Account = 'K2:{0}'
                                )
                                and '{1}' between Valid_From and Valid_To
                                and Is_Active = 1";

                    query2 = string.Format(query2, managerUserAccount, DateTime.Now.Date.ToString("yyyy/MM/dd"));
                    string PLTAccount = maxFlowContext.Database.SqlQuery<string>(query2).FirstOrDefault();
                    end
    
                if (!string.IsNullOrEmpty(PLTAccount) && !PLTAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var results = this.LookUpLoginUserAccount(PLTAccount);
                        return results;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(managerUserAccount) && !managerUserAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var result = this.LookUpLoginUserAccount(managerUserAccount);
                            return result;
                        }
                        return null;
                    }
                }
                return null;
            }
            */
        }

        public IEnumerable<UserViewModel> LookUpUserDepartment(string Department)
        {
            throw new NotImplementedException();
            /*
             using (MaxFlowEntities maxFlowContext = new MaxFlowEntities())
            {
                var query = @"SELECT
                            General_Master_Employee.Employee_ID EmployeeId, REPLACE( ISNULL(AD_Account, ''), 'K2:', '') UserAccount, Employee_Name FullName,  Email, Param1 AS NIK, Position_Name PositionName, Section Divisi, Department 
                            FROM MaxFlow.dbo.General_Master_Employee 
                            LEFT JOIN MaxFlow.dbo.General_Mapping_Employee_Position ON General_Mapping_Employee_Position.Employee_ID = General_Master_Employee.Employee_ID
                            LEFT JOIN MaxFlow.dbo.General_Master_Position ON General_Mapping_Employee_Position.Position_ID = General_Master_Position.Position_ID
                            WHERE Department = '{0}'";
                query = string.Format(query, Department);
                var result = maxFlowContext.Database.SqlQuery<UserViewModel>(query).ToList();
                return result;
            }
             */
        }
    }
}
