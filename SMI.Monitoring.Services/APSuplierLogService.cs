﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.Domain2.Services;

namespace SMI.Monitoring.Services
{
    public class APSuplierLogService : IAPSuplierLogService
    {
        private readonly OracleEntities context1;
        //private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;


        public APSuplierLogService(OracleEntities _context)
        {
            context1 = _context;
            InitializeMapper();
        }

        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SMI_AP_SUPPLIER_STAGING, APSuplierLogViewModel>();
                cfg.CreateMap<APSuplierLogViewModel, SMI_AP_SUPPLIER_STAGING>();
                cfg.CreateMap<SMI_INT_VALIDATION_LOG, Int_Validation_LogViewModel>();
                cfg.CreateMap<Int_Validation_LogViewModel, SMI_INT_VALIDATION_LOG>();
                cfg.CreateMap<Int_SourceViewModel, SMI_INT_SOURCE>();
                cfg.CreateMap<SMI_INT_SOURCE, Int_SourceViewModel>();
            });
            mapper = config.CreateMapper();
        }

        #region APSupplierLog
        public APSuplierLogViewModel GetById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_AP_SUPPLIER_STAGING.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<APSuplierLogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<APSuplierLogViewModel> List()
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            //var query = context1.SMI_AP_SUPPLIER_STAGING.AsQueryable().AsNoTracking();
            //if (predicate != null)
            //{
            //    var result = query.Where(predicate).ProjectToList<APSuplierLogViewModel>(config);
            //    return result;
            //}
            //else
            //{
            //    var result = query.ProjectToList<APSuplierLogViewModel>(config);
            //    return result;
            //}
            // }

            #region remark
            //using (OracleEntities context = new OracleEntities())
            //{
            var query = string.Empty;
            query = @"SELECT 
                       NVL(SB.INT_SOURCE_ID,''), SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                       SB.MODUL, SB.SOURCE_VALUE,
                       S.INT_SOURCE_ID, S.INT_BATCH_ID, TO_CHAR(S.INT_STAGING_ID) AS INT_STAGING_ID,
                       S.VENDOR_NAME, S.VENDOR_NAME_ALT, S.VAT_REGISTRATION_NUM, 
                       S.VENDOR_SITE_CODE, S.ADDRESS_LINE1, S.ADDRESS_LINE2, 
                       S.ADDRESS_LINE3, S.ADDRESS_LINE4, S.CITY, 
                       S.COUNTY, S.STATE, S.PROVINCE, 
                       S.ZIP, S.COUNTRY, S.ALLOW_AWT_FLAG, 
                       S.AREA_CODE, S.PHONE, S.FAX_AREA_CODE, 
                       S.FAX, S.EMAIL_ADDRESS, S.ORG_ID, 
                       S.ATTRIBUTE_CATEGORY, S.ATTRIBUTE1, S.ATTRIBUTE2, 
                       S.ATTRIBUTE3, S.ATTRIBUTE4, S.ATTRIBUTE5, 
                       S.ATTRIBUTE6, S.ATTRIBUTE7, S.ATTRIBUTE8, 
                       S.ATTRIBUTE9, S.PURCHASING_SITE_FLAG, S.PAY_SITE_FLAG, 
                       S.ADDRESS_ATTRIBUTE_CATEGORY, S.ADDRESS_ATTRIBUTE1, S.ADDRESS_ATTRIBUTE2, 
                       S.ADDRESS_ATTRIBUTE3, S.ADDRESS_ATTRIBUTE4, S.ADDRESS_ATTRIBUTE5, 
                       S.ADDRESS_ATTRIBUTE6, S.ADDRESS_ATTRIBUTE7, S.ADDRESS_ATTRIBUTE8, 
                       S.ADDRESS_ATTRIBUTE9, S.ADDRESS_ATTRIBUTE10, S.CONTACT_FIRST_NAME, 
                       S.CONTACT_LAST_NAME, S.CONTACT_AREA_CODE, S.CONTACT_PHONE_NUMBER, 
                       S.CONTACT_EMAIL_ADDRESS, S.BANK_ID, S.BRANCH_ID, 
                       S.CURRENCY_CODE, S.BANK_ACCOUNT_NUM, S.BANK_ACCOUNT_NAME, 
                       S.CREATION_DATE, S.CREATED_BY, S.LAST_UPDATE_DATE, 
                       S.LAST_UPDATED_BY, S.INT_REF_ID, S.INTERFACE_DATE, 
                       S.INT_REQUEST_ID, S.IMPORT_REQUEST_ID, S.INT_STATUS,CASE WHEN S.INT_STATUS = 'S' THEN 'SUCCESS' WHEN S.INT_STATUS = 'F' THEN 'FAILED' ELSE '--'  END AS Status, 
                       S.UPLOAD_TYPE, S.CONTACT_MIDDLE_NAME, S.VENDOR_TYPE_LOOKUP_CODE, 
                       S.RFQ_ONLY_SITE_FLAG, S.ACCTS_PAY_CODE_COMBINATION_ID, S.PREPAY_CODE_COMBINATION_ID, 
                       S.PAYMENT_METHOD_LOOKUP_CODE, S.FREIGHT_TERMS_LOOKUP_CODE, S.BILL_TO_LOCATION_ID, 
                       S.BILL_TO_LOCATION_CODE, S.SHIP_TO_LOCATION_ID, S.SHIP_TO_LOCATION_CODE, 
                       S.SHIP_VIA_LOOKUP_CODE, S.INACTIVE_DATE
              FROM APPS.SMI_AP_SUPPLIER_STAGING S
              LEFT JOIN APPS.SMI_INT_SOURCE SB
              ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
              ";
            var result = context1.Database.SqlQuery<APSuplierLogViewModel>(query).ToList();
            return result;
        }


        #endregion

    

    public IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_SOURCE.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
        }

        public IEnumerable<APSuplierLogViewModel> ListSource(Expression<Func<SMI_AP_SUPPLIER_STAGING, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            #region remark
            using (OracleEntities context = new OracleEntities())
            {
                var query = string.Empty;
                query = @"SELECT DISTINCT
                           SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                           SB.MODUL, SB.SOURCE_VALUE,
                           S.INT_SOURCE_ID 
                  FROM APPS.SMI_AP_SUPPLIER_STAGING S
                  LEFT JOIN APPS.SMI_INT_SOURCE SB
                  ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
                  WHERE S.INT_SOURCE_ID > 1
                  ORDER BY SourceName
              ";
                var result = context1.Database.SqlQuery<APSuplierLogViewModel>(query).ToList();
                return result;
                #endregion

            }
        }
        #endregion

        #region Validation Log
        public Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_INT_VALIDATION_LOG.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<Int_Validation_LogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_VALIDATION_LOG.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            // }
        }
        #endregion
    }
}
