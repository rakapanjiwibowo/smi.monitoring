﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.Domain2.Services;

namespace SMI.Monitoring.Services
{
  public class POOrderLogService : IPOOrderLogService
    {
        private readonly OracleEntities context1;
        //private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;


        public POOrderLogService(OracleEntities _context)
        {
            context1 = _context;
            InitializeMapper();
        }

        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SMI_PO_ORDER_STAGING, POOrderLogViewModel>();
                cfg.CreateMap<POOrderLogViewModel, SMI_PO_ORDER_STAGING>();
                cfg.CreateMap<SMI_INT_VALIDATION_LOG, Int_Validation_LogViewModel>();
                cfg.CreateMap<Int_Validation_LogViewModel, SMI_INT_VALIDATION_LOG>();
                cfg.CreateMap<Int_SourceViewModel, SMI_INT_SOURCE>();
                cfg.CreateMap<SMI_INT_SOURCE, Int_SourceViewModel>();
            });
            mapper = config.CreateMapper();
        }

        #region POOrderLog
        public POOrderLogViewModel GetById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_PO_ORDER_STAGING.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<POOrderLogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<POOrderLogViewModel> List()
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            //var query = context1.SMI_PO_ORDER_STAGING.AsQueryable().AsNoTracking();
            //if (predicate != null)
            //{
            //    var result = query.Where(predicate).ProjectToList<POOrderLogViewModel>(config);
            //    return result;
            //}
            //else
            //{
            //    var result = query.ProjectToList<POOrderLogViewModel>(config);
            //    return result;
            //}


            #region remark
            //using (OracleEntities context = new OracleEntities())
            //{
            var query = string.Empty;
            query = @"SELECT 
                       
                       S.INT_SOURCE_ID, S.INT_BATCH_ID, TO_CHAR(S.INT_STAGING_ID) AS INT_STAGING_ID,  
                       S.DOCUMENT_TYPE_CODE, S.DOCUMENT_NUM, S.VENDOR_ID, 
                       S.VENDOR_SITE_ID, S.VENDOR_CONTACT_ID, S.SHIP_TO_LOCATION_ID, 
                       S.BILL_TO_LOCATION_ID, S.TERMS_ID, S.ENCUMBRANCE_REQUIRED_FLAG, 
                       S.CURRENCY_CODE, S.RATE, S.RATE_DATE, 
                       S.RATE_TYPE, S.LINE_NUM, S.DISTRIBUTION_NUM, 
                       S.LINE_TYPE, S.ITEM_ID, S.CATEGORY_ID, 
                       S.ITEM_DESCRIPTION, S.UNIT_OF_MEASURE, S.QUANTITY, 
                       S.UNIT_PRICE, S.NEED_BY_DATE, S.PROMISED_DATE, 
                       S.DESTINATION_TYPE, S.DELIVER_TO_PERSON_ID, S.ORG_ID, 
                       S.DELIVER_TO_LOCATION_ID, S.CHARGE_ACCOUNT, S.BUDGET_ACCOUNT, 
                       S.ACCRUAL_ACCOUNT, S.VARIANCE_ACCOUNT, S.HEADER_ATTRIBUTE_CATEGORY, 
                       S.HEADER_ATTRIBUTE1, S.HEADER_ATTRIBUTE2, S.HEADER_ATTRIBUTE3, 
                       S.HEADER_ATTRIBUTE4, S.HEADER_ATTRIBUTE5, S.LINE_ATTRIBUTE_CATEGORY, 
                       S.LINE_ATTRIBUTE1, S.LINE_ATTRIBUTE2, S.LINE_ATTRIBUTE3, 
                       S.LINE_ATTRIBUTE4, S.LINE_ATTRIBUTE5, S.DIST_ATTRIBUTE_CATEGORY, 
                       S.DISTRIBUTION_ATTRIBUTE1, S.DISTRIBUTION_ATTRIBUTE2, S.DISTRIBUTION_ATTRIBUTE3, 
                       S.DISTRIBUTION_ATTRIBUTE4, S.DISTRIBUTION_ATTRIBUTE5, S.CREATION_DATE, 
                       S.CREATED_BY, S.LAST_UPDATE_DATE, S.LAST_UPDATED_BY, 
                       S.INT_REF_ID, S.INTERFACE_DATE, S.INT_STATUS, 
                       S.INT_REQUEST_ID, S.IMPORT_REQUEST_ID, S.REQ_HEADER_REFERENCE_NUM, 
                       S.REQ_LINE_REFERENCE_NUM, S.PO_STATUS, S.PROCESS_CODE, 
                       S.HEADER_ACTION, S.LINE_ACTION, S.AGENT_ID, 
                       S.HEADER_REF_NUM, S.DIST_QUANTITY_ORDERED, S.APPROVAL_STATUS, 
                       S.REQUISITION_LINE_ID, S.REQ_DISTRIBUTION_ID, S.GL_DATE, 
                       S.REQ_DIST_REFERENCE_NUM, S.IMPORT_TRX_ID,
                       SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                       SB.MODUL, SB.SOURCE_VALUE
              FROM APPS.SMI_PO_ORDER_STAGING S
              LEFT JOIN APPS.SMI_INT_SOURCE SB
              ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
              ";
            var result = context1.Database.SqlQuery<POOrderLogViewModel>(query).ToList();
            return result;

            #endregion


        }

        public IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_SOURCE.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
        }


        public IEnumerable<POOrderLogViewModel> ListSource(Expression<Func<SMI_PO_ORDER_STAGING, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            #region remark
            using (OracleEntities context = new OracleEntities())
            {
                var query = string.Empty;
                query = @"SELECT DISTINCT
                           SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                           SB.MODUL, SB.SOURCE_VALUE,
                           S.INT_SOURCE_ID 
                  FROM APPS.SMI_PO_ORDER_STAGING S
                  LEFT JOIN APPS.SMI_INT_SOURCE SB
                  ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
                  WHERE S.INT_SOURCE_ID > 1
                  ORDER BY SourceName
              ";
                var result = context1.Database.SqlQuery<POOrderLogViewModel>(query).ToList();
                return result;
                #endregion

            }
        }



        #endregion

        #region Validation Log
        public Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_INT_VALIDATION_LOG.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<Int_Validation_LogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_VALIDATION_LOG.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            // }
        }
        #endregion



    }
}
