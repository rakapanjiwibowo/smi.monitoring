﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Services
{
   public class APInvoiceLogService : IAPInvoiceLogService
    {
        private readonly OracleEntities context1;
        //private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;


        public APInvoiceLogService(OracleEntities _context)
        {
            context1 = _context;
            InitializeMapper();
        }

        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SMI_AP_INVOICE_STAGING, APInvoiceLogViewModel>();
                cfg.CreateMap<APInvoiceLogViewModel, SMI_AP_INVOICE_STAGING>();
                cfg.CreateMap<SMI_INT_VALIDATION_LOG, Int_Validation_LogViewModel>();
                cfg.CreateMap<Int_Validation_LogViewModel, SMI_INT_VALIDATION_LOG>();
                cfg.CreateMap<Int_SourceViewModel, SMI_INT_SOURCE>();
                cfg.CreateMap<SMI_INT_SOURCE, Int_SourceViewModel>();
            });
            mapper = config.CreateMapper();
        }

        #region APInvoiceLog
        public APInvoiceLogViewModel GetById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_AP_INVOICE_STAGING.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<APInvoiceLogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<APInvoiceLogViewModel>List(DateTime? startDate = null, DateTime? endDate = null)
        //(Expression<Func<SMI_AP_INVOICE_STAGING, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            #region remark
            //using (OracleEntities context = new OracleEntities())
            //{
            var query = string.Empty;
            query = @"SELECT 
                       NVL(SB.INT_SOURCE_ID,''), SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                       SB.MODUL, SB.SOURCE_VALUE,
                       S.INT_SOURCE_ID, S.INT_BATCH_ID,TO_CHAR(S.INT_STAGING_ID) AS INT_STAGING_ID,
                       S.CODE_COMBINATION, S.INVOICE_BATCH, S.INVOICE_NO, 
                       S.INVOICE_DESCRIPTION, S.INVOICE_DATE, S.GL_DATE, 
                       S.INVOICE_TYPE, S.INVOICE_AMOUNT, S.CURRENCY_CODE, 
                       S.CURRENCY_RATE, S.RATE_DATE, S.RATE_TYPE, 
                       S.INTERFACE_DATE, CASE WHEN S.INT_STATUS = 'S' THEN 'SUCCESS' WHEN S.INT_STATUS = 'F' THEN 'FAILED' ELSE '--'  END AS Status, S.CREATION_DATE, 
                       S.CREATED_BY, S.LAST_UPDATE_DATE, S.LAST_UPDATED_BY, 
                       S.VENDOR_ID, S.INVOICE_ID, S.INT_GROUP_ID, 
                       S.VENDOR_SITE_ID, S.INVOICE_LINE_ID, S.TAX_CODE, 
                       S.WHT_CODE, S.INT_REF_ID, S.INT_REQUEST_ID, 
                       S.AP_INVOICE_SOURCE, S.TERMS_DATE, S.LINE_NUMBER, 
                       S.CCID, S.ORG_ID, S.LINE_DESCRIPTION, 
                       S.PO_NUMBER, S.PO_LINE_NUMBER, S.VALIDATE_FLAG, 
                       S.BATCH_CONTEXT, S.BATCH_ATTRIBUTE1, S.BATCH_ATTRIBUTE2, 
                       S.BATCH_ATTRIBUTE3, S.BATCH_ATTRIBUTE4, S.BATCH_ATTRIBUTE5, 
                       S.BATCH_ATTRIBUTE6, S.INVOICE_CONTEXT, S.INVOICE_ATTRIBUTE1, 
                       S.INVOICE_ATTRIBUTE2, S.INVOICE_ATTRIBUTE3, S.INVOICE_ATTRIBUTE4, 
                       S.INVOICE_ATTRIBUTE5, S.INVOICE_ATTRIBUTE6, S.INVOICE_ATTRIBUTE7, 
                       S.INVOICE_ATTRIBUTE8, S.INVOICE_ATTRIBUTE9, S.INVOICE_ATTRIBUTE10, 
                       S.INVOICE_ATTRIBUTE11, S.INVOICE_ATTRIBUTE12, S.INVOICE_ATTRIBUTE13, 
                       S.INVOICE_ATTRIBUTE14, S.INVOICE_ATTRIBUTE15, S.LINE_CONTEXT, 
                       S.LINE_ATTRIBUTE1, S.LINE_ATTRIBUTE2, S.LINE_ATTRIBUTE3, 
                       S.LINE_ATTRIBUTE4, S.LINE_ATTRIBUTE5, S.LINE_ATTRIBUTE6, 
                       S.LINE_ATTRIBUTE7, S.LINE_ATTRIBUTE8, S.LINE_ATTRIBUTE9, 
                       S.IMPORT_REQUEST_ID, S.AWT_GROUP_ID, S.CCID_LIABILITY, 
                       S.AMOUNT_INCL_TAX_FLAG, S.RECEIPT_NUM, S.RCV_TRX_ID, 
                       S.RCV_LINE_NUM, S.QUANTITY_INVOICED
              FROM APPS.SMI_AP_INVOICE_STAGING S
              LEFT JOIN APPS.SMI_INT_SOURCE SB
              ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
              ";
                var result = context1.Database.SqlQuery<APInvoiceLogViewModel>(query).ToList();
                return result;
            }
        

        #endregion

        //var query = context1.SMI_AP_INVOICE_STAGING.AsQueryable().AsNoTracking();
        //if (predicate != null)
        //    {
        //        var result = query.Where(predicate).ProjectToList<APInvoiceLogViewModel>(config);
        //        return result;
        //    }
        //    else
        //    {
        //        var result = query.ProjectToList<APInvoiceLogViewModel>(config);
        //        return result;
        //    }
        //}


        public IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_SOURCE.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
        }

        public IEnumerable<APInvoiceLogViewModel> ListSource(Expression<Func<SMI_AP_INVOICE_STAGING, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            #region remark
            using (OracleEntities context = new OracleEntities())
            {
                var query = string.Empty;
                query = @"SELECT DISTINCT
                           SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                           SB.MODUL, SB.SOURCE_VALUE,
                           S.INT_SOURCE_ID 
                  FROM APPS.SMI_AP_INVOICE_STAGING S
                  LEFT JOIN APPS.SMI_INT_SOURCE SB
                  ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
                  WHERE S.INT_SOURCE_ID > 1
                  ORDER BY SourceName
              ";
                var result = context1.Database.SqlQuery<APInvoiceLogViewModel>(query).ToList();
                return result;
                #endregion

            }
        }
        #endregion

        #region Validation Log
        public Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_INT_VALIDATION_LOG.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<Int_Validation_LogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_VALIDATION_LOG.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            // }
        }
        #endregion

    }
}
