﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain2.ViewModel;
using SMI.Monitoring.Domain2.Services;

namespace SMI.Monitoring.Services
{
   public class ARReceiptLogService :IARReceiptLogService
    {
        private readonly OracleEntities context1;
        //private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;


        public ARReceiptLogService(OracleEntities _context)
        {
            context1 = _context;
            InitializeMapper();
        }

        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SMI_AR_RECEIPT_STAGING, ARReceiptLogViewModel>();
                cfg.CreateMap<ARReceiptLogViewModel, SMI_AR_RECEIPT_STAGING>();
                cfg.CreateMap<SMI_INT_VALIDATION_LOG, Int_Validation_LogViewModel>();
                cfg.CreateMap<Int_Validation_LogViewModel, SMI_INT_VALIDATION_LOG>();
                cfg.CreateMap<Int_SourceViewModel, SMI_INT_SOURCE>();
                cfg.CreateMap<SMI_INT_SOURCE, Int_SourceViewModel>();
            });
            mapper = config.CreateMapper();
        }

        #region APInvoiceLog
        public ARReceiptLogViewModel GetById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_AR_RECEIPT_STAGING.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<ARReceiptLogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<ARReceiptLogViewModel> List()
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            //var query = context1.SMI_AR_RECEIPT_STAGING.AsQueryable().AsNoTracking();
            //if (predicate != null)
            //{
            //    var result = query.Where(predicate).ProjectToList<ARReceiptLogViewModel>(config);
            //    return result;
            //}
            //else
            //{
            //    var result = query.ProjectToList<ARReceiptLogViewModel>(config);
            //    return result;
            //}
            // }

            #region remark
            //using (OracleEntities context = new OracleEntities())
            //{
            var query = string.Empty;
            query = @"SELECT 
                       NVL(SB.INT_SOURCE_ID,''), SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                       SB.MODUL, SB.SOURCE_VALUE,
                       S.INT_SOURCE_ID, S.INT_BATCH_ID, TO_CHAR(S.INT_STAGING_ID) AS INT_STAGING_ID, 
                       S.RECEIPT_NUMBER, S.RECEIPT_DATE, S.ORG_ID, 
                       S.CURRENCY_CODE, S.CONVERSION_TYPE, S.CONVERSION_DATE, 
                       S.CONVERSION_RATE, S.AMOUNT, S.GL_DATE, 
                       S.CUSTOMER_ID, S.RECEIPT_METHOD_ID, S.CUSTOMER_TRX_ID, 
                       S.CASH_RECEIPT_ID, S.REVERSAL_CATEGORY_CODE, S.REASON_CODE, 
                       S.CREATION_DATE, S.CREATED_BY, S.LAST_UPDATE_DATE, 
                       S.LAST_UPDATED_BY, S.INT_REF_ID, S.INTERFACE_DATE, 
                       S.INT_STATUS,CASE WHEN S.INT_STATUS = 'S' THEN 'SUCCESS' WHEN S.INT_STATUS = 'F' THEN 'FAILED' ELSE '--'  END AS Status, S.INT_REQUEST_ID, S.IMPORT_REQUEST_ID, 
                       S.RECEIPT_TYPE, S.ACTIVITY_ID
              FROM APPS.SMI_AR_RECEIPT_STAGING S
              LEFT JOIN APPS.SMI_INT_SOURCE SB
              ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
              ";
            var result = context1.Database.SqlQuery<ARReceiptLogViewModel>(query).ToList();
            return result;
        }


        #endregion

    

    public IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_SOURCE.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_SourceViewModel>(config);
                return result;
            }
        }

        public IEnumerable<ARReceiptLogViewModel> ListSource(Expression<Func<SMI_AR_RECEIPT_STAGING, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            #region remark
            using (OracleEntities context = new OracleEntities())
            {
                var query = string.Empty;
                query = @"SELECT DISTINCT
                           SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
                           SB.MODUL, SB.SOURCE_VALUE,
                           S.INT_SOURCE_ID 
                  FROM APPS.SMI_AR_RECEIPT_STAGING S
                  LEFT JOIN APPS.SMI_INT_SOURCE SB
                  ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
                  WHERE S.INT_SOURCE_ID > 1
                  ORDER BY SourceName
              ";
                var result = context1.Database.SqlQuery<ARReceiptLogViewModel>(query).ToList();
                return result;
                #endregion

            }
        }
        #endregion

        #region Validation Log
        public Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_INT_VALIDATION_LOG.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<Int_Validation_LogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_VALIDATION_LOG.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            // }
        }
        #endregion
    }
}
