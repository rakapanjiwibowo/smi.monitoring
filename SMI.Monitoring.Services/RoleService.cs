﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;

namespace SMI.Monitoring.Services
{
    public class RoleService : IRoleService
    {
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public RoleService(MainEntities1 _context)
        {
            context = _context;
            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Role, RoleViewModel>();
                cfg.CreateMap<RoleViewModel, Role>();
                cfg.CreateMap<VwMappingRoleModule, VwMappingRoleModuleViewModel>();
                cfg.CreateMap<VwMappingRoleModuleViewModel, VwMappingRoleModule>();
            });
            mapper = config.CreateMapper();
        }

        public void Delete(IEnumerable<RoleViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<Role>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }

        public void Delete(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.Roles
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void Edit(IEnumerable<RoleViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = (from d in context.Roles
                                  where d.Id == item.Id
                                  select d).FirstOrDefault();

                    entity = mapper.Map(item, entity);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void Edit(RoleViewModel newItem)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.Roles
                              where d.Id == newItem.Id
                              select d).FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<RoleViewModel, Role>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public RoleViewModel GetById(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.Roles.Where(x => x.Id == Id).FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<RoleViewModel>(result);
                    return entity;
                }
                return null;
            }
        }

        public RoleViewModel GetByUserName(string userAccount)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = from role in context.Roles
                            join mappingRoleUser in context.VwMappingRoleUsers on role.Id equals mappingRoleUser.RoleId
                            where mappingRoleUser.UserAccount.Equals(userAccount, StringComparison.CurrentCultureIgnoreCase)
                            select role;
                var defaultRole = query.ProjectToFirstOrDefault<RoleViewModel>(config);
                if (defaultRole == null)
                {
                    //set by default is Normal User
                    defaultRole = context.Roles.Where(x => x.Id == (int)EnumRole.User).ProjectToFirstOrDefault<RoleViewModel>(config);
                }
                return defaultRole;

                throw new NotImplementedException();
            }

        }

        public void Insert(IEnumerable<RoleViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<Role>(item);
                    context.Entry(entity).State = EntityState.Added;
                }
                context.SaveChanges();
            }
        }

        public RoleViewModel Insert(RoleViewModel item)
        {
             using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<Role>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }

        public IEnumerable<RoleViewModel> List(Expression<Func<Role, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.Roles.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<RoleViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<RoleViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<VwMappingRoleModuleViewModel> ListMenu(string role)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = from m in context.VwMappingRoleModules.AsQueryable()
                            where m.RoleCode.Equals(role, StringComparison.CurrentCultureIgnoreCase) && m.IsActive
                            select m;
                var result = query.AsNoTracking().ProjectToList<VwMappingRoleModuleViewModel>(config);
                return result;
            }
        }
    }
}
