﻿using AutoMapper;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Services
{
    public class MappingK2GASPPDService : IMappingK2GASPPDService
    {
        private readonly MainEntitiesSPPD context1;
        private MapperConfiguration config;
        private IMapper mapper;

        public IEnumerable<VwMappingK2GASPPDViewModel> List()
        {
            var query = string.Empty;
            using (MainEntitiesSPPD context = new MainEntitiesSPPD())
            {
                query = @"SELECT
                            GAMonitoring_Oracle.IFACE_CODE,
                         GAMonitoring_Oracle.H02,
                         GAMonitoring_Oracle.Requestor
                      FROM  GAMonitoring_Oracle
                ";

                var result = context1.Database.SqlQuery<VwMappingK2GASPPDViewModel>(query).ToList();
                return result;

            }

        }
    }
}
