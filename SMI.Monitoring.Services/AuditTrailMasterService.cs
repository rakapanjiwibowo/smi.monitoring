﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using SMI.Monitoring.Helper;
using SMI.EncryptTools.Agent;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Services
{
    public class AuditTrailMasterService : IAuditTrailMasterService
    {
        //private readonly MainEntities context;
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public AuditTrailMasterService(MainEntities1 _context)
        {
            context = _context;

            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AuditTrailMaster, AuditTrailMasterViewModel>();
                cfg.CreateMap<AuditTrailMasterViewModel, AuditTrailMaster>();
                cfg.CreateMap<AuditTrailMasterDetail, AuditTrailMasterDetailViewModel>();
                cfg.CreateMap<AuditTrailMasterDetailViewModel, AuditTrailMasterDetail>();
                cfg.CreateMap<AuditTrailAction, AuditTrailActionViewModel>();
                cfg.CreateMap<AuditTrailActionViewModel, AuditTrailAction>();
                cfg.CreateMap<VwAuditTrailMaster, VwAuditTrailMasterViewModel>();
                cfg.CreateMap<VwAuditTrailMasterDetail, VwAuditTrailMasterDetailViewModel>();
            });
            mapper = config.CreateMapper();
        }

        public AuditTrailMasterViewModel GetById(long Id)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.AuditTrailMasters.Where(x => x.Id == Id).AsNoTracking().FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<AuditTrailMasterViewModel>(result);
                    return entity;
                }
                return null;
            }
        }

        public IEnumerable<AuditTrailMasterViewModel> List(Expression<Func<AuditTrailMaster, bool>> predicate = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.AuditTrailMasters.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<AuditTrailMasterViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<AuditTrailMasterViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<AuditTrailMasterDetailViewModel> ListDetail(Expression<Func<AuditTrailMasterDetail, bool>> predicate = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.AuditTrailMasterDetails.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<AuditTrailMasterDetailViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<AuditTrailMasterDetailViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<AuditTrailActionViewModel> ListAction(Expression<Func<AuditTrailAction, bool>> predicate = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.AuditTrailActions.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<AuditTrailActionViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<AuditTrailActionViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<VwAuditTrailMasterViewModel> ListView(Expression<Func<VwAuditTrailMaster, bool>> predicate = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.VwAuditTrailMasters.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<VwAuditTrailMasterViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<VwAuditTrailMasterViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<VwAuditTrailMasterDetailViewModel> ListViewDetail(Expression<Func<VwAuditTrailMasterDetail, bool>> predicate = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.VwAuditTrailMasterDetails.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<VwAuditTrailMasterDetailViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<VwAuditTrailMasterDetailViewModel>(config);
                    return result;
                }
            }
        }

        public AuditTrailMasterViewModel Insert(AuditTrailMasterViewModel item)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<AuditTrailMaster>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }

        public void Insert(IEnumerable<AuditTrailMasterViewModel> list)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<AuditTrailMaster>(item);
                    context.Entry(entity).State = EntityState.Added;
                }
                context.SaveChanges();
            }
        }

        public void Edit(AuditTrailMasterViewModel newItem)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.AuditTrailMasters
                              where d.Id == newItem.Id
                              select d).FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<AuditTrailMasterViewModel, AuditTrailMaster>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Edit(IEnumerable<AuditTrailMasterViewModel> list)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = (from d in context.AuditTrailMasters
                                  where d.Id == item.Id
                                  select d).FirstOrDefault();

                    entity = mapper.Map(item, entity);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void Delete(long Id)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.AuditTrailMasters
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void Delete(IEnumerable<AuditTrailMasterViewModel> list)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    //Mapper.CreateMap<AuditTrailMasterViewModel, AuditTrailMaster>();
                    var entity = mapper.Map<AuditTrailMaster>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }

        #region Audit Master Detail

        public IEnumerable<AuditTrailMasterDetailViewModel> ListAuditDetail(Expression<Func<AuditTrailMasterDetail, bool>> predicate = null)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.AuditTrailMasterDetails.AsQueryable();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<AuditTrailMasterDetailViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<AuditTrailMasterDetailViewModel>(config);
                    return result;
                }
            }
        }

        public AuditTrailMasterDetailViewModel InsertAuditDetail(AuditTrailMasterDetailViewModel item)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<AuditTrailMasterDetail>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }

        public void InsertAuditDetail(IEnumerable<AuditTrailMasterDetailViewModel> list)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<AuditTrailMasterDetail>(item);
                    context.Entry(entity).State = EntityState.Added;
                }
                context.SaveChanges();
            }
        }

        public void EditAuditDetail(AuditTrailMasterDetailViewModel newItem)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.AuditTrailMasterDetails
                              where d.Id == newItem.Id
                              select d).AsNoTracking().FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<AuditTrailMasterDetailViewModel, AuditTrailMasterDetail>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void EditAuditDetail(IEnumerable<AuditTrailMasterDetailViewModel> list)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    //Mapper.CreateMap<AuditTrailMasterDetailViewModel, AuditTrailMasterDetail>();
                    var entity = mapper.Map<AuditTrailMasterDetail>(item);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void DeleteAuditDetail(long Id)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.AuditTrailMasterDetails
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void DeleteAuditDetail(IEnumerable<AuditTrailMasterDetailViewModel> list)
        {
            using (MainEntities1 context = new MainEntities1())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    //Mapper.CreateMap<AuditTrailMasterDetailViewModel, AuditTrailMasterDetail>();
                    var entity = mapper.Map<AuditTrailMasterDetail>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }

        #endregion

        #region Custom Method

        /// <summary>
        /// Log Audit Trails
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oldObject"></param>
        /// <param name="newObject"></param>
        /// <param name="action"></param>
        /// <param name="notes"></param>
        /// <param name="userIdentity"></param>
        public void AuditTrail<T>(List<T> oldObject, List<T> newObject, EnumAuditAction action, string moduleName, string notes, string userIdentity)
        {
            List<T> selectedObject = null;
            switch ((EnumAuditAction)action)
            {
                case EnumAuditAction.Insert:
                    selectedObject = newObject;
                    for (int i = 0; i < selectedObject.Count; i++)
                    {
                        AuditTrail(default(T), newObject[i], action, moduleName, notes, userIdentity);
                    }
                    break;
                case EnumAuditAction.Edit:
                    selectedObject = newObject;
                    for (int i = 0; i < selectedObject.Count; i++)
                    {
                        AuditTrail(oldObject[i], newObject[i], action, moduleName, notes, userIdentity);
                    }
                    break;
                case EnumAuditAction.Delete:
                    selectedObject = oldObject;
                    for (int i = 0; i < selectedObject.Count; i++)
                    {
                        AuditTrail(oldObject[i], default(T), action, moduleName, notes, userIdentity);
                    }
                    break;
            }
        }

        /// <summary>
        /// Log Audit Trails
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type"></param>
        /// <param name="oldObject"></param>
        /// <param name="newObject"></param>
        /// <param name="action"></param>
        /// <param name="notes"></param>
        /// <param name="userIdentity"></param>
        public void AuditTrail<T>(T oldObject, T newObject, EnumAuditAction action, string moduleName, string notes, string userIdentity)
        {
            Type type = typeof(T);
            var result = this.Insert(new AuditTrailMasterViewModel
            {
                ModuleName = moduleName,
                TypeName = type.Name,
                AuditTrailActionId = (long)action,
                Notes = notes,
                CreatedBy = userIdentity,
                CreatedDate = DateTime.Now
            });

            var listProperty = type.GetProperties();
            List<AuditTrailMasterDetailViewModel> listAuditTrailDetails = new List<AuditTrailMasterDetailViewModel>();
            foreach (PropertyInfo p in listProperty)
            {
                try
                {
                    PropertyInfo property = type.GetProperty(p.Name);
                    //handle nullable type
                    Type targetType = property.PropertyType.IsNullableType() ? Nullable.GetUnderlyingType(property.PropertyType) : property.PropertyType;
                    object oldValue = null;
                    object newValue = null;
                    object convertedOldValue = null;
                    object convertedNewValue = null;

                    //skip for binary file
                    if (targetType == typeof(byte[])) continue;

                    switch ((EnumAuditAction)action)
                    {
                        case EnumAuditAction.Insert:
                            newValue = property.GetValue(newObject, null);
                            convertedNewValue = (newValue == null ? null : Convert.ChangeType(newValue, targetType));
                            listAuditTrailDetails.Add(new AuditTrailMasterDetailViewModel
                            {
                                AuditTrailMasterId = result.Id,
                                FieldName = p.Name,
                                FieldType = targetType.Name,
                                FieldNewValues = newValue == null ? string.Empty : convertedNewValue.ToString(),
                                CreatedBy = userIdentity,
                                CreatedDate = DateTime.Now
                            });
                            break;
                        case EnumAuditAction.Edit:
                            oldValue = property.GetValue(oldObject, null);
                            newValue = property.GetValue(newObject, null);
                            convertedOldValue = (oldValue == null ? null : Convert.ChangeType(oldValue, targetType));
                            convertedNewValue = (newValue == null ? null : Convert.ChangeType(newValue, targetType));
                            listAuditTrailDetails.Add(new AuditTrailMasterDetailViewModel
                            {
                                AuditTrailMasterId = result.Id,
                                FieldName = p.Name,
                                FieldType = targetType.Name,
                                FieldOldValues = oldValue == null ? string.Empty : convertedOldValue.ToString(),
                                FieldNewValues = newValue == null ? string.Empty : convertedNewValue.ToString(),
                                CreatedBy = userIdentity,
                                CreatedDate = DateTime.Now
                            });
                            break;
                        case EnumAuditAction.Delete:
                            oldValue = property.GetValue(oldObject, null);
                            convertedOldValue = (oldValue == null ? null : Convert.ChangeType(oldValue, targetType));
                            listAuditTrailDetails.Add(new AuditTrailMasterDetailViewModel
                            {
                                AuditTrailMasterId = result.Id,
                                FieldName = p.Name,
                                FieldType = targetType.Name,
                                FieldOldValues = oldValue == null ? string.Empty : convertedOldValue.ToString(),
                                CreatedBy = userIdentity,
                                CreatedDate = DateTime.Now
                            });
                            break;
                    }
                }
                catch (NullReferenceException nre)
                {
                    throw nre;
                }

            }
            this.InsertAuditDetail(listAuditTrailDetails);
        }

        #endregion

    }
}
