﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.ViewModel;

namespace SMI.Monitoring.Services
{
    public class SPPDMonitoringService
    {
        private readonly MainEntitiesSPPD context1;
        private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;

        public SPPDMonitoringService(MainEntities _context)
        {
            context = _context;
            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
           {
               cfg.CreateMap<Domain.SPPD, SPPDViewModel>();
               cfg.CreateMap<SPPDViewModel, Domain.SPPD>();
               cfg.CreateMap<FPPD, FPPDViewModel>();
               cfg.CreateMap<FPPDViewModel, FPPD>();
           });
            mapper = config.CreateMapper();
        }
    }
}
