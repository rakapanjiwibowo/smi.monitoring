﻿using AutoMapper;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SMI.Monitoring.Services
{
    public class MappingRoleUserService : IMappingRoleUserService
    {
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public MappingRoleUserService(MainEntities1 _context)
        {
            context = _context;
            InitializeMapper();
        }

        private void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MappingRoleUser, MappingRoleUserViewModel>();
                cfg.CreateMap<VwMappingRoleUser, VwMappingRoleUserViewModel>();
                cfg.CreateMap<MappingRoleUserViewModel, MappingRoleUser>();
            });
            mapper = config.CreateMapper();
        }

        public MappingRoleUserViewModel GetById(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var result = context.MappingRoleUsers.Where(x => x.Id == Id).FirstOrDefault();
                if (result != null)
                {
                    var entity = mapper.Map<MappingRoleUserViewModel>(result);
                    return entity;
                }
                return null;
            }
        }

        public IEnumerable<MappingRoleUserViewModel> List(Expression<Func<MappingRoleUser, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.MappingRoleUsers.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<MappingRoleUserViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<MappingRoleUserViewModel>(config);
                    return result;
                }
            }
        }

        public IEnumerable<VwMappingRoleUserViewModel> ListView(Expression<Func<VwMappingRoleUser, bool>> predicate = null)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var query = context.VwMappingRoleUsers.AsQueryable().AsNoTracking();
                if (predicate != null)
                {
                    var result = query.Where(predicate).ProjectToList<VwMappingRoleUserViewModel>(config);
                    return result;
                }
                else
                {
                    var result = query.ProjectToList<VwMappingRoleUserViewModel>(config);
                    return result;
                }
            }
        }

        public MappingRoleUserViewModel Insert(MappingRoleUserViewModel item)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = mapper.Map<MappingRoleUser>(item);
                context.Entry(entity).State = EntityState.Added;
                context.SaveChanges();
                item.Id = entity.Id;
                return item;
            }
        }

        public void Insert(IEnumerable<MappingRoleUserViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<MappingRoleUser>(item);
                    context.Entry(entity).State = EntityState.Added;
                }
                context.SaveChanges();
            }
        }


        public void Edit(MappingRoleUserViewModel newItem)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.MappingRoleUsers
                              where d.Id == newItem.Id
                              select d).FirstOrDefault();

                var localConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<MappingRoleUserViewModel, MappingRoleUser>()
                    .ForMember(src => src.CreatedBy, opt => opt.Ignore())
                    .ForMember(src => src.CreatedDate, opt => opt.Ignore());
                });
                var localMapper = localConfig.CreateMapper();
                entity = localMapper.Map(newItem, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }

        }


        public void Edit(IEnumerable<MappingRoleUserViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = (from d in context.MappingRoleUsers
                                  where d.Id == item.Id
                                  select d).FirstOrDefault();

                    entity = mapper.Map(item, entity);
                    context.Entry(entity).State = EntityState.Modified;
                }
                context.SaveChanges();
            }
        }

        public void Delete(long Id)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                var entity = (from d in context.MappingRoleUsers
                              where d.Id == Id
                              select d).FirstOrDefault();

                if (entity != null)
                {
                    context.Entry(entity).State = EntityState.Deleted;
                    context.SaveChanges();
                }
            }
        }

        public void Delete(IEnumerable<MappingRoleUserViewModel> list)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

                foreach (var item in list)
                {
                    var entity = mapper.Map<MappingRoleUser>(item);
                    context.Entry(entity).State = EntityState.Deleted;
                }
                context.SaveChanges();
            }
        }
    }
}
