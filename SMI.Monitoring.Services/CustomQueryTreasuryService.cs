﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;

namespace SMI.Monitoring.Services
{
    public class CustomQueryTreasuryService : ICustomQueryTreasuryService, IDisposable
    {
        private readonly IMSEntities context;
        private MapperConfiguration config;
        private IMapper mapper;

        public CustomQueryTreasuryService(IMSEntities _context)
        {
            context = _context;
        }

        public IEnumerable<T> ExecuteSqlQuery<T>(string query, params object[] args)
        {
            var results = context.Database.SqlQuery<T>(query, args);
            return results;
        }

        public IEnumerable<T> ExecuteQuery<T>(string query, params object[] args)
        {
            var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreQuery<T>(query, args);
            return results;
        }

        public int ExecuteStoreCommand(string query, params object[] args)
        {
            var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreCommand(query, args);
            return results;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
