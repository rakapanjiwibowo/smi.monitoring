﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System.Configuration;
using SMI.EncryptTools.Agent;
using SMI.Monitoring.Domain2;
using SMI.Monitoring.Domain2.Services;
using SMI.Monitoring.Domain2.ViewModel;

namespace SMI.Monitoring.Services
{
    public class sgt_gl_journalService : Isgt_gl_journal_stagingService
    {
        private readonly OracleEntities context1;
        private readonly vr1_staging_prodEntities context2;
        //private readonly MainEntities context;
        private MapperConfiguration config;
        private IMapper mapper;


        public sgt_gl_journalService(vr1_staging_prodEntities _context, OracleEntities _context1)
        {
            context1 = _context1;
            context2 = _context;
            InitializeMapper();
        }

        public void InitializeMapper()
        {
            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<stg_gl_journal_staging, sgt_gl_journal_stagingViewModel>();
                cfg.CreateMap<sgt_gl_journal_stagingViewModel, stg_gl_journal_staging>();
                cfg.CreateMap<SMI_INT_VALIDATION_LOG, Int_Validation_LogViewModel>();
                cfg.CreateMap<Int_Validation_LogViewModel, SMI_INT_VALIDATION_LOG>();
                cfg.CreateMap<Int_SourceViewModel, SMI_INT_SOURCE>();
                cfg.CreateMap<SMI_INT_SOURCE, Int_SourceViewModel>();
            });
            mapper = config.CreateMapper();
        }

        #region sgt_gl_journal
        public sgt_gl_journal_stagingViewModel GetById(int int_batch_id, int int_staging_id)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context2.stg_gl_journal_staging.Where(x => x.int_batch_id == int_batch_id && x.int_staging_id == int_staging_id).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<sgt_gl_journal_stagingViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        //public IEnumerable<sgt_gl_journal_stagingViewModel> List()
        public IEnumerable<sgt_gl_journal_stagingViewModel> List(Expression<Func<stg_gl_journal_staging, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context2.stg_gl_journal_staging.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<sgt_gl_journal_stagingViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<sgt_gl_journal_stagingViewModel>(config);
                return result;
            }
        }


        #region remark
        //    using (OracleEntities context = new OracleEntities())
        //    {
        //        var query = string.Empty;
        //        query = @"SELECT 
        //               SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
        //               SB.MODUL, SB.SOURCE_VALUE,
        //               S.INT_SOURCE_ID, S.INT_BATCH_ID, TO_CHAR(S.INT_STAGING_ID) AS INT_STAGING_ID, 
        //               S.GL_DATE, S.CURRENCY_CODE, S.CURRENCY_RATE, 
        //               S.RATE_DATE, S.RATE_TYPE, S.CODE_COMBINATION, 
        //               S.DR_AMOUNT, S.CR_AMOUNT, S.BATCH_NAME, 
        //               S.JOURNAL_NAME, S.JOURNAL_CATEGORY, S.LINE_DESCRIPTION, 
        //               S.INT_GROUP_ID, S.INT_STATUS,CASE WHEN S.INT_STATUS = 'S' THEN 'SUCCESS' WHEN S.INT_STATUS = 'F' THEN 'FAILED' ELSE '--'  END AS Status,
        //               S.ACTUAL_FLAG, 
        //               S.CREATION_DATE, S.CREATED_BY, S.LAST_UPDATE_DATE, 
        //               S.LAST_UPDATED_BY, S.CCID, S.JE_BATCH_ID, 
        //               S.JE_HEADER_ID, S.JE_LINE_NUM, S.INT_REQUEST_ID, 
        //               S.INTERFACE_DATE, S.INT_REF_ID, S.LEDGER_ID, 
        //               S.JOURNAL_ATTRIBUTE1, S.JOURNAL_ATTRIBUTE2, S.JOURNAL_ATTRIBUTE3, 
        //               S.JOURNAL_ATTRIBUTE4, S.JOURNAL_ATTRIBUTE5, S.BATCH_ATTRIBUTE1, 
        //               S.BATCH_ATTRIBUTE2, S.BATCH_ATTRIBUTE3, S.BATCH_ATTRIBUTE4, 
        //               S.BATCH_ATTRIBUTE5, S.IMPORT_REQUEST_ID, S.BATCH_CONTEXT, 
        //               S.JOURNAL_CONTEXT, S.PERIOD_NAME, S.JOURNAL_SOURCE, 
        //               S.BUDGET_VERSION_ID, S.ENCUMBRANCE_TYPE_ID, S.LAST_UPDATE_LOGIN, 
        //               S.POSTED_FLAG
        //      FROM APPS.SMI_GL_JOURNAL_STAGING S
        //      LEFT JOIN APPS.SMI_INT_SOURCE SB
        //      ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
        //      ";
        //        var result = context1.Database.SqlQuery<GLJournalLogViewModel>(query).ToList();
        //        return result;


        //    }
        //}
        #endregion


        #region remark
        //public IEnumerable<Int_SourceViewModel> ListSourceName(Expression<Func<SMI_INT_SOURCE, bool>> predicate = null)
        //{
        //    //using (oracleentities context = new oracleentities())
        //    //{
        //    //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //    //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //    //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //    var query = context1.SMI_INT_SOURCE.AsQueryable().AsNoTracking();
        //    if (predicate != null)
        //    {
        //        var result = query.Where(predicate).ProjectToList<Int_SourceViewModel>(config);
        //        return result;
        //    }
        //    else
        //    {
        //        var result = query.ProjectToList<Int_SourceViewModel>(config);
        //        return result;
        //    }
        //}



        //public IEnumerable<GLJournalLogViewModel> ListSource(Expression<Func<SMI_GL_JOURNAL_STAGING, bool>> predicate = null)
        //{
        //    //using (oracleentities context = new oracleentities())
        //    //{
        //    //var encrypt = ConfigurationManager.AppSettings["Connection"];
        //    //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
        //    //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

        //    #region remark
        //    using (OracleEntities context = new OracleEntities())
        //    {
        //        var query = string.Empty;
        //        query = @"SELECT DISTINCT
        //                   SB.INT_SOURCE_ID, SB.INT_SOURCE_NAME || '-'|| SB.INT_NAME AS SourceName, 
        //                   SB.MODUL, SB.SOURCE_VALUE,
        //                   S.INT_SOURCE_ID 
        //          FROM APPS.SMI_GL_JOURNAL_STAGING S
        //          LEFT JOIN APPS.SMI_INT_SOURCE SB
        //          ON S.INT_SOURCE_ID = SB.INT_SOURCE_ID
        //          WHERE S.INT_SOURCE_ID > 1
        //          ORDER BY SourceName
        //      ";
        //        var result = context1.Database.SqlQuery<GLJournalLogViewModel>(query).ToList();
        //        return result;
        //        #endregion

        //    }
        //}

        #endregion

        #endregion


        #region Validation Log
        public Int_Validation_LogViewModel GetValidationById(string INT_SOURCE_ID)
        {
            //using (OracleEntities context = new OracleEntities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var result = context1.SMI_INT_VALIDATION_LOG.Where(x => x.INT_SOURCE_ID == INT_SOURCE_ID).FirstOrDefault();
            if (result != null)
            {
                var entity = mapper.Map<Int_Validation_LogViewModel>(result);
                return entity;
            }
            return null;
            //}
        }

        public IEnumerable<Int_Validation_LogViewModel> ListValidation(Expression<Func<SMI_INT_VALIDATION_LOG, bool>> predicate = null)
        {
            //using (oracleentities context = new oracleentities())
            //{
            //var encrypt = ConfigurationManager.AppSettings["Connection"];
            //var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
            //context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);

            var query = context1.SMI_INT_VALIDATION_LOG.AsQueryable().AsNoTracking();
            if (predicate != null)
            {
                var result = query.Where(predicate).ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            else
            {
                var result = query.ProjectToList<Int_Validation_LogViewModel>(config);
                return result;
            }
            // }
        }
        #endregion

    }
}
