﻿using AutoMapper;
using SMI.Monitoring.Domain;
using SMI.Monitoring.Domain.Services;
using SMI.Monitoring.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;
using System.Configuration;
using SMI.EncryptTools.Agent;

namespace SMI.Monitoring.Services
{
    public class CustomQueryService : ICustomQueryService, IDisposable
    {
        private readonly MainEntities1 context;
        private MapperConfiguration config;
        private IMapper mapper;

        public CustomQueryService(MainEntities1 _context)
        {
            context = _context;
        }

        public IEnumerable<T> ExecuteSqlQuery<T>(string query, params object[] args)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
                var results = context.Database.SqlQuery<T>(query, args);
                return results;
            }
        }

        public IEnumerable<T> ExecuteQuery<T>(string query, params object[] args)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
                var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreQuery<T>(query, args);
                return results;
            }
        }

        public int ExecuteStoreCommand(string query, params object[] args)
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
                var results = (context as IObjectContextAdapter).ObjectContext.ExecuteStoreCommand(query, args);
                return results;
            }
        }

        public void Dispose()
        {
            using (MainEntities context = new MainEntities())
            {
                var encrypt = ConfigurationManager.AppSettings["Connection"];
                var strConn = AESCryptoWithBCHelper.DecryptAESBounceCastle(encrypt);
                context.Database.Connection.ConnectionString = strConn.Replace("\0", string.Empty);
                context.Dispose();
            }
        }
    }
}
